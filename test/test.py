import torch
import torch.cuda
import os, sys
from torch.autograd import Variable, Function
import math
import ipdb

import tu.modules.ccl
import models


import torch.nn.functional as F

from torch.autograd import gradcheck



print('NbrNorm')

class NbrNorm(Function):
    def __init__(self, dii, djj, nbrhood):
        """
        : nbrhood [ntgt, nnbr]
        : dii [ntgt, nnbr]
        : djj [nsup, nsup]
        """
        from tu.functions.ccl import DLSEFunction
        super(NbrNorm, self).__init__()
        self.dii = dii
        self.djj = djj
        self.sigma = 4
        self.nbrhood = nbrhood
        self.dlse = DLSEFunction(4)

    def forward(self, similarities):
        """ similarities are not in log domain
        """
        dii = self.dii
        djj = self.djj
        nbrhood = self.nbrhood
        ntgt, nnbr = nbrhood.size()
        nsup = djj.size(0)
        nbrSimilarity = []
        for ti in range(ntgt):
            si_j_ = similarities.index_select(1, nbrhood[ti]) #[nb, nnbr i_, ns, nsup j_]
            djj_ = djj #[nsup j, nsup j_]
            di_ = dii[ti] # [nnbr i_]
            out = self.dlse.forward(si_j_, di_, djj_) # [nb, nnbr i_, ns, nsup j]
            res = out.mean(1) #[nb, ns,  nsup j]
            nbrSimilarity.append(res)
        nbrSimilarity = torch.stack(nbrSimilarity, dim=1) # [nb, ntgt i, ns, nsup j]

        self.save_for_backward(similarities)
        return nbrSimilarity

    def backward(self, gout):
        """
        gout [nb, ntgt i, ns, nsup j]
        """
        dii = self.dii
        djj = self.djj
        nbrhood = self.nbrhood
        similarities = self.saved_variables[0]
        res = gout * 0
        nb, ntgt, ns, nsup = gout.size()
        nnbr = nbrhood.size(1)
        for ti in range(ntgt):
            si_j_ = similarities.data.index_select(1, nbrhood[ti]) #[nb, nnbr i_, ns, nsup j_]
            djj_ = djj #[nsup j, nsup j_]
            di_ = dii[ti] # [nnbr i_]

            g0 = gout.select(1,ti) #[nb, ns, nsup j]
            g = self.dlse.backward(g0, si_j_, di_, djj_) #[nb, nnbr i_, ns, nsup _j]
            nbh = nbrhood[ti].view(1,nnbr, 1, 1).repeat(nb, 1, ns, nsup)
            res.scatter_add_(1,nbh,  g)
        return res/nnbr



for i in range(20):
    nb, ntgt, ns, nsup = 2, 5, 3, 7
    nnbr = 3
    nbrhood = torch.stack([torch.randperm(ntgt)[:nnbr] for i in range(ntgt)], dim=0).cuda() # [ntgt, nnbr]

    dii = torch.randn(ntgt, nnbr).abs().cuda().double()
    djj = torch.randn(nsup, nsup).abs().cuda().double()
    djj = djj * djj.t() # for symmetric
    #sim = (Variable(-torch.randn(nb, ntgt, ns, nsup).abs().cuda().double(), requires_grad = True), )
    sim = (Variable(-torch.randn(nb, ntgt, ns, nsup).abs().cuda().double()*0, requires_grad = True), )
    test = gradcheck(NbrNorm(dii, djj, nbrhood), sim, eps=1e-6, atol=1e-4)
    print(test)

exit(1)

print('PairwiseDiffL2Function')
for i in range(20):
    input = (Variable(torch.randn(2,23,28).cuda().double(), requires_grad=True),
             Variable(torch.randn(2,5*37,28).cuda().double(), requires_grad=True),)
    test = gradcheck(tu.functions.ccl.PairwiseDiffL2Function(), input, eps=1e-6, atol=1e-4)
    print(test)



print('PairwiseDiffL1Function')
for i in range(20):
    input = (Variable(torch.randn(2,23,28).cuda().double(), requires_grad=True),
             Variable(torch.randn(2,5*37,28).cuda().double(), requires_grad=True),)
    test = gradcheck(tu.functions.ccl.PairwiseDiffL1Function(), input, eps=1e-6, atol=1e-4)
    print(test)

print('MinCostMatching')
for i in range(20):
    nb, nsrc, npxsrc, ndim = 10, 1, 23, 28
    ntgt, npxtgt = 5, 37
    src = Variable(torch.randn(nb, nsrc, npxsrc, ndim).cuda().double(), requires_grad=True)
    tgt = Variable(torch.randn(nb, ntgt, npxtgt, ndim).cuda().double(), requires_grad=True)
    input = (src, tgt)
    test = gradcheck(models.MinCostMatching(), input, eps=1e-6, atol=1e-4)
    print(test)
