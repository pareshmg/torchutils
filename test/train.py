from __future__ import print_function
import builtins
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


import argparse, sys
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import tu.modules.ccl
from torchvision import datasets, transforms
from torch.autograd import Variable
import ipdb
from tqdm import tqdm
import os, sys
import numpy

from tmp import *
from models import *

from tu.imageUtils import *

# Training settings
parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
parser.add_argument('--mini-batch-size', type=int, default=4, metavar='N',
                    help='input batch size for training')
parser.add_argument('--batch-size', type=int, default=4, metavar='N',
                    help='input batch size for training (default: 64)')
parser.add_argument('--test-batch-size', type=int, default=4, metavar='N',
                    help='input batch size for testing')
parser.add_argument('--epochs', type=int, default=10, metavar='N',
                    help='number of epochs to train (default: 10)')
parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                    help='learning rate (default: 0.01)')
parser.add_argument('--momentum', type=float, default=0.9, metavar='M',
                    help='SGD momentum (default: 0.5)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                    help='how many batches to wait before logging training status')
parser.add_argument('--dataDir', type=str, default='/home/pareshmg/data',
                    help='which dir to get data from')
parser.add_argument('--outDir', type=str, default='/home/pareshmg/outs/logs/test',
                    help='which dir to get data from')
parser.add_argument('--deg', type=int, default=8,
                    help='which dir to get data from')
parser.add_argument('--dataset', type=str, default='MNIST',
                    help='which dataset to use: MNIST, SVHN')
parser.add_argument('--model', type=str, help='model to run')
builtins.args = parser.parse_args()


args.cuda = not args.no_cuda and torch.cuda.is_available()

args.dataset = args.dataset.upper().strip()
args.model = args.model.strip()

args.outDir = os.path.join(args.outDir.strip(), args.dataset, args.model)
args.dataDir = os.path.join(args.dataDir.strip(), args.dataset)

if not os.path.exists(args.dataDir):
    os.makedirs(args.dataDir)

if not os.path.exists(args.outDir):
    os.makedirs(args.outDir)



torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)

train_loader = None
test_loader = None



if args.dataset == 'MNIST':
    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.1307,), (0.3081,)))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        datasets.MNIST(args.dataDir, train=False,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.1307,), (0.3081,)))
            ])),
        batch_size=args.test_batch_size, shuffle=True)
elif args.dataset == 'FASHIONMNIST':
    train_loader = torch.utils.data.DataLoader(
        FashionMNIST(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5,), (0.5,)))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        FashionMNIST(args.dataDir, train=False,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5,), (0.5,)))
            ])),
        batch_size=args.test_batch_size, shuffle=True)
elif args.dataset == 'SVHN':
    train_loader = torch.utils.data.DataLoader(
        datasets.SVHN(args.dataDir, split='train', download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        datasets.SVHN(args.dataDir, split='test',
            transform=transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
            ])),
        batch_size=args.test_batch_size, shuffle=True)
elif args.dataset == 'CIFAR10':
    train_loader = torch.utils.data.DataLoader(
        datasets.CIFAR10(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        datasets.CIFAR10(args.dataDir, train=False,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ])),
        batch_size=args.test_batch_size, shuffle=True)
elif args.dataset == 'VOC':
    train_loader = torch.utils.data.DataLoader(
        VOCSegmentation(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        VOCSegmentation(args.dataDir, train=False,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ])),
        batch_size=args.test_batch_size, shuffle=True)
else:
    raise RuntimeError("unsupported dataset " + args.dataset)



for batch_idx, ((origdata, data), target) in enumerate(train_loader):
    _, args.nch, args.nr, args.nc = data.size()
    break


kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}




model = eval(args.model)()
if args.cuda:
    model.cuda()

#optimizer = optim.Adam(model.parameters(), lr=args.lr)
optimizer = optim.Adam([{'params': [x for (n,x) in model.named_parameters() if n != 'thetaNet.cclShape.weights']},
                        {'params': model.thetaNet.cclShape.parameters(), 'lr': args.lr*2}],
                        lr=args.lr)


def dbghook(m, gi, go):
    print(m)
    debugTensor(gi)
    debugTensor(go)

def dbgModel(model):
    print('grads')
    for x in model.parameters():
        if x.grad is not None:
            print(x.size(), x.data.abs().min(), x.data.abs().max(), x.grad.data.abs().min(), x.grad.data.abs().max())
        else:
            print(x.size(), x.data.abs().min(), x.data.abs().max())



def visualize(model, res, fname):
    shapefvs = res['shapefvs'].data
    clusters = res['clusters'].data
    batchIndex = res['batchIndex'].data
    shapellikelihood = res['shapellikelihood'].data

    nshapes = model.thetaNet.cclShape.nshapes
    uniqueBatches = numpy.unique(batchIndex.cpu().numpy())
    if True:
        nb, ns, ndim = shapefvs.size()
        clusters = (clusters >= 0).float()
        imgs = []
        for ubi in uniqueBatches:
            origImg = res['origdata'].select(0, ubi).permute(1,2,0)


            sel = torch.nonzero(batchIndex == int(ubi)).select(1,0)
            clusterImg = clusters.index_select(0, sel)
            clusterImg = [clusterImg.select(0,bi).contiguous().view(args.nr, args.nc) for bi in range(clusterImg.size(0))]

            tmp = padImg(hstackImages([origImg, hstackImages(clusterImg)]))
            imgs.append(tmp)
        imgs = vstackImages(imgs)
        imgs = makeImg(imgs)
        imgs.save(os.path.join(args.outDir, fname+'.png'))
        return imgs



def visualizeShapes(model, res, fname):
    shapefvs = res['shapefvs'].data
    clusters = res['clusters'].data
    batchIndex = res['batchIndex'].data
    shapellikelihood = res['shapellikelihood'].data
    weights = model.thetaNet.cclShape.weights.data
    nshapes = model.thetaNet.cclShape.nshapes
    uniqueBatches = numpy.unique(batchIndex.cpu().numpy())
    if True:
        nb, ns, ndim = shapefvs.size()
        clusters = (clusters >= 0).float()
        imgs = []
        p = (shapefvs.view(nb*ns, ndim).mm(weights).view(nb, ns, nshapes) >= 0).float()

        for ubi in uniqueBatches:
            origImg = res['origdata'].select(0, ubi).permute(1,2,0)


            sel = torch.nonzero(batchIndex == int(ubi)).select(1,0)
            clusterImg = clusters.index_select(0, sel)
            clusterImg = [clusterImg.select(0,bi).contiguous().view(args.nr, args.nc) for bi in range(clusterImg.size(0))]
            shapell = shapellikelihood.index_select(0, sel).max(0)[1]



            pimg = p.index_select(0, sel)
            tmp = []
            for bi in range(pimg.size(0)):
                shapeimgs = [pimg.select(2,si).select(0,bi).contiguous().view(args.nr, args.nc) for si in range(pimg.size(2))]
                shapeimgs[shapell[bi]] = padImg(shapeimgs[shapell[bi]], padWidth=1, padFill=[0,0.5,0])
                shapeimgs = hstackImages(shapeimgs)
                tmp.append(hstackImages([padImg(clusterImg[bi]), shapeimgs]))
            tmp = padImg(hstackImages([origImg, vstackImages(tmp)]))
            imgs.append(tmp)
        imgs = vstackImages(imgs)
        imgs = makeImg(imgs)
        imgs.save(os.path.join(args.outDir, fname+'.png'))
        return imgs


def train(epoch):
    model.train()


    with tqdm(total = len(train_loader)) as pbar:
        for batch_idx, ((origdata, data), target) in enumerate(train_loader):
            if args.cuda:
                data, target = data.cuda(), target.cuda()
            data, target = Variable(data), Variable(target)

            loss = 0
            nmb = 0
            optimizer.zero_grad()

            res = None

            for mbi in range(data.size(0)//args.mini_batch_size):
                d = data.narrow(0, mbi*args.mini_batch_size, args.mini_batch_size)
                t = target.narrow(0, mbi*args.mini_batch_size, args.mini_batch_size)
                res = model(d)

                l = F.nll_loss(res['classification'], t) - 0.01*res['shapellikelihood'].max(1)[0].mean()
                l.backward()
                loss += l.data[0]
                res['data'] = d
                res['target'] = t
                res['origdata'] = origdata.narrow(0, mbi*args.mini_batch_size, args.mini_batch_size).cuda()
                nmb += 1

            loss /= max(1,nmb)

            ## progress bar
            if batch_idx % args.log_interval == 0:
                pbar.set_description('Train Epoch: {} [{}/{} ({:.0f}%)]: Loss: {:.6f}'.format(
                    epoch, batch_idx * len(data), len(train_loader.dataset),
                    100. * batch_idx / len(train_loader), loss))
                pbar.update(args.log_interval)

            ## visualize
            if batch_idx % (args.log_interval*10) == 0:
                visualize(model, res, 'epoch%d_batch%d'%(epoch, batch_idx))


            # step
            optimizer.step()

def test():
    model.eval()
    test_loss = 0
    correct = 0
    for (origdata, data), target in test_loader:
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data, volatile=True), Variable(target)

        res = model(data)
        test_loss += F.nll_loss(res['classification'], target, size_average=False).data[0] # sum up batch loss
        pred = res['classification'].data.max(1, keepdim=True)[1] # get the index of the max log-probability
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()

    test_loss /= len(test_loader.dataset)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))




if __name__ == "__main__":
    if True:
        # model.thetaNet.cclShape.register_backward_hook(dbghook)
        nshapes = model.thetaNet.cclShape.nshapes
        nr = args.nr
        nc = args.nc
        ns = nr * nc
        fvs = model.thetaNet.rbf.clusterFV(torch.ones(nshapes, nr, nc).cuda(),
                                           torch.ones(nshapes).cuda().long()*ns)
        fvs = fvs.view(fvs.size(0), nr*nc, fvs.size(3)) # batch * nsamps * dim
        #model.thetaNet.cclShape.visualizeWeights(fvs, torch.ones(nshapes, ns), torch.ones(nshapes, ns), 'init')


    for epoch in range(1, args.epochs + 1):
        train(epoch)
        test()
