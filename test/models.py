import builtins
import torch
from torch.nn.modules.module import Module
from torch.autograd import Variable, Function
from torch.nn import Parameter
from torch import optim
import torch.nn as nn
import torch.nn.functional as F
import tu
import tu.modules.ccl
import numpy
import ipdb
import os
import gc
import torchvision
import torch.nn.init as init
import math
from tu.imageUtils import *
from tu.utils import *
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw


import torch.nn.functional as F

from torch.autograd import gradcheck
from tu.utils import *
from workingModels import *


class MinCostMatching(nn.Module):
    def forward(self, xsrc, xtgt):
        """
        xsrc :: nb * nsrc * npxsrc * ndim
        xtgt :: nb * ntgt * npxtgt * ndim

        return :: nb * nsrc * npxsrc * ntgt * npxtgt
        """
        nb, nsrc, npxsrc, ndim = xsrc.size()
        _, ntgt, npxtgt, _ = xtgt.size()

        xsrc = xsrc.view(nb, nsrc * npxsrc, ndim)
        xtgt = xtgt.view(nb, ntgt * npxtgt, ndim)

        diff = tu.modules.ccl.PairwiseDiffL1()(xsrc, xtgt)
        diff = diff.view(nb, nsrc, npxsrc, ntgt, npxtgt)

        mindiff = diff.min(4)[0] # nb * nsrc * npxsrc * ntgt
        totcost = mindiff.mean(2) # nb * nsrc * ntgt
        return totcost, diff




class Entropy(nn.Module):
    def forward(self, x):
        """
        x = float any
        """
        H = x * x.log().clamp(min=-100) + (1-x) * (1-x).log().clamp(min=-100)
        return -H


class LogisticNet(nn.Module):
    """ Shape only. No NN after.
    """
    def __init__(self):
        super(LogisticNet, self).__init__()
        self.conv1 = nn.Conv2d(args.nch, 10, kernel_size=5, padding=2)
        self.conv2 = nn.Conv2d(10, 50, kernel_size=5, padding=2)

        self.nshapes = 30

        self.deg = args.deg
        self.conv2_drop = nn.Dropout2d()

        self.phi = tu.modules.ccl.ToProb(50)
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.fc2 = nn.Linear(50 + self.thetaNet.ndim, 10)

    def forward(self, x):
        nb = x.size(0)
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = x.permute(0,2,3,1).contiguous()
        phi = self.phi(x) # probability of belonging to cluster

        res = self.thetaNet(phi, x)

        # thetas: nodes x thetadim
        x = res['theta']
        x = self.fc2(x)

        ## reorganize nodes by batch and pick the best supporting node
        xs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            mx,_ = x.index_select(0, Variable(idxs, requires_grad=False)).max(0)
            xs.append(mx.unsqueeze(0))

        x = torch.cat(xs, dim=0)

        res['classification'] = F.log_softmax(x)
        return res



class CNNMNIST(nn.Module):
    def __init__(self):
        super(Net0, self).__init__()
        self.conv1 = nn.Conv2d(args.nch, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, 10)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x)



# class PairwiseDiffL1(nn.Module):
#     """  applies linear layers and applies ReLU between each layer. Last layer doesn't have a relu
#     """
#     def forward(self, x1, x2):
#         """
#         x1: n1 * ndim
#         x2: n2 * ndim
#         """
#         diff = (x1.unsqueeze(1) - x2.unsqueeze(0)).abs().mean(2)
#         return diff



class AverageShapeFVNNForegroundNet(nn.Module):
    """ Shape only. No NN after.
    """
    def __init__(self):
        super(AverageShapeFVNNForegroundNet, self).__init__()
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.cclShape = tu.modules.ccl.ShapeAverageModule(self.nshapes, self.thetaNet.ndim)

        nodim = self.thetaNet.ndim
        self.fcs = SimpleNN([self.thetaNet.ndim, 50, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb = x0.size(0)

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = inp['actualSeg']

        res = self.thetaNet(phi, x)

        theta, llikelihood =  self.cclShape(res['probs'], res['clusters'],
                                            res['shapefvs'], res['contentfvs'], res['sizes'])

        x = self.fcs(theta)

        # thetas: nodes x thetadim
        ## reorganize nodes by batch and pick the best supporting node
        xs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            mx,_ = x.index_select(0, Variable(idxs, requires_grad=False)).max(0)
            xs.append(mx.unsqueeze(0))

        x = torch.cat(xs, dim=0)

        res['classification'] = F.log_softmax(x)
        return res





class AverageShapeFVForegroundNet(nn.Module):
    """ Shape only. No NN after.
    """
    def __init__(self):
        super(AverageShapeFVForegroundNet, self).__init__()
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.cclShape = tu.modules.ccl.ShapeAverageModule(self.nshapes, self.thetaNet.ndim)

        nodim = self.thetaNet.ndim
        self.fcs = SimpleNN([nodim, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb = x0.size(0)

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = inp['actualSeg']

        res = self.thetaNet(phi, x)

        theta, llikelihood =  self.cclShape(res['probs'], res['clusters'],
                                            res['shapefvs'], res['contentfvs'], res['sizes'])

        x = self.fcs(theta)

        # thetas: nodes x thetadim
        ## reorganize nodes by batch and pick the best supporting node
        xs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            mx,_ = x.index_select(0, Variable(idxs, requires_grad=False)).max(0)
            xs.append(mx.unsqueeze(0))

        x = torch.cat(xs, dim=0)

        res['classification'] = F.log_softmax(x)
        return res

    def getLoss(self, x, y, res):
        #l = F.nll_loss(res['classification'], y['target'], size_average=False)
        #l = F.nll_loss(res['classification'], y['target'], size_average=False)
        l = F.cross_entropy(res['classification'], y['target'], size_average=False)

        ## entropy criterion on phi
        #lH = Entropy()(res['phi'].view(-1, args.nr * args.nc)).mean(1).mean(0)
        lH = 0

        l -= (args.entropyLambda) * lH # maximize entropy
        return l



class ShapeImgForegroundNet(nn.Module):
    """ Shape only. No NN after.
    """
    def __init__(self):
        super(ShapeImgForegroundNet, self).__init__()
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.cclShape = tu.modules.ccl.ShapeImgModule(self.nshapes, self.thetaNet.ndim)

        nodim = self.cclShape.ndim
        self.fcs = SimpleNN([nodim, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb = x0.size(0)

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = inp['actualSeg']

        res = self.thetaNet(phi, x)

        theta =  self.cclShape(res['probs'], res['clusters'],
                               res['shapefvs'], res['contentfvs'], res['sizes'])

        x = self.fcs(theta)

        # thetas: nodes x thetadim
        ## reorganize nodes by batch and pick the best supporting node
        xs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            mx,_ = x.index_select(0, Variable(idxs, requires_grad=False)).max(0)
            xs.append(mx.unsqueeze(0))

        x = torch.cat(xs, dim=0)

        res['classification'] = F.log_softmax(x)
        return res



class ShapeImgNN3ForegroundNet(nn.Module, LossMClassification, VisualizeMCC):
    """ Shape only. No NN after.
    """
    def __init__(self):
        super(ShapeImgNN3ForegroundNet, self).__init__()
        self.nphi = 30
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.cclShape = tu.modules.ccl.ShapeImgModule(self.nshapes, self.thetaNet.ndim)
        self.shapeFVNet = tu.modules.ccl.KernFVNet(args.nr, args.nc, args.deg)

        nodim = self.cclShape.ndim
        self.fcs = SimpleNN([nodim, 128, 256, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb, nch, nr, nc = x0.size()

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = inp['actualSeg']

        res = self.thetaNet(phi, x)
        res['shapefvs'] = self.shapeFVNet(res['clusters'].view(-1, nr, nc), res['sizes'])
        theta, bbs =  self.cclShape(res['probs'], res['clusters'])




        fcsTheta = self.fcs(theta)
        res['fcsTheta'] = fcsTheta
        ccClassProbs = fcsTheta # nodes * nclasses

        ## reorganize nodes by batch and pick the best supporting node
        classProbs = []
        segs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            ## for each image, generate for each label the corresponding selection
            ## associated with it

            assert(idxs.nelement() > 0)
            idxs = Variable(idxs, requires_grad=False)

            batchClassProb = ccClassProbs.index_select(0, idxs) # nodes * nclasses
            mx,_ = batchClassProb.max(0)
            batchClassProb = mx.unsqueeze(0)
            classProbs.append(batchClassProb)

            if False:
                ## compute the weight associated with each cluster: lsm
                cWeight = ccClassProbs.index_select(0, idxs) # nodes * nclasses
                cWeight = F.softmax(cWeight) # nodes * nclasses
                cWeight = cWeight.unsqueeze(2).unsqueeze(3) # nodes * nclasses * nr * nc


                ## Prob that a point is in the cluster: sigmoid
                pCluster = res['probs'].index_select(0, idxs)
                pCluster = F.sigmoid(pCluster*10).view(pCluster.size(0), nr, nc) # nodes * nr * nc

                #pCluster = (res['clusters'].index_select(0, idxs) + 1)/2

                pCluster = pCluster.view(-1, 1, nr, nc) # nodex * nclasses * nr * nc

                # combine
                seg = (cWeight * pCluster).max(0)[0] # nclasses * nr * nc
                segs.append(seg.unsqueeze(0))
            else:
                segs.append(inp['actualSeg'].narrow(0,bi,1).repeat(1,22,1,1))

        classProbs = torch.cat(classProbs, dim=0)
        segs = torch.cat(segs, dim=0)
        res['classification'] = F.log_softmax(classProbs)
        res['theta'] = theta
        res['phi'] = phi
        res['bbs'] = bbs
        res['segs'] = segs
        return res



class ShapeImgNN3PhiNet(nn.Module):
    """ Take the image, generate phi and the corresponding clusters
    """
    def __init__(self):
        super(ShapeImgNN3PhiNet, self).__init__()
        self.nphi = 30
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.cclShape = tu.modules.ccl.ShapeImgModule(self.nshapes, self.nphi)

        self.phiNet = SimpleCNN([args.nch, 128, 256, self.nphi])

        nodim = self.cclShape.ndim
        self.fcs = SimpleNN([nodim, 128, 256, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb, nch, nr, nc = x0.size()

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = F.sigmoid(self.phiNet(x0))
        res = self.thetaNet(phi, x)
        theta, bbs =  self.cclShape(res['probs'], res['clusters'])


        fcsTheta = self.fcs(theta)
        res['fcsTheta'] = fcsTheta
        ccClassProbs = F.softmax(fcsTheta) # nodes * nclasses
        #ccClassProbs = self.fcs(theta) # nodes * nclasses

        ## reorganize nodes by batch and pick the best supporting node
        classProbs = []
        segs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            ## for each image, generate for each label the corresponding selection
            ## associated with it

            assert(idxs.nelement() > 0)
            idxs = Variable(idxs, requires_grad=False)

            # average across probs of each cc
            wts = (res['sizes'].index_select(0, idxs).float() / (nr * nc)).unsqueeze(1)

            batchClassProb = (ccClassProbs.index_select(0, idxs) * wts).sum(0, keepdim=True) # nclasses
            batchClassProb = batchClassProb / batchClassProb.sum(1, keepdim=True)

            classProbs.append(batchClassProb) # nb * nclasses


            if True:
                ## compute the weight associated with each cluster: lsm
                cWeight = ccClassProbs.index_select(0, idxs) # nodes * nclasses
                cWeight = F.softmax(cWeight) # nodes * nclasses
                cWeight = cWeight.unsqueeze(2).unsqueeze(3) # nodes * nclasses * nr * nc


                ## Prob that a point is in the cluster: sigmoid
                pCluster = res['probs'].index_select(0, idxs)
                pCluster = F.sigmoid(pCluster*10).view(pCluster.size(0), nr, nc) # nodes * nr * nc

                #pCluster = (res['clusters'].index_select(0, idxs) + 1)/2

                pCluster = pCluster.view(-1, 1, nr, nc) # nodex * nclasses * nr * nc

                # combine
                seg = (cWeight * pCluster).max(0)[0] # nclasses * nr * nc
                segs.append(seg.unsqueeze(0))


        classProbs = torch.cat(classProbs, dim=0).log()
        #classProbs /= classProbs.sum(1, keepdim=True)  # normalized prob
        segs = torch.cat(segs, dim=0)
        res['classification'] = classProbs
        res['theta'] = theta
        res['phi'] = phi
        res['bbs'] = bbs
        res['segs'] = segs
        return res

    def getLoss(self, x, y, res):
        #l = F.nll_loss(res['classification'], y['target'], size_average=False)
        #l = F.nll_loss(res['classification'], y['target'], size_average=False)
        l = F.cross_entropy(res['classification'], y['target'], size_average=False)

        ## entropy criterion on phi
        #lH = Entropy()(res['phi'].view(-1, args.nr * args.nc)).mean(1).mean(0)
        lH = 0

        l -= (args.entropyLambda) * lH # maximize entropy
        return l


    def visualize(self, res, fname):
        clusters = res['clusters'].data
        nb, nk, nr, nc = res['phi'].data.size()
        nt = self.thetaNet.cclThresh.thresholds.data.size(0)
        classnames = builtins.args.classnames

        if True: # visualize the segmentation
            allimgs = []
            for bi in range(nb):
                origImg = res['origdata'].data.select(0, bi).permute(1,2,0)
                actualSeg = res['actualSeg'].data.select(0, bi).permute(1,2,0)
                actualClass = res['target'].data[bi]
                predClass = res['classification'].data.max(1)[1][bi]

                def selClass(img, ci):
                    if ci == actualClass: img = padImg(img, fill=[0,1,0])
                    else: img = padImg(img, fill=[0.1,0.1,0.1])

                    if ci == predClass: img = padImg(img, fill=[1,1,0])
                    else: img = padImg(img, fill=[0.1,0.1,0.1])

                    return img

                if bi == 0:
                    ## header
                    def txtImg(txt):
                        img = makeImg(setupImg(res['segs'].data.select(1,0).select(0,bi)*0))
                        draw = ImageDraw.Draw(img)
                        font = ImageFont.truetype("FreeMono.ttf", 12)
                        draw.text((0, 0),txt,(255,255,255),font=font)
                        img = torchvision.transforms.ToTensor()(img).permute(1,2,0).cuda()
                        img = padImg(img, fill=[0.1,0.1,0.1])
                        img = padImg(img, fill=[0.1,0.1,0.1])
                        return img

                    hdr = hstackImgs([origImg*0, actualSeg*0] + [txtImg(x) for x in classnames])
                    allimgs.append(padImg(hdr, fill=[0,0,1]))

                tmp = hstackImgs([origImg, actualSeg] + [selClass(res['segs'].data.select(1,ci).select(0,bi), ci) for ci in range(args.nclasses)])
                tmp2 = hstackImgs([origImg, actualSeg] + [selClass(normImg(res['segs'].data.select(1,ci).select(0,bi)), ci) for ci in range(args.nclasses)])
                allimgs.append(padImg(vstackImgs([tmp, tmp2]), fill=[0,0,1]))
            allimgs = vstackImgs(allimgs)
            allimgs = makeImg(allimgs)
            allimgs.save(os.path.join(args.outDir, fname+'_seg.png'))
            del allimgs
            gc.collect()



        uniqueBatches = numpy.unique(res['batchIndex'].data.cpu().numpy())
        if True:
            clusters = (clusters >= 0).float()
            imgs = {}
            for i in range(clusters.size(0)):
                bi = res['batchIndex'].data[i]
                ci = res['clusterIndex'].data[i]
                pi = (res['clusterIndex'].data[i] // nt) % nk
                ti = res['clusterIndex'].data[i] % nt

                origPhi = res['phi'].data.select(1,pi).select(0, bi)
                origImg = res['origdata'].data.select(0, bi).permute(1,2,0)
                origCluster = res['actualSeg'].data.select(0, bi)
                phiImg = res['probs'].data.select(0, i).view(nr, nc)
                clusterImg = res['clusters'].data.select(0,i).view(nr, nc)
                thetaImg = res['theta'].data.select(0,i).view(self.cclShape.width, self.cclShape.width)
                origImg = drawBB(nn.ZeroPad2d(self.cclShape.padWidth)(origImg.permute(2,0,1).unsqueeze(0)).data.squeeze(0).permute(1,2,0), res['bbs'].data.select(0,i)+self.cclShape.padWidth, fill=[1,1,0])


                if (bi,pi, ti) not in imgs: imgs[(bi,pi,ti)] = []
                imgs[(bi, pi, ti)].append(padImg(hstackImgs([origImg,
                                                             origCluster, clusterImg,
                                                             origPhi, phiImg, normImg(phiImg),
                                                             thetaImg, normImg(thetaImg)]), padWidth=1, fill=[0,0,0.5]))
                #imgs[(bi, pi)].append(hstackImgs([origImg]))

            # combine images that are of a particular batch, phi and threshold
            for k in imgs: imgs[k] = padImg(vstackImgs(imgs[k]), fill=[0,1,0])

            # combine images that are in a given threshold
            for bi in range(nb):
                for pi in range(nk):
                    timgs = []
                    for ti in range(nt):
                        if (bi, pi, ti) in imgs:
                            timgs.append(imgs[(bi, pi, ti)])
                    if len(timgs) == 0: continue
                    timgs = padImg(vstackImgs(timgs), fill=[1,0,0])
                    imgs[(bi,pi)] = timgs
                    for ti in range(nt):
                        if (bi, pi, ti) in imgs:
                            del imgs[(bi, pi, ti)]

            gc.collect()
            nimgs = []
            for bi in range(min(2, nb)): #nb):
                nimgs.append( hstackImgs([v for k,v in imgs.items() if k[0] == bi]) )
            del imgs
            imgs = vstackImgs(nimgs)
            del nimgs
            gc.collect()
            imgs = makeImg(imgs)
            imgs.save(os.path.join(args.outDir, fname+'.png'))

            del imgs
            gc.collect()

class ShapeBankFVForegroundNet(nn.Module):
    """ Shape only. No NN after.
    """
    def __init__(self):
        super(ShapeBankFVForegroundNet, self).__init__()
        self.deg = args.deg
        self.nshapes = args.nshapes
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.cclShape = tu.modules.ccl.CclShapeTemplateModule(self.nshapes, self.thetaNet.ndim)
        self.sm = nn.Softmax()

        nodim = self.thetaNet.ndim
        self.fcs = SimpleNN([nodim, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb = x0.size(0)

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = inp['actualSeg']

        res = self.thetaNet(phi, x)

        theta, llikelihood =  self.cclShape(res['probs'], res['clusters'],
                                            res['shapefvs'], res['contentfvs'], res['sizes'])
        ncl = llikelihood.size(0)
        llikelihood = llikelihood.view(ncl, args.nclasses, args.nshapes // args.nclasses)
        sm = self.sm(llikelihood.view(ncl * args.nclasses, args.nshapes // args.nclasses)).view(ncl, args.nclasses, args.nshapes // args.nclasses)
        theta = theta.view(ncl, theta.size(1), args.nclasses, args.nshapes // args.nclasses)

        theta = (theta * sm.unsqueeze(1)).sum(3)

        llikelihood = (llikelihood * sm).sum(2)
        x = llikelihood

        #x = self.fcs(theta)

        # thetas: nodes x thetadim
        ## reorganize nodes by batch and pick the best supporting node
        xs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            mx,_ = x.index_select(0, Variable(idxs, requires_grad=False)).max(0)
            xs.append(mx.unsqueeze(0))

        x = torch.cat(xs, dim=0)

        res['classification'] = F.log_softmax(x)
        return res



def getComparisonVector(res, idxs):
    idxs = Variable(idxs, requires_grad=False)

    # ## compute the weight associated with each cluster: lsm
    # cWeight = res['ccFV'].index_select(0, idxs) # nodes * nclasses
    # cWeight = F.softmax(cWeight) # nodes * nclasses
    # cWeight = cWeight.unsqueeze(2).unsqueeze(3) # nodes * nclasses * nr * nc

    # ## Prob that a point is in the cluster: sigmoid
    # pCluster = res['probs'].index_select(0, idxs)
    # pCluster = F.sigmoid(pCluster*10).view(pCluster.size(0), nr, nc) # nodes * nr * nc

    # #pCluster = (res['clusters'].index_select(0, idxs) + 1)/2
    # pCluster = pCluster.view(-1, 1, nr, nc) # nodex * nclasses * nr * nc

    # # combine
    # seg = (cWeight * pCluster).max(0)[0] # nclasses * nr * nc
    # segs.append(seg.unsqueeze(0))

    fv = res['ccFV'].index_select(0, idxs)
    return fv


class ShapeImgNN3PhiMatchNet(nn.Module):
    """ Take the image, generate phi and the corresponding clusters
    """
    def __init__(self):
        super(ShapeImgNN3PhiMatchNet, self).__init__()
        self.nphi = 16
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.cclShape = tu.modules.ccl.ShapeImgModule(self.nshapes, 16)

        self.phiNet = SimpleCNN([args.nch, 128, 256, self.nphi])
        self.imgFVNet = SimpleCNN([args.nch, 128, 256])

        nodim = self.cclShape.ndim
        self.fcs = SimpleNN([nodim, 128, 256])#, args.nclasses])
        self.pd = PairwiseDiffL1()

    def forward(self, inp):
        x0 = inp['data']
        nb, nmatch, nch, nr, nc = x0.size()
        x0 = x0.view(nb*nmatch, nch, nr, nc)
        x = x0
        x = x.permute(0,2,3,1).contiguous()

        res = {}


        phi = F.sigmoid(self.phiNet(x0)).unsqueeze(1)
        imgFVs = self.imgFVNet(x0).unsqueeze(2)

        imgs = (imgFVs * phi).sum(4).sum(3) / phi.sum(4).sum(3)
        imgs = imgs.view(nb ,nmatch, imgs.size(1), imgs.size(2)) # nb * nmatch * dims * phis

        # l1 diff
        img1 = imgs.narrow(1,0,1)
        img2s = imgs.narrow(1,1,nmatch-1)

        diff = (img1 - img2s).abs().mean(2).mean(2)

        batchMatchScores = F.log_softmax(-diff)
        res['matches'] = batchMatchScores
        return res


    def getLoss(self, x, y, res):
        l = F.nll_loss(res['matches'], y['targetMatch'], size_average=False)
        return l

    def visualize(self, res, fname):
        sels = res['matches'].data.max(1)[1]
        imgs = []
        for bi in range(res['data'].size(0)):
            timgs = []
            for ii in range(res['data'].size(1)):
                img = res['data'].data[bi][ii].permute(1,2,0)
                if ii == (sels[bi]+1):
                    img = padImg(img, fill=[1,0,0])
                timgs.append(img)
            imgs.append(hstackImgs(timgs))

        imgs = vstackImgs(imgs)
        imgs = makeImg(imgs)
        imgs.save(os.path.join(args.outDir, fname+'_match.png'))



class AveragePolyKernNN3PhiMatchNet(nn.Module):
    """ Take the image, generate phi and the corresponding clusters
    """
    def __init__(self):
        super(AveragePolyKernNN3PhiMatchNet, self).__init__()
        self.nphi = 16
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.cclShape = tu.modules.ccl.ShapeAverageModule(self.nshapes, self.thetaNet.ndim)

        self.phiNet = SimpleCNN([args.nch, 128, 256, self.nphi])

        nodim = self.cclShape.ndim
        self.fcs = SimpleNN([nodim, 128, 256])#, args.nclasses])
        self.pd = PairwiseDiffL1()

    def forward(self, inp):
        x0 = inp['data']
        nb, nmatch, nch, nr, nc = x0.size()
        x0 = x0.view(nb*nmatch, nch, nr, nc)
        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = F.sigmoid(self.phiNet(x0))

        res = self.thetaNet(phi, x)

        theta, bbs =  self.cclShape(res['probs'], res['clusters'],
                                    res['shapefvs'], res['contentfvs'], res['sizes'])

        ccClassProbs = self.fcs(theta) # nodes * nclasses

        res['ccFV'] = ccClassProbs
        ## reorganize nodes by batch and pick the best supporting node
        classProbs = []
        segs = []
        batchMatchScores = []
        for bi in range(nb):
            # 0 : bi*nmatch
            idxs0  = torch.nonzero(res['batchIndex'].data == (bi*nmatch)).view(-1)

            ## extract comparison clusters
            c0ComparisonVector = getComparisonVector(res, idxs0) # ncc0 * ndim

            ## extract individual components and generate matrix
            matchScores = []
            for mi in range(1, nmatch):
                idxsm = torch.nonzero(res['batchIndex'].data == (bi*nmatch + mi)).view(-1)

                ## for each image, generate for each label the corresponding selection
                ## associated with it
                cMComparisonVector = getComparisonVector(res, idxsm) # ncc1 * ndim

                ## pairwise difference.
                matchMatrix = self.pd(c0ComparisonVector, cMComparisonVector) # ncc0 * ncc1

                # independent matching
                matchScore = matchMatrix.min(1)[0].mean(0) # 1
                matchScores.append(matchScore)


            matchScores = torch.cat(matchScores, dim=0).unsqueeze(0) # nmatch - 1

            #matchScores = F.softmax(-matchScores).unsqueeze(0)
            batchMatchScores.append(matchScores)

        batchMatchScores = torch.cat(batchMatchScores, dim=0)
        batchMatchScores = F.softmax(-batchMatchScores)
        res['matches'] = batchMatchScores
        return res

    def visualize(self, res, fname):
        sels = res['matches'].data.max(1)[1]
        imgs = []
        for bi in range(res['data'].size(0)):
            timgs = []
            for ii in range(res['data'].size(1)):
                img = res['data'].data[bi][ii].permute(1,2,0)
                if ii == (sels[bi]+1):
                    img = padImg(img, fill=[1,0,0])
                timgs.append(img)
            imgs.append(hstackImgs(timgs))

        imgs = vstackImgs(imgs)
        imgs = makeImg(imgs)
        imgs.save(os.path.join(args.outDir, fname+'_match.png'))



class AverageShapeFVNN3ForegroundNet2(nn.Module):
    """ Shape only. No NN after.
    Do not do softmax stuff at end
    """
    def __init__(self):
        super(AverageShapeFVNN3ForegroundNet2, self).__init__()
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.cclShape = tu.modules.ccl.ShapeAverageModule(self.nshapes, self.thetaNet.ndim)

        nodim = self.thetaNet.ndim
        self.fcs = SimpleNN([self.thetaNet.ndim, 128, 256, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb, nch, nr, nc = x0.size()

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = inp['actualSeg']

        res = self.thetaNet(phi, x)

        theta, llikelihood =  self.cclShape(res['probs'], res['clusters'],
                                            res['shapefvs'], res['contentfvs'], res['sizes'])

        fcsTheta = self.fcs(theta)
        res['fcsTheta'] = fcsTheta
        ccClassProbs = fcsTheta # nodes * nclasses

        #x = self.fcs(theta)

        # thetas: nodes x thetadim
        ## reorganize nodes by batch and pick the best supporting node
        classProbs = []
        segs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            ## for each image, generate for each label the corresponding selection
            ## associated with it

            assert(idxs.nelement() > 0)
            idxs = Variable(idxs, requires_grad=False)

            # average across probs of each cc
            #wts = (res['sizes'].index_select(0, idxs).float() / (nr * nc)).unsqueeze(1)
            #batchClassProb = (ccClassProbs.index_select(0, idxs) * wts).sum(0, keepdim=True) # nclasses
            # batchClassProb = batchClassProb / batchClassProb.sum(1, keepdim=True)

            #batchClassProb = ccClassProbs.index_select(0, idxs).max(0, keepdim=True)[0]
            batchClassProb = ccClassProbs.index_select(0, idxs).sum(0, keepdim=True)

            classProbs.append(batchClassProb) # nb * nclasses


            if False:
                ## compute the weight associated with each cluster: lsm
                cWeight = ccClassProbs.index_select(0, idxs) # nodes * nclasses
                cWeight = F.softmax(cWeight) # nodes * nclasses
                cWeight = cWeight.unsqueeze(2).unsqueeze(3) # nodes * nclasses * nr * nc


                ## Prob that a point is in the cluster: sigmoid
                pCluster = res['probs'].index_select(0, idxs)
                pCluster = F.sigmoid(pCluster*10).view(pCluster.size(0), nr, nc) # nodes * nr * nc

                #pCluster = (res['clusters'].index_select(0, idxs) + 1)/2

                pCluster = pCluster.view(-1, 1, nr, nc) # nodex * nclasses * nr * nc

                # combine
                seg = (cWeight * pCluster).max(0)[0] # nclasses * nr * nc
                segs.append(seg.unsqueeze(0))
            else:
                segs.append(inp['actualSeg'].narrow(0,bi,1).repeat(1,22,1,1))


        classProbs = torch.cat(classProbs, dim=0)
        #classProbs /= classProbs.sum(1, keepdim=True)  # normalized prob
        segs = torch.cat(segs, dim=0)
        res['classification'] = F.log_softmax(classProbs)
        res['theta'] = theta
        res['phi'] = phi
        res['segs'] = segs
        return res

    def getLoss(self, x, y, res):
        l = F.nll_loss(res['classification'], y['target'], size_average=False)
        return l

    def visualize(self, res, fname):
        clusters = res['clusters'].data
        nb, nk, nr, nc = res['phi'].data.size()
        nt = self.thetaNet.cclThresh.thresholds.data.size(0)
        classnames = builtins.args.classnames

        if True: # visualize the segmentation
            allimgs = []
            for bi in range(nb):
                origImg = res['origdata'].data.select(0, bi).permute(1,2,0)
                actualSeg = res['actualSeg'].data.select(0, bi).permute(1,2,0)
                actualClass = res['target'].data[bi]
                predClass = res['classification'].data.max(1)[1][bi]

                def selClass(img, ci):
                    if ci == actualClass: img = padImg(img, fill=[0,1,0])
                    else: img = padImg(img, fill=[0.1,0.1,0.1])

                    if ci == predClass: img = padImg(img, fill=[1,1,0])
                    else: img = padImg(img, fill=[0.1,0.1,0.1])

                    return img

                if bi == 0:
                    ## header
                    def txtImg(txt):
                        img = makeImg(setupImg(res['segs'].data.select(1,0).select(0,bi)*0))
                        draw = ImageDraw.Draw(img)
                        font = ImageFont.truetype("FreeMono.ttf", 12)
                        draw.text((0, 0),txt,(255,255,255),font=font)
                        img = torchvision.transforms.ToTensor()(img).permute(1,2,0).cuda()
                        img = padImg(img, fill=[0.1,0.1,0.1])
                        img = padImg(img, fill=[0.1,0.1,0.1])
                        return img

                    hdr = hstackImgs([origImg*0, actualSeg*0] + [txtImg(x) for x in classnames])
                    allimgs.append(padImg(hdr, fill=[0,0,1]))

                tmp = hstackImgs([origImg, actualSeg] + [selClass(res['segs'].data.select(1,ci).select(0,bi), ci) for ci in range(args.nclasses)])
                tmp2 = hstackImgs([origImg, actualSeg] + [selClass(normImg(res['segs'].data.select(1,ci).select(0,bi)), ci) for ci in range(args.nclasses)])
                allimgs.append(padImg(vstackImgs([tmp, tmp2]), fill=[0,0,1]))
            allimgs = vstackImgs(allimgs)
            allimgs = makeImg(allimgs)
            allimgs.save(os.path.join(args.outDir, fname+'_seg.png'))
            del allimgs
            gc.collect()



        uniqueBatches = numpy.unique(res['batchIndex'].data.cpu().numpy())
        if False:
            clusters = (clusters >= 0).float()
            imgs = {}
            for i in range(clusters.size(0)):
                bi = res['batchIndex'].data[i]
                ci = res['clusterIndex'].data[i]
                pi = (res['clusterIndex'].data[i] // nt) % nk
                ti = res['clusterIndex'].data[i] % nt

                origPhi = res['phi'].data.select(1,pi).select(0, bi)
                origImg = res['origdata'].data.select(0, bi).permute(1,2,0)
                origCluster = res['actualSeg'].data.select(0, bi)
                phiImg = res['probs'].data.select(0, i).view(nr, nc)
                clusterImg = res['clusters'].data.select(0,i).view(nr, nc)
                thetaImg = res['theta'].data.select(0,i).view(self.cclShape.width, self.cclShape.width)
                origImg = drawBB(nn.ZeroPad2d(self.cclShape.padWidth)(origImg.permute(2,0,1).unsqueeze(0)).data.squeeze(0).permute(1,2,0), res['bbs'].data.select(0,i)+self.cclShape.padWidth, fill=[1,1,0])


                if (bi,pi, ti) not in imgs: imgs[(bi,pi,ti)] = []
                imgs[(bi, pi, ti)].append(padImg(hstackImgs([origImg,
                                                             origCluster, clusterImg,
                                                             origPhi, phiImg, normImg(phiImg),
                                                             thetaImg, normImg(thetaImg)]), padWidth=1, fill=[0,0,0.5]))
                #imgs[(bi, pi)].append(hstackImgs([origImg]))

            # combine images that are of a particular batch, phi and threshold
            for k in imgs: imgs[k] = padImg(vstackImgs(imgs[k]), fill=[0,1,0])

            # combine images that are in a given threshold
            for bi in range(nb):
                for pi in range(nk):
                    timgs = []
                    for ti in range(nt):
                        if (bi, pi, ti) in imgs:
                            timgs.append(imgs[(bi, pi, ti)])
                    if len(timgs) == 0: continue
                    timgs = padImg(vstackImgs(timgs), fill=[1,0,0])
                    imgs[(bi,pi)] = timgs
                    for ti in range(nt):
                        if (bi, pi, ti) in imgs:
                            del imgs[(bi, pi, ti)]

            gc.collect()
            nimgs = []
            for bi in range(min(2, nb)): #nb):
                nimgs.append( hstackImgs([v for k,v in imgs.items() if k[0] == bi]) )
            del imgs
            imgs = vstackImgs(nimgs)
            del nimgs
            gc.collect()
            imgs = makeImg(imgs)
            imgs.save(os.path.join(args.outDir, fname+'.png'))

            del imgs
            gc.collect()





class ShapeImgNN3ForegroundNet3(nn.Module):
    """ Shape img. Do not do softmax stuff in end
    """
    def __init__(self):
        super(ShapeImgNN3ForegroundNet3, self).__init__()
        self.nphi = 30
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNetNoCC(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.cclShape = tu.modules.ccl.ShapeImgModule2(self.nshapes, self.nphi)

        self.phiNet = SimpleCNN([args.nch, 128, 256, self.nphi])

        nodim = self.cclShape.ndim
        self.fcs = SimpleNN([nodim, 128, 256, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb, nch, nr, nc = x0.size()

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = inp['actualSeg']
        res = self.thetaNet(phi, x)
        theta, bbs =  self.cclShape(res['probs'], res['clusters'],
                                    res['shapefvs'], res['contentfvs'], res['sizes'])

        ## Theta will be between 0 and 1
        theta = theta.clamp(min=1e-9).log() - (1-theta).clamp(min=1e-9).log() # logit


        fcsTheta = self.fcs(theta)
        res['fcsTheta'] = fcsTheta
        ccClassProbs = fcsTheta # nodes * nclasses
        #ccClassProbs = self.fcs(theta) # nodes * nclasses

        ## reorganize nodes by batch and pick the best supporting node
        classProbs = []
        segs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            ## for each image, generate for each label the corresponding selection
            ## associated with it

            assert(idxs.nelement() > 0)
            idxs = Variable(idxs, requires_grad=False)

            batchClassProb = ccClassProbs.index_select(0, idxs).sum(0, keepdim=True) # nclasses

            classProbs.append(batchClassProb) # nb * nclasses


            if False:
                ## compute the weight associated with each cluster: lsm
                cWeight = ccClassProbs.index_select(0, idxs) # nodes * nclasses
                cWeight = F.softmax(cWeight) # nodes * nclasses
                cWeight = cWeight.unsqueeze(2).unsqueeze(3) # nodes * nclasses * nr * nc


                ## Prob that a point is in the cluster: sigmoid
                pCluster = res['probs'].index_select(0, idxs)
                pCluster = F.sigmoid(pCluster*10).view(pCluster.size(0), nr, nc) # nodes * nr * nc

                #pCluster = (res['clusters'].index_select(0, idxs) + 1)/2

                pCluster = pCluster.view(-1, 1, nr, nc) # nodex * nclasses * nr * nc

                # combine
                seg = (cWeight * pCluster).max(0)[0] # nclasses * nr * nc
                segs.append(seg.unsqueeze(0))
            else:
                segs.append(inp['actualSeg'].narrow(0,bi,1).repeat(1,22,1,1))


        classProbs = torch.cat(classProbs, dim=0)
        #classProbs /= classProbs.sum(1, keepdim=True)  # normalized prob
        segs = torch.cat(segs, dim=0)
        res['classification'] = F.log_softmax(classProbs)
        res['theta'] = theta
        res['phi'] = phi
        res['bbs'] = bbs
        res['segs'] = segs
        return res

    def getLoss(self, x, y, res):
        l = F.nll_loss(res['classification'], y['target'], size_average=False)
        return l


    def visualize(self, res, fname):
        clusters = res['clusters'].data
        nb, nk, nr, nc = res['phi'].data.size()
        classnames = builtins.args.classnames

        if True: # visualize the segmentation
            allimgs = []
            for bi in range(nb):
                origImg = res['origdata'].data.select(0, bi).permute(1,2,0)
                actualSeg = res['actualSeg'].data.select(0, bi).permute(1,2,0)
                actualClass = res['target'].data[bi]
                predClass = res['classification'].data.max(1)[1][bi]

                def selClass(img, ci):
                    if ci == actualClass: img = padImg(img, fill=[0,1,0])
                    else: img = padImg(img, fill=[0.1,0.1,0.1])

                    if ci == predClass: img = padImg(img, fill=[1,1,0])
                    else: img = padImg(img, fill=[0.1,0.1,0.1])

                    return img

                if bi == 0:
                    ## header
                    def txtImg(txt):
                        img = makeImg(setupImg(res['segs'].data.select(1,0).select(0,bi)*0))
                        draw = ImageDraw.Draw(img)
                        font = ImageFont.truetype("FreeMono.ttf", 12)
                        draw.text((0, 0),txt,(255,255,255),font=font)
                        img = torchvision.transforms.ToTensor()(img).permute(1,2,0).cuda()
                        img = padImg(img, fill=[0.1,0.1,0.1])
                        img = padImg(img, fill=[0.1,0.1,0.1])
                        return img

                    hdr = hstackImgs([origImg*0, actualSeg*0] + [txtImg(x) for x in classnames])
                    allimgs.append(padImg(hdr, fill=[0,0,1]))

                tmp = hstackImgs([origImg, actualSeg] + [selClass(res['segs'].data.select(1,ci).select(0,bi), ci) for ci in range(args.nclasses)])
                tmp2 = hstackImgs([origImg, actualSeg] + [selClass(normImg(res['segs'].data.select(1,ci).select(0,bi)), ci) for ci in range(args.nclasses)])
                allimgs.append(padImg(vstackImgs([tmp, tmp2]), fill=[0,0,1]))
            allimgs = vstackImgs(allimgs)
            allimgs = makeImg(allimgs)
            allimgs.save(os.path.join(args.outDir, fname+'_seg.png'))
            del allimgs
            gc.collect()



        uniqueBatches = numpy.unique(res['batchIndex'].data.cpu().numpy())
        if True:
            clusters = (clusters >= 0).float()
            imgs = {}
            for i in range(clusters.size(0)):
                bi = res['batchIndex'].data[i]
                #ci = res['clusterIndex'].data[i]
                pi = res['phiIndex'].data[i]
                ti = 0

                origPhi = res['phi'].data.select(1,pi).select(0, bi)
                origImg = res['origdata'].data.select(0, bi).permute(1,2,0)
                origCluster = res['actualSeg'].data.select(0, bi)
                phiImg = res['probs'].data.select(0, i).view(nr, nc)
                clusterImg = res['clusters'].data.select(0,i).view(nr, nc)
                thetaImg = res['theta'].data.select(0,i).view(self.cclShape.width, self.cclShape.width)
                origImg = drawBB(nn.ZeroPad2d(self.cclShape.padWidth)(origImg.permute(2,0,1).unsqueeze(0)).data.squeeze(0).permute(1,2,0), res['bbs'].data.select(0,i)+self.cclShape.padWidth, fill=[1,1,0])


                if (bi,pi, ti) not in imgs: imgs[(bi,pi,ti)] = []
                imgs[(bi, pi, ti)].append(padImg(hstackImgs([origImg,
                                                             origCluster, clusterImg,
                                                             origPhi, phiImg, normImg(phiImg),
                                                             thetaImg, normImg(thetaImg)]), padWidth=1, fill=[0,0,0.5]))
                #imgs[(bi, pi)].append(hstackImgs([origImg]))

            # combine images that are of a particular batch, phi and threshold
            for k in imgs: imgs[k] = padImg(vstackImgs(imgs[k]), fill=[0,1,0])

            # combine images that are in a given threshold
            for bi in range(nb):
                for pi in range(nk):
                    timgs = []
                    for ti in [0]:
                        if (bi, pi, ti) in imgs:
                            timgs.append(imgs[(bi, pi, ti)])
                    if len(timgs) == 0: continue
                    timgs = padImg(vstackImgs(timgs), fill=[1,0,0])
                    imgs[(bi,pi)] = timgs
                    for ti in [0]:
                        if (bi, pi, ti) in imgs:
                            del imgs[(bi, pi, ti)]

            gc.collect()
            nimgs = []
            for bi in range(min(2, nb)): #nb):
                nimgs.append( hstackImgs([v for k,v in imgs.items() if k[0] == bi]) )
            del imgs
            imgs = vstackImgs(nimgs)
            del nimgs
            gc.collect()
            imgs = makeImg(imgs)
            imgs.save(os.path.join(args.outDir, fname+'.png'))

            del imgs
            gc.collect()






class ShapeContextImgNN3PhiNet(nn.Module):
    """ Take the image, generate phi and the corresponding clusters
    """
    def __init__(self):
        super(ShapeImgNN3PhiNet, self).__init__()
        self.nphi = 30
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.cclShape = tu.modules.ccl.ShapeImgModule(self.nshapes, self.nphi)

        self.contextNet = ContextFV([args.nch, 64, 64, 64])
        self.shapeMaskNet = SimpleCNN([64, 128, 256, self.nphi])

        nodim = self.cclShape.ndim
        self.fcs = SimpleNN([nodim, 128, 256, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        m0 = inp['mask']
        nb, nch, nr, nc = x0.size()

        x = x0 * m0
        x = x.permute(0,2,3,1).contiguous()
        #contextFV = self.contextNet(x0) # batch * ndim * nr * nc
        phi = F.sigmoid(self.phiNet(x0))

        res = self.thetaNet(phi, x)
        theta, bbs =  self.cclShape(res['probs'], res['clusters'])


        fcsTheta = self.fcs(theta)
        res['fcsTheta'] = fcsTheta
        ccClassProbs = F.softmax(fcsTheta) # nodes * nclasses
        #ccClassProbs = self.fcs(theta) # nodes * nclasses

        ## reorganize nodes by batch and pick the best supporting node
        classProbs = []
        segs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            ## for each image, generate for each label the corresponding selection
            ## associated with it

            assert(idxs.nelement() > 0)
            idxs = Variable(idxs, requires_grad=False)

            # average across probs of each cc
            wts = (res['sizes'].index_select(0, idxs).float() / (nr * nc)).unsqueeze(1)

            batchClassProb = (ccClassProbs.index_select(0, idxs) * wts).sum(0, keepdim=True) # nclasses
            batchClassProb = batchClassProb / batchClassProb.sum(1, keepdim=True)

            classProbs.append(batchClassProb) # nb * nclasses


            if True:
                ## compute the weight associated with each cluster: lsm
                cWeight = ccClassProbs.index_select(0, idxs) # nodes * nclasses
                cWeight = F.softmax(cWeight) # nodes * nclasses
                cWeight = cWeight.unsqueeze(2).unsqueeze(3) # nodes * nclasses * nr * nc


                ## Prob that a point is in the cluster: sigmoid
                pCluster = res['probs'].index_select(0, idxs)
                pCluster = F.sigmoid(pCluster*10).view(pCluster.size(0), nr, nc) # nodes * nr * nc

                #pCluster = (res['clusters'].index_select(0, idxs) + 1)/2

                pCluster = pCluster.view(-1, 1, nr, nc) # nodex * nclasses * nr * nc

                # combine
                seg = (cWeight * pCluster).max(0)[0] # nclasses * nr * nc
                segs.append(seg.unsqueeze(0))


        classProbs = torch.cat(classProbs, dim=0).log()
        #classProbs /= classProbs.sum(1, keepdim=True)  # normalized prob
        segs = torch.cat(segs, dim=0)
        res['classification'] = classProbs
        res['theta'] = theta
        res['phi'] = phi
        res['bbs'] = bbs
        res['segs'] = segs
        return res

    def getLoss(self, x, y, res):
        #l = F.nll_loss(res['classification'], y['target'], size_average=False)
        #l = F.nll_loss(res['classification'], y['target'], size_average=False)
        l = F.cross_entropy(res['classification'], y['target'], size_average=False)

        ## entropy criterion on phi
        #lH = Entropy()(res['phi'].view(-1, args.nr * args.nc)).mean(1).mean(0)
        lH = 0

        l -= (args.entropyLambda) * lH # maximize entropy
        return l


    def visualize(self, res, fname):
        clusters = res['clusters'].data
        nb, nk, nr, nc = res['phi'].data.size()
        nt = self.thetaNet.cclThresh.thresholds.data.size(0)
        classnames = builtins.args.classnames

        if True: # visualize the segmentation
            allimgs = []
            for bi in range(nb):
                origImg = res['origdata'].data.select(0, bi).permute(1,2,0)
                actualSeg = res['actualSeg'].data.select(0, bi).permute(1,2,0)
                actualClass = res['target'].data[bi]
                predClass = res['classification'].data.max(1)[1][bi]

                def selClass(img, ci):
                    if ci == actualClass: img = padImg(img, fill=[0,1,0])
                    else: img = padImg(img, fill=[0.1,0.1,0.1])

                    if ci == predClass: img = padImg(img, fill=[1,1,0])
                    else: img = padImg(img, fill=[0.1,0.1,0.1])

                    return img

                if bi == 0:
                    ## header
                    def txtImg(txt):
                        img = makeImg(setupImg(res['segs'].data.select(1,0).select(0,bi)*0))
                        draw = ImageDraw.Draw(img)
                        font = ImageFont.truetype("FreeMono.ttf", 12)
                        draw.text((0, 0),txt,(255,255,255),font=font)
                        img = torchvision.transforms.ToTensor()(img).permute(1,2,0).cuda()
                        img = padImg(img, fill=[0.1,0.1,0.1])
                        img = padImg(img, fill=[0.1,0.1,0.1])
                        return img

                    hdr = hstackImgs([origImg*0, actualSeg*0] + [txtImg(x) for x in classnames])
                    allimgs.append(padImg(hdr, fill=[0,0,1]))

                tmp = hstackImgs([origImg, actualSeg] + [selClass(res['segs'].data.select(1,ci).select(0,bi), ci) for ci in range(args.nclasses)])
                tmp2 = hstackImgs([origImg, actualSeg] + [selClass(normImg(res['segs'].data.select(1,ci).select(0,bi)), ci) for ci in range(args.nclasses)])
                allimgs.append(padImg(vstackImgs([tmp, tmp2]), fill=[0,0,1]))
            allimgs = vstackImgs(allimgs)
            allimgs = makeImg(allimgs)
            allimgs.save(os.path.join(args.outDir, fname+'_seg.png'))
            del allimgs
            gc.collect()



        uniqueBatches = numpy.unique(res['batchIndex'].data.cpu().numpy())
        if True:
            clusters = (clusters >= 0).float()
            imgs = {}
            for i in range(clusters.size(0)):
                bi = res['batchIndex'].data[i]
                ci = res['clusterIndex'].data[i]
                pi = (res['clusterIndex'].data[i] // nt) % nk
                ti = res['clusterIndex'].data[i] % nt

                origPhi = res['phi'].data.select(1,pi).select(0, bi)
                origImg = res['origdata'].data.select(0, bi).permute(1,2,0)
                origCluster = res['actualSeg'].data.select(0, bi)
                phiImg = res['probs'].data.select(0, i).view(nr, nc)
                clusterImg = res['clusters'].data.select(0,i).view(nr, nc)
                thetaImg = res['theta'].data.select(0,i).view(self.cclShape.width, self.cclShape.width)
                origImg = drawBB(nn.ZeroPad2d(self.cclShape.padWidth)(origImg.permute(2,0,1).unsqueeze(0)).data.squeeze(0).permute(1,2,0), res['bbs'].data.select(0,i)+self.cclShape.padWidth, fill=[1,1,0])


                if (bi,pi, ti) not in imgs: imgs[(bi,pi,ti)] = []
                imgs[(bi, pi, ti)].append(padImg(hstackImgs([origImg,
                                                             origCluster, clusterImg,
                                                             origPhi, phiImg, normImg(phiImg),
                                                             thetaImg, normImg(thetaImg)]), padWidth=1, fill=[0,0,0.5]))
                #imgs[(bi, pi)].append(hstackImgs([origImg]))

            # combine images that are of a particular batch, phi and threshold
            for k in imgs: imgs[k] = padImg(vstackImgs(imgs[k]), fill=[0,1,0])

            # combine images that are in a given threshold
            for bi in range(nb):
                for pi in range(nk):
                    timgs = []
                    for ti in range(nt):
                        if (bi, pi, ti) in imgs:
                            timgs.append(imgs[(bi, pi, ti)])
                    if len(timgs) == 0: continue
                    timgs = padImg(vstackImgs(timgs), fill=[1,0,0])
                    imgs[(bi,pi)] = timgs
                    for ti in range(nt):
                        if (bi, pi, ti) in imgs:
                            del imgs[(bi, pi, ti)]

            gc.collect()
            nimgs = []
            for bi in range(min(2, nb)): #nb):
                nimgs.append( hstackImgs([v for k,v in imgs.items() if k[0] == bi]) )
            del imgs
            imgs = vstackImgs(nimgs)
            del nimgs
            gc.collect()
            imgs = makeImg(imgs)
            imgs.save(os.path.join(args.outDir, fname+'.png'))

            del imgs
            gc.collect()

class ShapeContextImgNN3PhiMatchNet(nn.Module):
    """ Take the image, generate phi and the corresponding clusters
    """
    def __init__(self):
        super(ShapeContextImgNN3PhiMatchNet, self).__init__()
        self.nphi = 16
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)

        self.contextKerns = [args.nch, 64, 64, 64]
        self.contextNet = ContextFV(self.contextKerns)
        self.costKerns = [sum(self.contextKerns), 128, 256]
        self.costFVNet = SimpleNN(self.costKerns)

        self.minCostMatching = MinCostMatching()


    def forward(self, inp):
        x0 = inp['data']
        m = inp['actualSeg']
        nb, nmatch, nch, nr, nc = x0.size()

        x0 = x0 * m # multiply with mask
        x0 = x0.view(nb*nmatch, nch, nr, nc)

        rsampSource = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= 0.1)).select(1,0), requires_grad=False) # sample 10%
        rsampTarget = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= 0.2)).select(1,0), requires_grad=False) # sample 20%

        res = {}

        ## get contextual feature map
        contextFV = self.contextNet(x0) # (nb * nmatch) * ndim * nr * nc
        contextFV = contextFV.permute(0,2,3,1).contiguous().view(nb*nmatch*nr*nc, self.costKerns[0])

        costFV = self.costFVNet(contextFV) # all * ncostdim

        ## sample the pixels. Using identity for now
        costFV = costFV.view(nb,nmatch, nr*nc, self.costKerns[-1])

        ## compute matching score
        sourceFVs = costFV.narrow(1,0,1).index_select(2,rsampSource).contiguous()  # nb * 1 * (nr * nc) * ndim
        targetFVs = costFV.narrow(1,1,args.nPositiveExamples + args.nNegativeExamples).index_select(2,rsampTarget).contiguous()

        matchCost, pdiff = self.minCostMatching(sourceFVs, targetFVs) # nb * 1 * (nmatch-1)
        matchCost = matchCost.view(matchCost.size(0), matchCost.size(2))

        res['pdiff'] = pdiff
        res['matchCost'] = matchCost
        res['costFV'] = costFV
        batchMatchScores = F.log_softmax(-matchCost)
        res['matches'] = batchMatchScores
        return res

    def getLoss(self, x, y, res):
        l = F.nll_loss(res['matches'], y['targetMatch'], size_average=False)
        ## l1 regularization
        lreg = 1e-3 * torch.nn.utils.clip_grad.clip_grad_norm(self.parameters(), 0.1, 1)
        lnorm = 1e-3 * sum([x.abs().sum() for x in self.parameters()])
        res['l'] = l
        res['lreg'] = lreg
        res['lnorm'] = lnorm
        return l + lreg + lnorm

    def visualizeMask(self, res, fname):
        """
        show inp img, mask and mask * img
        """
        sels = res['matches'].data.max(1)[1]
        imgs = []
        for bi in range(res['origdata'].size(0)):
            timgs = []
            for ii in range(res['origdata'].size(1)):
                img = vstackImgs([res['origdata'].data[bi][ii].permute(1,2,0),
                                  res['mask'].data[bi][ii].permute(1,2,0),
                                  res['origdata'].data[bi][ii].permute(1,2,0) * res['mask'].data[bi][ii].permute(1,2,0)])
                if ii == (sels[bi]+1):
                    img = padImg(img, fill=[1,0,0])
                timgs.append(img)
            imgs.append(hstackImgs(timgs))

        imgs = vstackImgs(imgs)
        imgs = makeImg(imgs)
        imgs.save(os.path.join(args.outDir, fname+'_match.png'))

    def visualizeActualSeg(self, res, fname):
        """
        show inp img, mask and mask * img
        """
        sels = res['matches'].data.max(1)[1]
        imgs = []
        for bi in range(res['data'].size(0)):
            timgs = []
            for ii in range(res['data'].size(1)):
                img = vstackImgs([yuv2rgb(res['data'].data[bi][ii].permute(1,2,0)),
                                  res['actualSeg'].data[bi][ii].permute(1,2,0),
                                  yuv2rgb(res['data'].data[bi][ii].permute(1,2,0)) * res['actualSeg'].data[bi][ii].permute(1,2,0)])
                if ii == (sels[bi]+1):
                    img = padImg(img, fill=[1,0,0])
                timgs.append(img)
            imgs.append(hstackImgs(timgs))

        imgs = vstackImgs(imgs)
        imgs = makeImg(imgs)
        imgs.save(os.path.join(args.outDir, fname+'_match.png'))

    def visualize(self, *args):
        return self.visualizeActualSeg(*args)


class AverageFVNN3MatchNet(nn.Module):
    """ Take the image, generate phi and the corresponding clusters
    """
    def __init__(self):
        super(AverageFVNN3MatchNet, self).__init__()
        self.nphi = 16
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNetNoCC(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.shapeFVNet = tu.modules.ccl.KernFVNet(args.nr, args.nc, args.deg)
        self.cclShape = tu.modules.ccl.ShapeAverageModule(self.nshapes, self.thetaNet.ndim)


        contextKerns = [args.nch, 64, 64, 64]
        self.costFVNet = SimpleNN([self.shapeFVNet.ndim, 128])

        self.minCostMatching = tu.modules.ccl.PairwiseDiffL1()


    def forward(self, inp):
        x0 = inp['data']
        m = inp['actualSeg']
        nb, nmatch, nch, nr, nc = x0.size()

        x0 = x0.view(nb*nmatch, nch, nr, nc)
        phi = inp['actualSeg'].view(nb * nmatch , 1, nr, nc)
        res = self.thetaNet(phi, x0.permute(0,2,3,1))
        res['shapefvs'] = self.shapeFVNet(res['clusters'].view(nb*nmatch, nr, nc), res['sizes'])
        theta, llikelihood =  self.cclShape(res['probs'], res['clusters'],
                                            res['shapefvs'], res['contentfvs'], res['sizes'])

        theta = self.costFVNet(theta) # (nb * nmatch) * ndim
        costFV = theta.view(nb, nmatch, theta.size(1)) # nb * nmatch * ndim


        ## compute matching score
        sourceFVs = costFV.narrow(1,0,1).contiguous()  # nb * 1 * (nr * nc) * ndim
        targetFVs = costFV.narrow(1,1,args.nPositiveExamples + args.nNegativeExamples).contiguous()

        matchCost = self.minCostMatching(sourceFVs, targetFVs) # nb * 1 * (nmatch-1)
        matchCost = matchCost.view(matchCost.size(0), matchCost.size(2))

        res['matchCost'] = matchCost
        res['costFV'] = costFV
        batchMatchScores = F.log_softmax(-matchCost)
        res['matches'] = batchMatchScores
        return res

    def getLoss(self, x, y, res):
        l = F.nll_loss(res['matches'], y['targetMatch'], size_average=False)
        ## l1 regularization
        lreg = 1e-3 * torch.nn.utils.clip_grad.clip_grad_norm(self.parameters(), 0.1, 1)
        lnorm = 1e-3 * sum([x.abs().sum() for x in self.parameters()])
        res['l'] = l
        res['lreg'] = lreg
        res['lnorm'] = lnorm
        return l # + lreg + lnorm

    def visualizeMask(self, res, fname):
        """
        show inp img, mask and mask * img
        """
        sels = res['matches'].data.max(1)[1]
        imgs = []
        for bi in range(res['data'].size(0)):
            timgs = []
            for ii in range(res['data'].size(1)):
                img = vstackImgs([res['data'].data[bi][ii].permute(1,2,0),
                                  res['mask'].data[bi][ii].permute(1,2,0),
                                  res['data'].data[bi][ii].permute(1,2,0) * res['mask'].data[bi][ii].permute(1,2,0)])
                if ii == (sels[bi]+1):
                    img = padImg(img, fill=[1,0,0])
                timgs.append(img)
            imgs.append(hstackImgs(timgs))

        imgs = vstackImgs(imgs)
        imgs = makeImg(imgs)
        imgs.save(os.path.join(args.outDir, fname+'_match.png'))

    def visualizeActualSeg(self, res, fname):
        """
        show inp img, mask and mask * img
        """
        sels = res['matches'].data.max(1)[1]
        imgs = []
        for bi in range(res['data'].size(0)):
            timgs = []
            for ii in range(res['data'].size(1)):
                img = vstackImgs([res['data'].data[bi][ii].permute(1,2,0),
                                  res['actualSeg'].data[bi][ii].permute(1,2,0),
                                  res['data'].data[bi][ii].permute(1,2,0) * res['actualSeg'].data[bi][ii].permute(1,2,0)])
                if ii == (sels[bi]+1):
                    img = padImg(img, fill=[1,0,0])
                timgs.append(img)
            imgs.append(padImg(hstackImgs(timgs), fill=[0,0,1]))

        imgs = vstackImgs(imgs)
        imgs = makeImg(imgs)
        imgs.save(os.path.join(args.outDir, fname+'_match.png'))

    def visualize(self, *args):
        return self.visualizeActualSeg(*args)




class SiameseMatchNet(nn.Module):
    """ Take the image, generate phi and the corresponding clusters
    """
    def __init__(self):
        super(SiameseMatchNet, self).__init__()
        self.nphi = 16
        self.deg = args.deg
        self.nshapes = 30

        self.nkernCNN = [args.nch, 64, 128, 256]
        self.nkernLL = [(args.nr // 2**len(self.nkernCNN[2:])) * (args.nc // 2**len(self.nkernCNN[2:])) * self.nkernCNN[-1], 512, 512]

        self.cnn = SimpleCNNPooling(self.nkernCNN)
        self.ll = SimpleNN(self.nkernLL)

        self.minCostMatching = tu.modules.ccl.PairwiseDiffL1()


    def forward(self, inp):
        x0 = inp['data']
        m = inp['actualSeg']
        nb, nmatch, nch, nr, nc = x0.size()

        x0 = x0.view(nb*nmatch, nch, nr, nc)
        x = self.cnn(x0)
        costFV = self.ll(x.view(nb*nmatch, self.nkernLL[0]))
        costFV = costFV.view(nb, nmatch, self.nkernLL[-1])

        res = {}

        ## compute matching score
        sourceFVs = costFV.narrow(1,0,1).contiguous()  # nb * 1 * (nr * nc) * ndim
        targetFVs = costFV.narrow(1,1,args.nPositiveExamples + args.nNegativeExamples).contiguous()

        matchCost = self.minCostMatching(sourceFVs, targetFVs) # nb * 1 * (nmatch-1)
        matchCost = matchCost.view(matchCost.size(0), matchCost.size(2))

        res['matchCost'] = matchCost
        res['costFV'] = costFV
        batchMatchScores = F.log_softmax(-matchCost)
        res['matches'] = batchMatchScores
        return res

    def getLoss(self, x, y, res):
        l = F.nll_loss(res['matches'], y['targetMatch'], size_average=False)
        ## l1 regularization
        lreg = 1e-3 * torch.nn.utils.clip_grad.clip_grad_norm(self.parameters(), 0.1, 1)
        lnorm = 1e-3 * sum([x.abs().sum() for x in self.parameters()])
        res['l'] = l
        res['lreg'] = lreg
        res['lnorm'] = lnorm
        return l # + lreg + lnorm

    def visualizeMask(self, res, fname):
        """
        show inp img, mask and mask * img
        """
        sels = res['matches'].data.max(1)[1]
        imgs = []
        for bi in range(res['data'].size(0)):
            timgs = []
            for ii in range(res['data'].size(1)):
                img = vstackImgs([res['data'].data[bi][ii].permute(1,2,0),
                                  res['mask'].data[bi][ii].permute(1,2,0),
                                  res['data'].data[bi][ii].permute(1,2,0) * res['mask'].data[bi][ii].permute(1,2,0)])
                if ii == (sels[bi]+1):
                    img = padImg(img, fill=[1,0,0])
                timgs.append(img)
            imgs.append(hstackImgs(timgs))

        imgs = vstackImgs(imgs)
        imgs = makeImg(imgs)
        imgs.save(os.path.join(args.outDir, fname+'_match.png'))

    def visualizeActualSeg(self, res, fname):
        """
        show inp img, mask and mask * img
        """
        sels = res['matches'].data.max(1)[1]
        imgs = []
        for bi in range(res['data'].size(0)):
            timgs = []
            for ii in range(res['data'].size(1)):
                img = vstackImgs([res['data'].data[bi][ii].permute(1,2,0),
                                  res['actualSeg'].data[bi][ii].permute(1,2,0),
                                  res['data'].data[bi][ii].permute(1,2,0) * res['actualSeg'].data[bi][ii].permute(1,2,0)])
                if ii == (sels[bi]+1):
                    img = padImg(img, fill=[1,0,0])
                timgs.append(img)
            imgs.append(padImg(hstackImgs(timgs), fill=[0,0,1]))

        imgs = vstackImgs(imgs)
        imgs = makeImg(imgs)
        imgs.save(os.path.join(args.outDir, fname+'_match.png'))

    def visualize(self, *args):
        return self.visualizeActualSeg(*args)




class AverageShapeFVNN3ForegroundNet3(nn.Module, LossMClassification, VisualizeMCC):
    """ Actual segmentation -> Connected components -> PolyKern -> Average in cluster -> simple NN classifier

    Summing in prob space

    Training:
    ---------

    CUDA_VISIBLE_DEVICES=1 python trainSeg.py --batch-size 2 --dataset MNIST --model AverageShapeFVNN3ForegroundNet2 --deg 10 --epochs 250 --vis-interval 200 --lr 0.0001 --outDir /home/pareshmg/outs/logs/testseg

    """
    def __init__(self):
        super(AverageShapeFVNN3ForegroundNet3, self).__init__()
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.shapeFVNet = tu.modules.ccl.KernFVNet(args.nr, args.nc, args.deg)
        self.cclShape = tu.modules.ccl.ShapeAverageModule(self.nshapes, self.thetaNet.ndim)

        nodim = self.thetaNet.ndim

        self.fcs = SimpleNN([self.thetaNet.ndim, 128, 256, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb, nch, nr, nc = x0.size()

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = inp['actualSeg']

        res = self.thetaNet(phi, x)
        res['shapefvs'] = self.shapeFVNet(res['clusters'].view(-1, nr, nc), res['sizes'])
        theta =  self.cclShape(res['probs'], res['clusters'],
                               res['shapefvs'], res['contentfvs'], res['sizes'])

        ## Theta will be between 0 and 1
        #theta = theta.clamp(min=1e-9).log() - (1-theta).clamp(min=1e-9).log() # logit

        fcsTheta = self.fcs(theta)
        res['fcsTheta'] = fcsTheta
        ccClassProbs = fcsTheta # nodes * nclasses

        ## reorganize nodes by batch and pick the best supporting node
        classProbs = []
        segs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            ## for each image, generate for each label the corresponding selection
            ## associated with it

            assert(idxs.nelement() > 0)
            idxs = Variable(idxs, requires_grad=False)

            batchClassProb = ccClassProbs.index_select(0, idxs) # nodes * nclasses
            batchClassProb = batchClassProb.exp()
            batchClassProb = batchClassProb.sum(0, keepdim=True) / batchClassProb.sum()


            classProbs.append(batchClassProb) # nb * nclasses


            if False:
                ## compute the weight associated with each cluster: lsm
                cWeight = ccClassProbs.index_select(0, idxs) # nodes * nclasses
                cWeight = F.softmax(cWeight) # nodes * nclasses
                cWeight = cWeight.unsqueeze(2).unsqueeze(3) # nodes * nclasses * nr * nc


                ## Prob that a point is in the cluster: sigmoid
                pCluster = res['probs'].index_select(0, idxs)
                pCluster = F.sigmoid(pCluster*10).view(pCluster.size(0), nr, nc) # nodes * nr * nc

                #pCluster = (res['clusters'].index_select(0, idxs) + 1)/2

                pCluster = pCluster.view(-1, 1, nr, nc) # nodex * nclasses * nr * nc

                # combine
                seg = (cWeight * pCluster).max(0)[0] # nclasses * nr * nc
                segs.append(seg.unsqueeze(0))
            else:
                segs.append(inp['actualSeg'].narrow(0,bi,1).repeat(1,22,1,1))



        classProbs = torch.cat(classProbs, dim=0)
        segs = torch.cat(segs, dim=0)
        res['classification'] = classProbs.log()
        res['theta'] = theta
        res['phi'] = phi
        res['segs'] = segs
        return res


class ShapeImgNN3PhiNet2(nn.Module, LossMClassification, VisualizeMShapeImgCC):
    """ actual segmentation -> connected components -> shape image -> nn classification

    Summing in prob space

    training:


    """
    def __init__(self):
        super(ShapeImgNN3PhiNet2, self).__init__()
        self.nphi = 30
        self.deg = args.deg
        self.nshapes = 30

        self.contextNet = Encoder(64, 0, args.nch, False, args.nr)
        self.phiNet = SimpleNN([259, 128, self.nphi])

        self.thetaNet = tu.modules.ccl.ShapeNetNoCC(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.shapeFVNet = tu.modules.ccl.KernFVNet(args.nr, args.nc, args.deg)
        self.cclShape = tu.modules.ccl.ShapeImgModule2(self.nshapes, self.nphi)


        nodim = self.cclShape.ndim
        self.fcs = SimpleNN([nodim, 128, 256, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb, nch, nr, nc = x0.size()

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        #phi = inp['actualSeg']

        ## get the contextual pixel vector
        _, contextfv = self.contextNet(x0)
        contextfv = contextfv.permute(0,2,3,1).contiguous()

        ## get the part prob mask
        phi = F.sigmoid(self.phiNet(contextfv).permute(0,3,1,2).contiguous())

        ## create clusters based on phi
        res = self.thetaNet(phi, contextfv)
        res['shapefvs'] = self.shapeFVNet(res['clusters'].view(-1, nr, nc), res['sizes'])
        theta, bbs =  self.cclShape(res['probs'], res['clusters'],
                                    res['shapefvs'], res['contentfvs'], res['sizes'])

        ## Theta will be between 0 and 1
        theta = theta.clamp(min=1e-9).log() - (1-theta).clamp(min=1e-9).log() # logit

        fcsTheta = self.fcs(theta)
        res['fcsTheta'] = fcsTheta
        ccClassProbs = fcsTheta # nodes * nclasses

        ## reorganize nodes by batch and pick the best supporting node
        classProbs = []
        segs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            ## for each image, generate for each label the corresponding selection
            ## associated with it

            assert(idxs.nelement() > 0)
            idxs = Variable(idxs, requires_grad=False)

            batchClassProb = ccClassProbs.index_select(0, idxs) # nodes * nclasses
            batchClassProb = batchClassProb.exp()
            batchClassProb = batchClassProb / batchClassProb.sum()

            if True:
                ## compute the weight associated with each cluster: lsm
                cWeight = batchClassProb
                cWeight = cWeight.unsqueeze(2).unsqueeze(3) # nodes * nclasses * nr * nc


                ## Prob that a point is in the cluster: sigmoid
                pCluster = res['probs'].index_select(0, idxs)
                pCluster = F.sigmoid(pCluster*10).view(pCluster.size(0), nr, nc) # nodes * nr * nc

                #pCluster = (res['clusters'].index_select(0, idxs) + 1)/2

                pCluster = pCluster.view(-1, 1, nr, nc) # nodes * nclasses * nr * nc

                # combine
                seg = (cWeight * pCluster).max(0)[0] # nclasses * nr * nc
                segs.append(seg.unsqueeze(0))
            else:
                segs.append(inp['actualSeg'].narrow(0,bi,1).repeat(1,22,1,1))

            classProbs.append(batchClassProb.sum(0, keepdim=True)) # nb * nclasses


        classProbs = torch.cat(classProbs, dim=0)
        segs = torch.cat(segs, dim=0)
        res['classification'] = classProbs.log()
        res['theta'] = theta
        res['phi'] = phi
        res['bbs'] = bbs
        res['segs'] = segs
        return res


##################################################
##################################################
##################################################
##################################################
##################################################
##################################################
##################################################
##################################################
##################################################
##################################################


def convLayer(in_planes, out_planes, useDropout = False):
    "3x3 convolution with padding"
    seq = nn.Sequential(
        nn.Conv2d(in_planes, out_planes, kernel_size=3,
                  stride=1, padding=1, bias=True),
        nn.BatchNorm2d(out_planes),
        nn.ReLU(True),
        nn.MaxPool2d(kernel_size=2, stride=2)
    )
    if useDropout: # Add dropout module
        list_seq = list(seq.modules())[1:]
        list_seq.append(nn.Dropout(0.1))
        seq = nn.Sequential(*list_seq)

    return seq

class Encoder(nn.Module):
    def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28):
        super(Encoder, self).__init__()

        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """
        self.layer1 = convLayer(num_channels, layer_size, useDropout)
        self.layer2 = convLayer(layer_size, layer_size, useDropout)
        self.layer3 = convLayer(layer_size, layer_size, useDropout)
        self.layer4 = convLayer(layer_size, layer_size, useDropout)

        finalSize = int(math.floor(image_size / (2 * 2 * 2 * 2)))
        self.outSize = finalSize * finalSize * layer_size
        if nClasses>0: # We want a linear
            self.useClassification = True
            self.layer5 = nn.Linear(self.outSize,nClasses)
            self.outSize = nClasses
        else:
            self.useClassification = False

        self.weights_init(self.layer1)
        self.weights_init(self.layer2)
        self.weights_init(self.layer3)
        self.weights_init(self.layer4)
        self.upsample = nn.Upsample(size=(args.nr, args.nc), mode='nearest')

    def weights_init(self,module):
        for m in module.modules():
            if isinstance(m, nn.Conv2d):
                init.xavier_uniform(m.weight, gain=numpy.sqrt(2))
                init.constant(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()


    def forward(self, image_input):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, 1, 28, 28]
        :return: Embeddings of size [batch_size, 64]
        """
        pxe = [image_input]
        x = self.layer1(image_input)
        pxe.append(self.upsample(x))
        x = self.layer2(x)
        pxe.append(self.upsample(x))
        x = self.layer3(x)
        pxe.append(self.upsample(x))
        x = self.layer4(x)
        pxe.append(self.upsample(x))
        x = x.view(x.size(0), -1)
        if self.useClassification:
            x = self.layer5(x)
        return x, torch.cat(pxe, 1)




class EncoderNet(nn.Module, LossMClassification, VisualizeMCC):
    """ Actual segmentation -> Connected components -> PolyKern -> Average in cluster -> simple NN classifier

    Summing in prob space

    Training:
    ---------


    """
    def __init__(self):
        super(EncoderNet, self).__init__()
        self.deg = args.deg
        self.nshapes = 30
        self.contextNet = Encoder(64, 0, 1, False, args.nr)
        self.fcs = SimpleNN([64*(args.nr//2**4)**2, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb, nch, nr, nc = x0.size()

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = inp['actualSeg']

        theta, _ = self.contextNet(x0)

        res = {}
        fcsTheta = self.fcs(theta)
        res['fcsTheta'] = fcsTheta
        ccClassProbs = fcsTheta # nodes * nclasses

        classProbs = ccClassProbs
        res['classification'] = F.log_softmax(classProbs)
        res['theta'] = theta
        res['segs'] = inp['data'].repeat(1,args.nclasses, 1,1)
        return res


class EncoderMatchNet(nn.Module):
    """ Actual segmentation -> Connected components -> PolyKern -> Average in cluster -> simple NN classifier

    Summing in prob space

    Training:
    ---------


    """
    def __init__(self):
        super(EncoderMatchNe, self).__init__()
        self.deg = args.deg
        self.nshapes = 30
        self.contextNet = Encoder(64, 0, 1, False, args.nr)
        self.fcs = SimpleNN([64*(args.nr//2**4)**2, args.nclasses])

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        encoded_images = []
        for i in np.arange(support_set_images.size(1)):
            _, gen_encode = self.contextNet(support_set_images[:,i,:,:,:])
            encoded_images.append(gen_encode)

        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            gen_encode = self.contextNet(target_image[:,i,:,:,:])
            encoded_images.append(gen_encode)
            outputs = torch.stack(encoded_images)

            if self.fce:
                outputs, hn, cn = self.lstm(outputs)

            # get similarity between support set embeddings and target
            similarities = self.dn(support_set=outputs[:-1], input_image=outputs[-1])
            similarities = similarities.t()

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy = accuracy + torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss = accuracy + F.cross_entropy(preds, target_label[:, i].long())

            # delete the last target image encoding of encoded_images
            encoded_images.pop()

        return accuracy/target_image.size(1), crossentropy_loss/target_image.size(1)



class AverageShapeContextFVNN3ForegroundNet2(nn.Module, LossMClassification, VisualizeMCC):
    """ Actual segmentation -> Connected components -> PolyKern -> Average in cluster -> simple NN classifier

    Summing in prob space

    Training:
    ---------


    """
    def __init__(self):
        super(AverageShapeContextFVNN3ForegroundNet2, self).__init__()
        self.deg = args.deg
        self.nshapes = 30
        self.contextNet = Encoder(64, 0, 1, False, args.nr)
        self.thetaNet = tu.modules.ccl.ShapeNetNoCC(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.shapeFVNet = tu.modules.ccl.KernFVNet(args.nr, args.nc, args.deg)
        self.cclShape = tu.modules.ccl.ShapeAverageModule(self.nshapes, self.thetaNet.ndim)

        nodim = self.thetaNet.ndim + 64*(args.nr//2**4)**2

        self.fcs = SimpleNN([nodim, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb, nch, nr, nc = x0.size()

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = inp['actualSeg']

        contextEmb, _ = self.contextNet(x0)

        res = self.thetaNet(phi, x)
        res['shapefvs'] = self.shapeFVNet(res['clusters'].view(-1, nr, nc), res['sizes'])
        theta =  self.cclShape(res['probs'], res['clusters'],
                               res['shapefvs'], res['contentfvs'], res['sizes'])

        ## Theta will be between 0 and 1
        theta = theta.clamp(min=1e-9).log() - (1-theta).clamp(min=1e-9).log() # logit

        theta = torch.cat([theta, contextEmb], dim=1)

        fcsTheta = self.fcs(theta)
        res['fcsTheta'] = fcsTheta
        ccClassProbs = fcsTheta # nodes * nclasses

        ## reorganize nodes by batch and pick the best supporting node
        classProbs = []
        segs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            ## for each image, generate for each label the corresponding selection
            ## associated with it

            assert(idxs.nelement() > 0)
            idxs = Variable(idxs, requires_grad=False)

            batchClassProb = ccClassProbs.index_select(0, idxs) # nodes * nclasses
            batchClassProb = batchClassProb.exp()
            batchClassProb = batchClassProb.sum(0, keepdim=True) / batchClassProb.sum()


            classProbs.append(batchClassProb) # nb * nclasses


            if False:
                ## compute the weight associated with each cluster: lsm
                cWeight = ccClassProbs.index_select(0, idxs) # nodes * nclasses
                cWeight = F.softmax(cWeight) # nodes * nclasses
                cWeight = cWeight.unsqueeze(2).unsqueeze(3) # nodes * nclasses * nr * nc


                ## Prob that a point is in the cluster: sigmoid
                pCluster = res['probs'].index_select(0, idxs)
                pCluster = F.sigmoid(pCluster*10).view(pCluster.size(0), nr, nc) # nodes * nr * nc

                #pCluster = (res['clusters'].index_select(0, idxs) + 1)/2

                pCluster = pCluster.view(-1, 1, nr, nc) # nodex * nclasses * nr * nc

                # combine
                seg = (cWeight * pCluster).max(0)[0] # nclasses * nr * nc
                segs.append(seg.unsqueeze(0))
            else:
                segs.append(inp['actualSeg'].narrow(0,bi,1).repeat(1,22,1,1))



        classProbs = torch.cat(classProbs, dim=0)
        segs = torch.cat(segs, dim=0)
        res['classification'] = classProbs.log()
        res['theta'] = theta
        res['phi'] = phi
        res['segs'] = segs
        return res
