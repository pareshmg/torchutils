### Models that work
import builtins
import tu
import torch
from tu.utils import *
from tu.imageUtils import *
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import gc
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import os, sys
from torch.autograd import Variable, Function


class LossMClassification(LossM):
    def getLoss(self, x, y, res):
        l = F.nll_loss(res['classification'], y['target'].view(-1), size_average=False)
        return l

class VisualizeMShapeImg(VisualizeM):
    def visualize(self, res, fname):
        clusters = res['clusters'].data
        nb, nk, nr, nc = res['phi'].data.size()
        nt = 1
        try :
            nt = self.thetaNet.cclThresh.thresholds.data.size(0)
        except:
            pass
        classnames = builtins.args.classnames
        uniqueBatches = numpy.unique(res['batchIndex'].data.cpu().numpy())
        if True:
            clusters = (clusters >= 0).float()
            imgs = {}
            for i in range(clusters.size(0)):
                bi = res['batchIndex'].data[i]
                ci = res['clusterIndex'].data[i]
                pi = (res['clusterIndex'].data[i] // nt) % nk
                ti = res['clusterIndex'].data[i] % nt

                origPhi = res['phi'].data.select(1,pi).select(0, bi)
                origImg = res['origdata'].data.select(0, bi).permute(1,2,0)
                origCluster = res['actualSeg'].data.select(0, bi)
                phiImg = res['probs'].data.select(0, i).view(nr, nc)
                clusterImg = res['clusters'].data.select(0,i).view(nr, nc)
                thetaImg = res['theta'].data.select(0,i).view(self.cclShape.width, self.cclShape.width)
                origImg = drawBB(nn.ZeroPad2d(self.cclShape.padWidth)(origImg.permute(2,0,1).unsqueeze(0)).data.squeeze(0).permute(1,2,0), res['bbs'].data.select(0,i)+self.cclShape.padWidth, fill=[1,1,0])


                if (bi,pi, ti) not in imgs: imgs[(bi,pi,ti)] = []
                imgs[(bi, pi, ti)].append(padImg(hstackImgs([origImg,
                                                             origCluster, clusterImg,
                                                             origPhi, phiImg, normImg(phiImg),
                                                             thetaImg, normImg(thetaImg)]), padWidth=1, fill=[0,0,0.5]))
                #imgs[(bi, pi)].append(hstackImgs([origImg]))

            # combine images that are of a particular batch, phi and threshold
            for k in imgs: imgs[k] = padImg(vstackImgs(imgs[k]), fill=[0,1,0])

            # combine images that are in a given threshold
            for bi in range(nb):
                for pi in range(nk):
                    timgs = []
                    for ti in range(nt):
                        if (bi, pi, ti) in imgs:
                            timgs.append(imgs[(bi, pi, ti)])
                    if len(timgs) == 0: continue
                    timgs = padImg(vstackImgs(timgs), fill=[1,0,0])
                    imgs[(bi,pi)] = timgs
                    for ti in range(nt):
                        if (bi, pi, ti) in imgs:
                            del imgs[(bi, pi, ti)]

            gc.collect()
            nimgs = []
            for bi in range(min(2, nb)): #nb):
                nimgs.append( hstackImgs([v for k,v in imgs.items() if k[0] == bi]) )
            del imgs
            imgs = vstackImgs(nimgs)
            del nimgs
            gc.collect()
            imgs = makeImg(imgs)
            imgs.save(os.path.join(args.outDir, fname+'.png'))

            del imgs
            gc.collect()

class VisualizeMCC(VisualizeM):
    def visualize(self, res, fname):
        nb, nk, nr, nc = res['data'].data.size()
        classnames = builtins.args.classnames

        if True: # visualize the segmentation
            allimgs = []
            for bi in range(nb):
                origImg = res['origdata'].data.select(0, bi).permute(1,2,0)
                actualSeg = res['actualSeg'].data.select(0, bi).permute(1,2,0)
                actualClass = res['target'].data.view(-1)[bi]
                predClass = res['classification'].data.max(1)[1][bi]

                def selClass(img, ci):
                    if ci == actualClass: img = padImg(img, fill=[0,1,0])
                    else: img = padImg(img, fill=[0.1,0.1,0.1])

                    if ci == predClass: img = padImg(img, fill=[1,1,0])
                    else: img = padImg(img, fill=[0.1,0.1,0.1])

                    return img

                if bi == 0:
                    ## header
                    def txtImg(txt):
                        img = makeImg(setupImg(res['data'].data.select(1,0).select(0,bi)*0))
                        draw = ImageDraw.Draw(img)
                        font = ImageFont.truetype("FreeMono.ttf", 12)
                        draw.text((0, 0),txt,(255,255,255),font=font)
                        img = torchvision.transforms.ToTensor()(img).permute(1,2,0).cuda()
                        img = padImg(img, fill=[0.1,0.1,0.1])
                        img = padImg(img, fill=[0.1,0.1,0.1])
                        return img

                    hdr = hstackImgs([origImg*0, actualSeg*0] + [txtImg(x) for x in classnames])
                    allimgs.append(padImg(hdr, fill=[0,0,1]))

                tmp = hstackImgs([origImg, actualSeg] + [selClass(res['segs'].data.select(1,ci).select(0,bi), ci) for ci in range(args.nclasses)])
                tmp2 = hstackImgs([origImg, actualSeg] + [selClass(normImg(res['segs'].data.select(1,ci).select(0,bi)), ci) for ci in range(args.nclasses)])
                allimgs.append(padImg(vstackImgs([tmp, tmp2]), fill=[0,0,1]))
            allimgs = vstackImgs(allimgs)
            allimgs = makeImg(allimgs)
            allimgs.save(os.path.join(args.outDir, fname+'_seg.png'))
            del allimgs
            gc.collect()




class AverageShapeFVNN3ForegroundNet(nn.Module, LossMClassification, VisualizeMCC):
    """ Actual segmentation -> Connected components -> PolyKern -> Average in cluster -> simple NN classifier

    Training:
    ---------
    183e44c9a08368371fb8e053e7c4f16638f1e97b
    CUDA_VISIBLE_DEVICES=0 python trainSeg.py --batch-size 2 --dataset MNIST --model AverageShapeFVNN3ForegroundNet --deg 10 --epochs 250 --vis-interval 200 --lr 0.0001 --outDir /home/pareshmg/outs/logs/testseg

    """
    def __init__(self):
        super(AverageShapeFVNN3ForegroundNet, self).__init__()
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.shapeFVNet = tu.modules.ccl.KernFVNet(args.nr, args.nc, args.deg)
        self.cclShape = tu.modules.ccl.ShapeAverageModule(self.nshapes, self.thetaNet.ndim)

        nodim = self.thetaNet.ndim

        self.fcs = SimpleNN([self.thetaNet.ndim, 128, 256, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb, nch, nr, nc = x0.size()

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = inp['actualSeg']

        res = self.thetaNet(phi, x)
        res['shapefvs'] = self.shapeFVNet(res['clusters'].view(-1, nr, nc), res['sizes'])
        theta =  self.cclShape(res['probs'], res['clusters'],
                               res['shapefvs'], res['contentfvs'], res['sizes'])

        fcsTheta = self.fcs(theta)
        res['fcsTheta'] = fcsTheta
        ccClassProbs = F.softmax(fcsTheta) # nodes * nclasses

        #x = self.fcs(theta)

        # thetas: nodes x thetadim
        ## reorganize nodes by batch and pick the best supporting node
        classProbs = []
        segs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            ## for each image, generate for each label the corresponding selection
            ## associated with it

            assert(idxs.nelement() > 0)
            idxs = Variable(idxs, requires_grad=False)

            # average across probs of each cc
            wts = (res['sizes'].index_select(0, idxs).float() / (nr * nc)).unsqueeze(1)

            batchClassProb = (ccClassProbs.index_select(0, idxs) * wts).sum(0, keepdim=True) # nclasses
            batchClassProb = batchClassProb / batchClassProb.sum(1, keepdim=True)

            classProbs.append(batchClassProb) # nb * nclasses


            if True:
                ## compute the weight associated with each cluster: lsm
                cWeight = ccClassProbs.index_select(0, idxs) # nodes * nclasses
                cWeight = F.softmax(cWeight) # nodes * nclasses
                cWeight = cWeight.unsqueeze(2).unsqueeze(3) # nodes * nclasses * nr * nc


                ## Prob that a point is in the cluster: sigmoid
                pCluster = res['probs'].index_select(0, idxs)
                pCluster = F.sigmoid(pCluster*10).view(pCluster.size(0), nr, nc) # nodes * nr * nc

                #pCluster = (res['clusters'].index_select(0, idxs) + 1)/2

                pCluster = pCluster.view(-1, 1, nr, nc) # nodex * nclasses * nr * nc

                # combine
                seg = (cWeight * pCluster).max(0)[0] # nclasses * nr * nc
                segs.append(seg.unsqueeze(0))


        classProbs = torch.cat(classProbs, dim=0).log()
        #classProbs /= classProbs.sum(1, keepdim=True)  # normalized prob
        segs = torch.cat(segs, dim=0)
        res['classification'] = classProbs
        res['theta'] = theta
        res['phi'] = phi
        res['segs'] = segs
        return res




class ShapeImgNN3ForegroundNet2(nn.Module, LossMClassification, VisualizeMCC):
    """ actual segmentation -> connected components -> shape image -> nn classification

    Summing in prob space

    training:
    cec83076cf7c9967154a4633914c3cb81d797ec1
    CUDA_VISIBLE_DEVICES=1 python trainSeg.py --batch-size 2 --dataset MNIST --model ShapeImgNN3ForegroundNet2 --deg 10 --epochs 250 --vis-interval 200 --lr 0.0001 --outDir /home/pareshmg/outs/logs/testseg

    """
    def __init__(self):
        super(ShapeImgNN3ForegroundNet2, self).__init__()
        self.nphi = 30
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.cclShape = tu.modules.ccl.ShapeImgModule2(self.nshapes, self.nphi)
        self.shapeFVNet = tu.modules.ccl.KernFVNet(args.nr, args.nc, args.deg)

        nodim = self.cclShape.ndim
        self.fcs = SimpleNN([nodim, 128, 256, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb, nch, nr, nc = x0.size()

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = inp['actualSeg']

        res = self.thetaNet(phi, x)
        res['shapefvs'] = self.shapeFVNet(res['clusters'].view(-1, nr, nc), res['sizes'])
        theta, bbs =  self.cclShape(res['probs'], res['clusters'],
                                    res['shapefvs'], res['contentfvs'], res['sizes'])

        ## Theta will be between 0 and 1
        theta = theta.clamp(min=1e-9).log() - (1-theta).clamp(min=1e-9).log() # logit

        fcsTheta = self.fcs(theta)
        res['fcsTheta'] = fcsTheta
        ccClassProbs = fcsTheta # nodes * nclasses

        ## reorganize nodes by batch and pick the best supporting node
        classProbs = []
        segs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            ## for each image, generate for each label the corresponding selection
            ## associated with it

            assert(idxs.nelement() > 0)
            idxs = Variable(idxs, requires_grad=False)

            batchClassProb = ccClassProbs.index_select(0, idxs) # nodes * nclasses
            batchClassProb = batchClassProb.exp()
            batchClassProb = batchClassProb.sum(0, keepdim=True) / batchClassProb.sum()


            classProbs.append(batchClassProb) # nb * nclasses


            if False:
                ## compute the weight associated with each cluster: lsm
                cWeight = ccClassProbs.index_select(0, idxs) # nodes * nclasses
                cWeight = F.softmax(cWeight) # nodes * nclasses
                cWeight = cWeight.unsqueeze(2).unsqueeze(3) # nodes * nclasses * nr * nc


                ## Prob that a point is in the cluster: sigmoid
                pCluster = res['probs'].index_select(0, idxs)
                pCluster = F.sigmoid(pCluster*10).view(pCluster.size(0), nr, nc) # nodes * nr * nc

                #pCluster = (res['clusters'].index_select(0, idxs) + 1)/2

                pCluster = pCluster.view(-1, 1, nr, nc) # nodex * nclasses * nr * nc

                # combine
                seg = (cWeight * pCluster).max(0)[0] # nclasses * nr * nc
                segs.append(seg.unsqueeze(0))
            else:
                segs.append(inp['actualSeg'].narrow(0,bi,1).repeat(1,22,1,1))



        classProbs = torch.cat(classProbs, dim=0)
        segs = torch.cat(segs, dim=0)
        res['classification'] = classProbs.log()
        res['theta'] = theta
        res['phi'] = phi
        res['bbs'] = bbs
        res['segs'] = segs
        return res


class AverageShapeFVNN3ForegroundNet2(nn.Module, LossMClassification, VisualizeMCC):
    """ Actual segmentation -> Connected components -> PolyKern -> Average in cluster -> simple NN classifier

    Summing in prob space

    Training:
    ---------

    CUDA_VISIBLE_DEVICES=1 python trainSeg.py --batch-size 2 --dataset MNIST --model AverageShapeFVNN3ForegroundNet2 --deg 10 --epochs 250 --vis-interval 200 --lr 0.0001 --outDir /home/pareshmg/outs/logs/testseg

    """
    def __init__(self):
        super(AverageShapeFVNN3ForegroundNet2, self).__init__()
        self.deg = args.deg
        self.nshapes = 30
        self.thetaNet = tu.modules.ccl.ShapeNet(args.nr, args.nc, self.nshapes, deg = args.deg)
        self.shapeFVNet = tu.modules.ccl.KernFVNet(args.nr, args.nc, args.deg)
        self.cclShape = tu.modules.ccl.ShapeAverageModule(self.nshapes, self.thetaNet.ndim)

        nodim = self.thetaNet.ndim

        self.fcs = SimpleNN([self.thetaNet.ndim, 128, 256, args.nclasses])

    def forward(self, inp):
        x0 = inp['data']
        nb, nch, nr, nc = x0.size()

        x = x0
        x = x.permute(0,2,3,1).contiguous()
        phi = inp['actualSeg']

        res = self.thetaNet(phi, x)
        res['shapefvs'] = self.shapeFVNet(res['clusters'].view(-1, nr, nc), res['sizes'])
        theta =  self.cclShape(res['probs'], res['clusters'],
                               res['shapefvs'], res['contentfvs'], res['sizes'])

        ## Theta will be between 0 and 1
        theta = theta.clamp(min=1e-9).log() - (1-theta).clamp(min=1e-9).log() # logit

        fcsTheta = self.fcs(theta)
        res['fcsTheta'] = fcsTheta
        ccClassProbs = fcsTheta # nodes * nclasses

        ## reorganize nodes by batch and pick the best supporting node
        classProbs = []
        segs = []
        for bi in range(nb):
            idxs = torch.nonzero(res['batchIndex'].data == bi).view(-1)
            ## for each image, generate for each label the corresponding selection
            ## associated with it

            assert(idxs.nelement() > 0)
            idxs = Variable(idxs, requires_grad=False)

            batchClassProb = ccClassProbs.index_select(0, idxs) # nodes * nclasses
            batchClassProb = batchClassProb.exp()
            batchClassProb = batchClassProb.sum(0, keepdim=True) / batchClassProb.sum()


            classProbs.append(batchClassProb) # nb * nclasses


            if False:
                ## compute the weight associated with each cluster: lsm
                cWeight = ccClassProbs.index_select(0, idxs) # nodes * nclasses
                cWeight = F.softmax(cWeight) # nodes * nclasses
                cWeight = cWeight.unsqueeze(2).unsqueeze(3) # nodes * nclasses * nr * nc


                ## Prob that a point is in the cluster: sigmoid
                pCluster = res['probs'].index_select(0, idxs)
                pCluster = F.sigmoid(pCluster*10).view(pCluster.size(0), nr, nc) # nodes * nr * nc

                #pCluster = (res['clusters'].index_select(0, idxs) + 1)/2

                pCluster = pCluster.view(-1, 1, nr, nc) # nodex * nclasses * nr * nc

                # combine
                seg = (cWeight * pCluster).max(0)[0] # nclasses * nr * nc
                segs.append(seg.unsqueeze(0))
            else:
                segs.append(inp['actualSeg'].narrow(0,bi,1).repeat(1,22,1,1))



        classProbs = torch.cat(classProbs, dim=0)
        segs = torch.cat(segs, dim=0)
        res['classification'] = classProbs.log()
        res['theta'] = theta
        res['phi'] = phi
        res['segs'] = segs
        return res



class VisualizeMShapeImgCC(VisualizeM):
    def visualize(self, res, fname):
        VisualizeMShapeImg.visualize(self, res, fname)
        VisualizeMCC.visualize(self, res, fname)
