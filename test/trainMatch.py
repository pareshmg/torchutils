from __future__ import print_function
import builtins
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from loadargs import *


import logging
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import tu.modules.ccl
from torchvision import datasets, transforms
from torch.autograd import Variable
import ipdb
from tqdm import tqdm
import os, sys
import numpy
import gc

from dset import *
from dsetMatch import *
from models import *

from tu.imageUtils import *


##################################################
## Set up dataset

train_loader = None
test_loader = None



if args.dataset == 'MNIST':
    args.nr, args.nc, args.nch = 28, 28, 1
    args.nclasses = 10
    train_loader = torch.utils.data.DataLoader(
        MNISTMatch(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.1307,), (0.3081,)))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        MNISTMatch(args.dataDir, train=False,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.1307,), (0.3081,)))
            ])),
        batch_size=args.batch_size, shuffle=True)
elif args.dataset == 'FASHIONMNIST':
    args.nr, args.nc, args.nch = 28, 28, 1
    args.nclasses = 10
    train_loader = torch.utils.data.DataLoader(
        FashionMNISTMatch(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5,), (0.5,)))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        FashionMNISTMatch(args.dataDir, train=False,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5,), (0.5,)))
            ])),
        batch_size=args.batch_size, shuffle=True)
elif args.dataset == 'SVHN':
    args.nr, args.nc, args.nch = 28, 28, 3
    args.nclasses = 10
    train_loader = torch.utils.data.DataLoader(
        datasets.SVHN(args.dataDir, split='train', download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        datasets.SVHN(args.dataDir, split='test',
            transform=transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
            ])),
        batch_size=args.batch_size, shuffle=True)
elif args.dataset == 'CIFAR10':
    args.nr, args.nc, args.nch = 28, 28, 3
    args.nclasses = 10
    train_loader = torch.utils.data.DataLoader(
        CIFAR10Match(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        CIFAR10Match(args.dataDir, train=False,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ])),
        batch_size=args.batch_size, shuffle=True)
elif args.dataset == 'CIFAR100':
    args.nr, args.nc, args.nch = 28, 28, 3
    args.nclasses = 100
    train_loader = torch.utils.data.DataLoader(
        CIFAR100Match(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        CIFAR100Match(args.dataDir, train=False,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ])),
        batch_size=args.batch_size, shuffle=True)
elif args.dataset == 'VOC':
    args.nr, args.nc, args.nch = 64, 64, 3
    args.nclasses = len(VOCImagePairs.CLASSES)
    args.seg = True
    train_loader = torch.utils.data.DataLoader(
        VOCImagePairs(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                MyScale((64,64)),
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ]),
            target_transform=transforms.Compose([
                MyScale((64,64)),
                transforms.ToTensor(),
                transforms.Lambda(lambda x: x.select(0,0))
            ])
        ),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        VOCImagePairs(args.dataDir, train=False,
            transform=transforms.Compose([
                MyScale((64, 64)),
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ]),
            target_transform=transforms.Compose([
                MyScale((64,64)),
                transforms.ToTensor(),
                transforms.Lambda(lambda x: x.select(0,0))
            ])
        ),
        batch_size=args.batch_size, shuffle=True)
elif args.dataset == 'OMNIGLOT':
    args.nr, args.nc, args.nch = 105, 105, 1
    args.nclasses = 1623
    train_loader = torch.utils.data.DataLoader(
        OmniglotMatch(args.dataDir, download=True,
            transform=transforms.Compose([
                #FilenameToPILImage(),
                #MyScale((64,64)),
                #transforms.ToTensor(),
                transforms.Lambda(lambda t: (1-torch.Tensor(numpy.array(t, dtype=float)).unsqueeze(0))),
                KeepOrigImg(transforms.Normalize((0.5,), (0.5,)))
            ]),
        ),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        OmniglotMatch(args.dataDir,
            transform=transforms.Compose([
                #FilenameToPILImage(),
                #MyScale((64, 64)),
                transforms.Lambda(lambda t: (1-torch.Tensor(numpy.array(t, dtype=float)).unsqueeze(0))),
                KeepOrigImg(transforms.Normalize((0.5,), (0.5,)))
            ]),
        ),
        batch_size=args.batch_size, shuffle=True)
else:
    raise RuntimeError("unsupported dataset " + args.dataset)


args.nshapes = 4 * args.nclasses



kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}




model = eval(args.model)()
if args.cuda:
    model.cuda()

optimizer = optim.SGD(model.parameters(), lr=args.lr)
# optimizer = optim.Adam([{'params': [x for (n,x) in model.named_parameters() if n != 'thetaNet.cclShape.weights']},
#                         {'params': model.thetaNet.cclShape.parameters(), 'lr': args.lr*2}],
#                         lr=args.lr)




learnedMasks = {'train': torch.ones(len(train_loader)*args.batch_size, 1, args.nr, args.nc) * 0.5,
                'test' : torch.ones(len(test_loader)*args.batch_size,  1, args.nr, args.nc) * 0.5}






if __name__ == "__main__":
    Y = 'targetMatch'
    Ypred = 'matches'

    args.start_epoch = 1
    if args.resume is not None:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            args.start_epoch = checkpoint['epoch']
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))


    for epoch in range(args.start_epoch, args.epochs + 1):
        runOnLoader(epoch, train_loader, train=True)
        save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'optimizer' : optimizer.state_dict(),
        }, False)
        gc.collect()
        runOnLoader(epoch, test_loader, train=False)
        gc.collect()
