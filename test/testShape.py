import numpy as np

import torch
import math
import torch.cuda
from torch.autograd import Variable
from torch.nn import Parameter
from torch import optim

import ipdb
import tu.modules.ccl
from tqdm import tqdm


polyKernDegree = 3
nr = 10
nc = 10

probs = [torch.zeros(1,nr,nc),
         torch.ones(1,nr,nc),
         torch.Tensor([
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1]
         ]).unsqueeze(0),
         torch.Tensor([
             [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
             [1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
             [1, 1, 1, 1, 0, 0, 0, 1, 1, 1],
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
             [1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
             [1, 1, 1, 1, 0, 0, 0, 1, 1, 1],
             [1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
             [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
         ]).unsqueeze(0),
         torch.Tensor([
             [1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
             [1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
             [1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
             [1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
             [1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
             [1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
             [1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
             [1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
             [1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
             [1, 1, 1, 0, 0, 0, 0, 0, 0, 0]
         ]).unsqueeze(0),
]

probs = torch.cat(probs, dim=0)
nb = probs.size(0)

def rbfApproximation(nr, nc, sigma=1, deg=3):
    x1 = torch.arange(0,nr).unsqueeze(1).unsqueeze(2).repeat(1,nc,1)/nr
    x2 = torch.arange(0,nc).unsqueeze(0).unsqueeze(2).repeat(nr,1,1)/nc


    gamma = 2/float(sigma * sigma)
    mult = (-(gamma * (x1.pow(2) + 2 * x1 * x2 + x2.pow(2)))).exp()

    fv = [x1*0+1]
    for i in range(1,deg+1):
        for j in range(i+1):
            #fv.append(mult * x1.pow(j) * x2.pow(i-j) * math.sqrt(2.0*gamma/math.factorial(i)))
            fv.append(x1.pow(j) * x2.pow(i-j))
    xs = torch.cat(fv, dim=2)
    return xs


fvs = rbfApproximation(nr,nc, deg=polyKernDegree)
print('kernelizing with degree', polyKernDegree,fvs.size())




ns = fvs.size(0) * fvs.size(1)
nd = fvs.size(2)
fvs = Variable(fvs.view(1, ns, nd).repeat(nb,1,1))



probs = Variable(probs)
params = Variable(torch.randn(nb, nd, 2))




def forward(fvs, clusters):
    x = Variable(fvs.data, requires_grad=False)
    p = None

    if fvs.is_cuda:
        p = Parameter(torch.randn(nb, nd).cuda() , requires_grad=True)
    else:
        p = Parameter(torch.randn(nb, nd) , requires_grad=True)

    pt = Variable(p.data * 0, requires_grad=False) # for l1 loss
    y = clusters.view(-1).float() # for classification


    loss1 = torch.nn.SoftMarginLoss(size_average=False)
    loss2 = torch.nn.L1Loss(size_average=False)
    model = tu.modules.ccl.CclBatchLinearModule()
    optimizer = optim.SGD([p], lr=0.1, momentum=0.9)

    for i in range(100):
        # Reset gradient
        optimizer.zero_grad()

        # Forward
        fx = model(x, p)
        output1 = loss1.forward(fx.view(-1), y)
        output2 = loss2.forward(p, pt) * 0.01 / nd

        # Backward
        output1.backward()
        output2.backward()

        # Update parameters
        optimizer.step()


        # check acc
        acc = (createCluster(fx > 0) == clusters).float().mean().data[0]

        print(i, (output1+output2).data[0], acc)

    # Reset gradient
    optimizer.zero_grad()
    # Forward
    fx = model(fvs, p)


    print(clusters)
    print(createCluster(fx > 0).view(nb, nr, nc) == clusters)
    unitP = p/torch.norm(p, p=2, dim=1).unsqueeze(1).expand_as(p)
    print(unitP)
    output1 = loss1.forward(fx.view(-1), y)
    output2 = loss2.forward(p, pt) * 0.01 / nd
    # Backward
    output1.backward()
    output2.backward()

    grads = (x.grad, p.grad)
    return output1, p, grads



def createCluster(zeroone):
    return (zeroone == 1).long() - (zeroone == 0).long()


output, theta, grads = forward(fvs, createCluster(probs >= 0.5))
m = tu.modules.ccl.CclShapeModule(nr, nc, 1, 3)

theta, loss = m((probs*0+1).view(nb, ns), createCluster(probs >= 0.5).float())
print(theta, loss)
#output, theta, grads = forward(fvs.cuda(), (probs >= 0.5).long().cuda())
