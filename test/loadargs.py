import builtins
import argparse, sys, os
import torch
import logging
from blessed import Terminal
from models import *
from tqdm import tqdm
import gc
import torch
import torch.autograd.variable
terminal = Terminal()


# Training settings
parser = argparse.ArgumentParser(description='PyTorch MNIST Example',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--batch-size', type=int, default=4, metavar='N',
                    help='input batch size for training')
parser.add_argument('--epochs', type=int, default=50, metavar='N',
                    help='number of epochs to train ')
parser.add_argument('--lr', type=float, default=0.0001, metavar='LR',
                    help='learning rate ')
parser.add_argument('--momentum', type=float, default=0.9, metavar='M',
                    help='SGD momentum')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed ')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                    help='how many batches to wait before logging training status')
parser.add_argument('--vis-interval', type=int, default=100, metavar='N',
                    help='how many batches to wait before visualizing')
parser.add_argument('--dataDir', type=str, default='/home/pareshmg/data',
                    help='which dir to get data from')
parser.add_argument('--outDir', type=str, default='/home/pareshmg/outs/logs/test',
                    help='which dir to get data from')
parser.add_argument('--deg', type=int, default=8,
                    help='which dir to get data from')
parser.add_argument('--dataset', type=str, default='MNIST',
                    help='which dataset to use: MNIST, SVHN')
parser.add_argument('--model', type=str, default='', help='model to run')
parser.add_argument("--resume", type=str, default=None,  help="resume with the checkpoint file")
parser.add_argument("--nPositiveExamples", type=int, default=1,  help="nPositiveExamples for match")
parser.add_argument("--nNegativeExamples", type=int, default=1,  help="nNegativeExamples for match")
parser.add_argument("--entropyLambda", type=float, default=0.1,  help="nNegativeExamples for match")

builtins.args = parser.parse_args()

args.nPrintLines = 25



args.cuda = not args.no_cuda and torch.cuda.is_available()
args.seg = False
args.classnames = []

args.dataset = args.dataset.upper().strip()
args.model = args.model.strip()

args.outDir = os.path.join(args.outDir.strip(), args.dataset, args.model)
args.dataDir = os.path.join(args.dataDir.strip(), args.dataset)

if not os.path.exists(args.dataDir):
    os.makedirs(args.dataDir)

if not os.path.exists(args.outDir):
    os.makedirs(args.outDir)



torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)


##################################################
### Set up logger

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename=os.path.join(args.outDir, 'run.log'),
                    filemode='w')
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler(sys.stdout)
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)



def dbghook(m, gi, go):
    print(m)
    debugTensor(gi)
    debugTensor(go)

def dbgModel(model):
    print('grads')
    for x in model.parameters():
        if x.grad is not None:
            print(x.size(), x.data.abs().min(), x.data.abs().max(), x.grad.data.abs().min(), x.grad.data.abs().max())
        else:
            print(x.size(), x.data.abs().min(), x.data.abs().max())

def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
    torch.save(state, os.path.join(args.outDir, filename))
    logging.info('Saved model to ' + args.outDir)
    if is_best:
        shutil.copyfile(filename, os.path.join(args.outDir, 'model_best.pth.tar'))


def validTensor(t):
    if type(t) in [torch.autograd.variable.Variable, torch.nn.parameter.Parameter]:
        return validTensor(t.data)
    if type(t) == type([]):
        return (len(t) == 0) or (all([validTensor(x) for x in t]))
    if type(t) == type({}):
        return (len(t) == 0) or (all([validTensor(t[k]) for k in t]))
    if type(t) in [torch.FloatTensor, torch.DoubleTensor, torch.cuda.FloatTensor, torch.cuda.DoubleTensor]:
        inf_mask = t.eq(float('inf')) | t.eq(float('-inf')) | (t != t)  # inf or -inf or nan
        return (inf_mask.long().sum() == 0)
    if type(t) in [int, float, str, bool]:
        return True
    else:
        return ((t != t).long().sum() == 0)






def runOnLoader(model, optimizer, epoch, loader, Y, Ypred, train=False):
    if train: model.train()
    else: model.eval()

    tag = 'train' if train else 'test'

    total_loss = 0
    total_correct = 0
    nsamps = 0
    if train:
        # print a whole screen of newlines and then reset location of cursor
        for i in range(terminal.height-args.nPrintLines): print(':')

    with tqdm(total = len(loader)) as pbar:
        for batch_idx, (batchX, batchY) in enumerate(loader):
            if 'actualSeg' not in batchX:
                batchX['actualSeg'] = batchX['origdata']

            #batchX['mask'] = learnedMasks[tag].index_select(0, batchY['origidx'].view(-1)).view(batchY['origidx'].size(0), batchY['origidx'].size(1), 1, args.nr, args.nc)

            if args.cuda:
                batchX = {k:v.cuda() for (k,v) in batchX.items()}
                batchY = {k:v.cuda() for (k,v) in batchY.items()}

            batchX = {k:Variable(v, requires_grad=(k == 'mask')) for (k,v) in batchX.items()}
            batchY = {k:Variable(v) for (k,v) in batchY.items()}

            res = None
            if train: optimizer.zero_grad()

            ## forward through model
            res = model(batchX)
            l = model.getLoss(batchX, batchY, res)

            ## backward through the model and step
            if train:
                l.backward()
                optimizer.step()
                # if batchX['mask'].grad is not None:
                #     learnedMasks[tag].index_add_(0, batchY['origidx'].view(-1).data.cpu(), -args.lr * 0.01 * batchX['mask'].grad.data.view(-1, 1, args.nr, args.nc).cpu())
                #     learnedMasks[tag] = learnedMasks[tag].clamp(min=0.01, max=1)

            ## aggregate Loss and accuracy
            loss = l.data[0]
            n = batchY[Y].size(0)

            pred = res[Ypred].data.max(1, keepdim=True)[1] # get the index of the max log-probability
            correct = pred.eq(batchY[Y].data.view_as(pred)).cpu().sum()

            total_loss += loss
            total_correct += correct
            nsamps += n

            ## add to dictionary
            res.update(batchX)
            res.update(batchY)


            ## Data integrity check: nan and inf
            data_valid = validTensor(res) and validTensor(list(model.parameters()))

            ## progress bar
            if (batch_idx % args.log_interval == 0) or (batch_idx == (len(loader) - 1)) or (not data_valid):
                args.nPrintLines = len(res) + 6
                with terminal.location(0,terminal.height-(args.nPrintLines if train else 0)):
                    # desc = '{} Epoch: {:3d} [{:7d}/{:7d} ({:3.0f}%)]: Loss/Acc: {:9.6f} {:3.0f}% | {:9.6f} {:3.0f}%'.format(
                    #     tag, epoch, batch_idx * n, len(loader.dataset),
                    #     100. * batch_idx / len(loader),
                    #     loss/n, correct/n * 100,
                    #     total_loss/nsamps, total_correct/nsamps * 100)
                    desc = '{} Epoch: {:3d} [{:7d}/{:7d} ({:3.0f}%)]: Loss/Acc: {:9.6f} {:3.0f}%'.format(
                        tag, epoch, batch_idx * n, len(loader.dataset),
                        100. * batch_idx / len(loader),
                        total_loss/nsamps, total_correct/nsamps * 100)

                    if train: print(terminal.clear_eos)

                    #terminal.clear_eos()
                    #terminal.clear()
                    logging.debug(desc)
                    pbar.set_description(desc)
                    pbar.update(args.log_interval)
                    #print('pbar: ' + desc)
                    if train:
                        #logging.info(str(res['matches'].data.cpu().numpy().tolist()))
                        logging.debug('----------')
                        #logging.info('loss was {}'.format(total_loss/nsamps))
                        print('loss was {}'.format(total_loss/nsamps))
                        mps = list(model.parameters())
                        mps = [((x != x).long().sum().data[0], x.data.min(), x.data.max()) for x in mps]
                        #logging.info('model param nans: ' + str(mps))
                        for k,v in res.items():
                            s = ''
                            nnan = 0
                            vmin = None
                            vmax = None
                            vmean = None
                            try:
                                vmin = v
                                nnan = (v.data != v.data).long().sum()
                                vmin = v.data.min()
                                vmax = v.data.max()
                                vmean = v.data.mean()
                            except:
                                pass

                            s = '{:15s} '.format(k)
                            for x in [vmin, vmax, vmean]:
                                if x is None:
                                    s += '           None '
                                else:
                                    s += '{:15.7f} '.format(x)
                            if nnan > 0: s = colors.color(s, 'red')
                            #logging.info(s)
                            print(s)


            if not data_valid:
                logging.info('Nans or infs. Quitting')
                exit(5)

            ## visualize
            if (batch_idx % (args.vis_interval) == 0):
                model.visualize(res, '%s_epoch%d_batch%d'%(tag, epoch, batch_idx))


            # cleanup
            del res
            del batchX
            del batchY
            del loss
            gc.collect()
