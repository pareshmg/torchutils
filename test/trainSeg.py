from __future__ import print_function
import builtins
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


from loadargs import *

import logging
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import tu.modules.ccl
from torchvision import datasets, transforms
from torch.autograd import Variable
import ipdb
import os, sys
import numpy
import gc

from dset import *
from models import *

from tu.imageUtils import *
import colors





##################################################
## Set up dataset

train_loader = None
test_loader = None



if args.dataset == 'MNIST':
    args.nr, args.nc, args.nch = 32, 32, 1
    args.nclasses = 10
    train_loader = torch.utils.data.DataLoader(
        MNIST(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                transforms.Lambda(lambda x: nn.ZeroPad2d(2)(x.unsqueeze(0)).select(0,0).data),
                KeepOrigImg(transforms.Normalize((0.1307,), (0.3081,)))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        MNIST(args.dataDir, train=False,
            transform=transforms.Compose([
                transforms.ToTensor(),
                transforms.Lambda(lambda x: nn.ZeroPad2d(2)(x.unsqueeze(0)).select(0,0).data),
                KeepOrigImg(transforms.Normalize((0.1307,), (0.3081,)))
            ])),
        batch_size=args.batch_size, shuffle=True)
elif args.dataset == 'FASHIONMNIST':
    args.nr, args.nc, args.nch = 32, 32, 1
    args.nclasses = 10
    train_loader = torch.utils.data.DataLoader(
        FashionMNIST(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                transforms.Lambda(lambda x: nn.ZeroPad2d(2)(x.unsqueeze(0)).select(0,0).data),
                KeepOrigImg(transforms.Normalize((0.5,), (0.5,)))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        FashionMNIST(args.dataDir, train=False,
            transform=transforms.Compose([
                transforms.ToTensor(),
                transforms.Lambda(lambda x: nn.ZeroPad2d(2)(x.unsqueeze(0)).select(0,0).data),
                KeepOrigImg(transforms.Normalize((0.5,), (0.5,)))
            ])),
        batch_size=args.batch_size, shuffle=True)
elif args.dataset == 'SVHN':
    args.nr, args.nc, args.nch = 28, 28, 3
    args.nclasses = 10
    train_loader = torch.utils.data.DataLoader(
        SVHN(args.dataDir, split='train', download=True,
            transform=transforms.Compose([
                KeepOrigImg(transforms.ToTensor()),
                #transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
                #KeepOrigImg(transforms.Normalize((0.5,), (0.5,)))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        SVHN(args.dataDir, split='test',
            transform=transforms.Compose([
                KeepOrigImg(transforms.ToTensor()),
                #KeepOrigImg(transforms.Normalize((0.5,), (0.5,)))
            ])),
        batch_size=args.batch_size, shuffle=True)
elif args.dataset == 'CIFAR10':
    args.nr, args.nc, args.nch = 28, 28, 3
    args.nclasses = 10
    train_loader = torch.utils.data.DataLoader(
        CIFAR10(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        CIFAR10(args.dataDir, train=False,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ])),
        batch_size=args.batch_size, shuffle=True)
elif args.dataset == 'CIFAR100':
    args.nr, args.nc, args.nch = 28, 28, 3
    args.nclasses = 100
    train_loader = torch.utils.data.DataLoader(
        CIFAR100(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ])),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        CIFAR100(args.dataDir, train=False,
            transform=transforms.Compose([
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ])),
        batch_size=args.batch_size, shuffle=True)
elif args.dataset == 'VOC':
    args.nr, args.nc, args.nch = 32, 32, 3
    args.nclasses = len(VOCSegmentation.CLASSES)
    args.classnames = VOCSegmentation.CLASSES
    args.seg = True
    train_loader = torch.utils.data.DataLoader(
        VOCSegmentation(args.dataDir, train=True, download=True,
            transform=transforms.Compose([
                MyScale((32,32)),
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ]),
            target_transform=transforms.Compose([
                MyScale((32,32)),
                transforms.ToTensor(),
                transforms.Lambda(lambda x: x.select(0,0))
            ])
        ),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        VOCSegmentation(args.dataDir, train=False,
            transform=transforms.Compose([
                MyScale((32, 32)),
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))
            ]),
            target_transform=transforms.Compose([
                MyScale((32,32)),
                transforms.ToTensor(),
                transforms.Lambda(lambda x: x.select(0,0))
            ])
        ),
        batch_size=args.batch_size, shuffle=True)
elif args.dataset == 'OMNIGLOT':
    args.nr, args.nc, args.nch = 32, 32, 1
    args.nclasses = 1623
    train_loader = torch.utils.data.DataLoader(
        OMNIGLOT(args.dataDir, download=True,
            transform=transforms.Compose([
                FilenameToPILImage(),
                MyScale((32,32)),
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5,), (0.5,)))
            ]),
        ),
        batch_size=args.batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(
        OMNIGLOT(args.dataDir,
            transform=transforms.Compose([
                FilenameToPILImage(),
                MyScale((32, 32)),
                transforms.ToTensor(),
                KeepOrigImg(transforms.Normalize((0.5,), (0.5,)))
            ]),
        ),
        batch_size=args.batch_size, shuffle=True)
else:
    raise RuntimeError("unsupported dataset " + args.dataset)


args.nshapes = 4 * args.nclasses
if len(args.classnames) == 0: args.classnames = [str(x) for x in range(args.nclasses)]


kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}





model = eval(args.model)()
if args.cuda:
    model.cuda()



optimizer = optim.Adam(model.parameters(), lr=args.lr)
# optimizer = optim.Adam([{'params': [x for (n,x) in model.named_parameters() if n != 'thetaNet.cclShape.weights']},
#                         {'params': model.thetaNet.cclShape.parameters(), 'lr': args.lr*2}],
#                         lr=args.lr)




if __name__ == "__main__":
    Y = 'target'
    Ypred = 'classification'
    args.start_epoch = 1
    if args.resume is not None:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            args.start_epoch = checkpoint['epoch']
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))

    if False:
        # model.thetaNet.cclShape.register_backward_hook(dbghook)
        nshapes = args.nshapes
        nr = args.nr
        nc = args.nc
        ns = nr * nc
        fvs = model.thetaNet.rbf.clusterFV(torch.ones(nshapes, nr, nc).cuda(),
                                           torch.ones(nshapes).cuda().long()*ns)
        fvs = fvs.view(fvs.size(0), nr*nc, fvs.size(3)) # batch * nsamps * dim
        #model.thetaNet.cclShape.visualizeWeights(fvs, torch.ones(nshapes, ns), torch.ones(nshapes, ns), 'init')



    #runOnLoader(0, test_loader, train=False)

    for epoch in range(args.start_epoch, args.epochs + 1):
        runOnLoader(model, optimizer, epoch, train_loader, Y, Ypred, train=True)
        save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'optimizer' : optimizer.state_dict(),
        }, False)
        gc.collect()
        runOnLoader(model, optimizer, epoch, test_loader, Y, Ypred, train=False)
        gc.collect()
