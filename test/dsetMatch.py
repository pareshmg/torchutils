from dset import *


def getImagePairs(cl, obj, index):
    """
        For each image, also provide npairs of other images from the same class randomly selected

    object needs to have:
    nPositiveExamples
    nNegativeExamples
    ppClassMembers

    """
    dx, dy = super(cl, obj).__getitem__(index)
    c = dy['target'][0]
    ## randomly select other objects from same class
    pidxs = [random.choice(obj.ppClassMembers[c]) for _ in range(obj.nPositiveExamples)]
    nidxs = []
    while len(nidxs) < obj.nNegativeExamples:
        c2 = random.choice(list(range(len(obj.ppClassMembers))))
        if c2 == c: continue
        if len(obj.ppClassMembers[c2]) == 0: continue
        nidxs.append(random.choice(obj.ppClassMembers[c2]))


    resx = {k:[dx[k].unsqueeze(0)] for k in dx}
    resy = {k:[dy[k]] for k in dy}

    rshuf = list(range(len(pidxs) + len(nidxs)))
    random.shuffle(rshuf)
    aidxs = pidxs + nidxs
    aidxs = [aidxs[x] for x in rshuf] # shuffle

    for x in aidxs:
        dxt,dyt = super(cl, obj).__getitem__(x)
        for k in dxt: resx[k].append(dxt[k].unsqueeze(0))
        for k in dyt: resy[k].append(dyt[k])



    for k in resx: resx[k] = torch.cat(resx[k], dim=0)
    for k in resy: resy[k] = torch.cat(resy[k], dim=0)
    resy['target'] = c
    resy['targetMatch'] = rshuf.index(0)
    return resx, resy


class VOCImagePairs(VOCSegmentation):
    def __getitem__(self, index):
        return getImagePairs(VOCImagePairs, self, index)

class MNISTMatch(MNIST):
    def __getitem__(self, index):
        return getImagePairs(MNISTMatch, self, index)

class FashionMNISTMatch(FashionMNIST):
    def __getitem__(self, index):
        return getImagePairs(FashionMNISTMatch, self, index)


class CIFAR10Match(CIFAR10):
    def __getitem__(self, index):
        return getImagePairs(CIFAR10Match, self, index)


class CIFAR100Match(CIFAR100):
    def __getitem__(self, index):
        return getImagePairs(CIFAR100Match, self, index)

class OmniglotMatch(Omniglot):
    def __getitem__(self, index):
        return getImagePairs(OmniglotMatch, self, index)
