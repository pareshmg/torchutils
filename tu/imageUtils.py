import numpy
import PIL

from matplotlib import cm
import torch
import torch.nn as nn
import ipdb
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import torchvision
from torchvision import transforms
from torchvision.transforms import ToTensor

def debugImageTensor(t, label, outdir):
    #if type(t) == torch.autograd.variable.Variable: t = t.data
    imgs = vstackImgs([makeImg(t.select(0,i)) for i in range(t.size(0))])
    imgs.save(os.path.join(outdir, label+'.png'))

def debugTensor(t, indent=0):
    if type(t) == tuple:
        print(''*indent + '[')
        for x in list(t): debugTensor(x, indent+4)
        print(''*indent + ']')
    #elif type(t) == torch.autograd.variable.Variable: debugTensor(t.data, indent)
    else: print(' '*indent, t.size(), t.abs().min(), t.abs().max())


def makeImg(img):
    """ [0,1] nr * nc * 3 -> PIL Image
    """
    img = (img*255).long().clamp(min=0, max=255)
    return Image.fromarray(numpy.uint8(img.cpu().numpy()), 'RGB')


def setupImg(img):
    """ img RGB, normalize to 0,1. Return Tensor
    """
    #if type(img) == torch.autograd.variable.Variable: img = img.data
    if img.ndimension() == 2: img = img.unsqueeze(2)
    if img.ndimension() == 3:
        if (img.size(2) > 3) and (img.size(0) <= 3):
            img = img.permute(1,2,0)
    if img.size(2) == 1: img = img.repeat(1,1,3)
    return img # [nr, nc, nk]

def yuv2rgb(t):
    """
    t :: r, c, ch
    """
    t2 = ToTensor()(Image.fromarray(numpy.uint8(t.cpu().numpy()*255), 'YCbCr').convert('RGB'))
    #t2 = t2.permute(1,2,0)
    if t.is_cuda: t2 = t2.cuda()
    return t2

def makeSameSize(imgs, dim=0):
    nr = max([x.size(0) for x in imgs])
    nc = max([x.size(1) for x in imgs])

    res = []
    for x in imgs:
        nx = x.new()
        if dim == 0: nx.resize_(nr, x.size(1), 3).fill_(0)
        elif dim == 1: nx.resize_(x.size(0), nc, 3).fill_(0)
        nx.narrow(0,0,x.size(0)).narrow(1,0,x.size(1)).copy_(x)
        res.append(nx)
    return res

def hstackImgs(imgs):
    return torch.cat(makeSameSize([setupImg(x) for x in imgs], dim=0), dim=1)


def drawBB(img, bb,fill=torch.Tensor([0,0,1])):
    """
    img :: [nch, nr, nc]
    bb :: [4] (left, top, right, bottom)
    """
    left,top,right,bottom = bb

    img[:,top:bottom, left].zero_()
    img[:,top:bottom, right].zero_()
    img[:,top, left:right].zero_()
    img[:,bottom, left:right].zero_()


    img[:,top:bottom, left] += fill
    img[:,top:bottom, right] += fill
    img[:,top, left:right] += fill
    img[:,bottom, left:right] += fill

    return img

def vstackImgs(imgs):
    return torch.cat(makeSameSize([setupImg(x) for x in imgs], dim=1), dim=0)

def normImg(img):
    mn = img.min()
    mx = img.max()
    img = img - mn
    img = img / (mx-mn if (mx - mn) > 0 else 1)
    return img

def unnormImgOld(img, mean, std):
    img = img*std + mean
    return img

def unnormImg(img, mean, std):
    if type(mean) == list:
        invTrans = transforms.Compose([ transforms.Normalize(mean = [0. for x in mean],
                                                             std = [ 1/x for x in std]),
                                        transforms.Normalize(mean = [ -x for x in mean],
                                                             std = [ 1. for x in std])
        ])
    else:
        invTrans = transforms.Compose([ transforms.Normalize(mean = 0.,
                                                             std = 1/std),
                                        transforms.Normalize(mean = -mean,
                                                             std = 1)
        ])

    if img.ndimension() == 4:
        res = torch.stack([invTrans(x) for x in img])
        return res
    else:
        return invTrans(img)



def padImg(img, padWidth=2, fill=[1,0,0]):
    img = setupImg(img)
    nx = img.new().resize_(img.size(0)+2*padWidth, img.size(1)+2*padWidth, img.size(2))
    for i in range(3): nx.select(2,i).fill_(fill[i])
    nx.narrow(0,padWidth, img.size(0)).narrow(1,padWidth, img.size(1)).copy_(img)
    return nx


def drawBB(img, bb, fill=[0,0,1]):
    img = setupImg(img)
    ymin, xmin, ymax, xmax = bb
    for chi in range(len(fill)):
        im = img.narrow(2,chi,1)
        f = fill[chi]
        im.narrow(0,ymin,1).narrow(1,xmin, xmax-xmin+1).fill_(f)
        im.narrow(0,ymax,1).narrow(1,xmin, xmax-xmin+1).fill_(f)
        im.narrow(1,xmin,1).narrow(0,ymin, ymax-ymin+1).fill_(f)
        im.narrow(1,xmax,1).narrow(0,ymin, ymax-ymin+1).fill_(f)
    return img


class ShiftImage(nn.Module):
    def __init__(self, dim, dx, fill=0):
        super(ShiftImage, self).__init__()
        self.dim = dim
        self.dx = dx
        self.fill = fill
    def forward(self, inp):
        dn = inp.size(self.dim)
        if self.dx == 0: return inp
        if self.dx > 0: ## shift right
            res = torch.cat([inp.narrow(self.dim, dn-self.dx, dx) * 0 + self.fill,
                             inp.narrow(self.dim, 0, dn-self.dx)])
            return res
        else: ## shift left
            res = torch.cat([inp.narrow(self.dim, dn, dn-self.dx),
                             inp.narrow(self.dim, 0, self.dx) * 0 + self.fill])
            return res

def txtImg(base, txt):
    img = makeImg(setupImg(base*0))
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype("FreeMono.ttf", 12)
    draw.text((0, 0),txt,(255,255,255),font=font)
    img = ToTensor()(img).permute(1,2,0)
    if base.is_cuda: img = img.cuda()
    return img




torch.split1 = lambda x, dim=0: [x.select(dim, i) for i in range(x.size(dim))]

def resizeImg(t, sz, interp=Image.BILINEAR):
    """ t: nb, ndim, nr, nc
    """
    res = []
    for i in range(t.size(0)):
        img = makeImg(setupImg(t[i]))
        img = img.resize(sz, interp)
        img = ToTensor()(img)
        res.append(img)
    res = torch.stack(res)
    return res


def save_image(t, *args, **kwargs):
    """
    t : nr, nc, nch
    """
    t = t.permute(2,0,1)
    torchvision.utils.save_image(t, *args, **kwargs)
