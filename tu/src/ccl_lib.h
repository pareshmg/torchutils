int ccl_lib_add_forward(THFloatTensor *input1, THFloatTensor *input2, THFloatTensor *output);
int ccl_lib_add_backward(THFloatTensor *grad_output, THFloatTensor *grad_input);


int ccl_lib_ccl_le_forward(THFloatTensor *probs, THLongTensor *labs, THLongTensor *refs, float th);
int ccl_lib_init_forward(THFloatTensor *probs, THLongTensor *labs, THLongTensor *refs);
