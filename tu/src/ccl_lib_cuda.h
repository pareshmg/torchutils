int ccl_lib_add_forward_cuda(THCudaTensor *input1, THCudaTensor *input2,
           THCudaTensor *output);
int ccl_lib_add_backward_cuda(THCudaTensor *grad_output, THCudaTensor *grad_input);

int ccl_lib_ccl_le_forward_cuda(THCudaTensor *probs, THCudaLongTensor *labs, THCudaLongTensor *refs, float th);
int ccl_lib_init_forward_cuda(THCudaTensor *probs, THCudaLongTensor *labs, THCudaLongTensor *refs);

int pairwisediffl1_forward_cuda(THCudaTensor *v1, THCudaTensor *v2, THCudaTensor *out);
int pairwisediffl1_backward_cuda(THCudaTensor *v1, THCudaTensor *v2, THCudaTensor *gout, THCudaTensor *gin1, THCudaTensor *gin2);


int pairwisediffl1_forward_cudadouble(THCudaDoubleTensor *v1, THCudaDoubleTensor *v2, THCudaDoubleTensor *out);
int pairwisediffl1_backward_cudadouble(THCudaDoubleTensor *v1, THCudaDoubleTensor *v2, THCudaDoubleTensor *gout, THCudaDoubleTensor *gin1, THCudaDoubleTensor *gin2);


int pairwisediffl2_forward_cuda(THCudaTensor *v1, THCudaTensor *v2, THCudaTensor *out);
int pairwisediffl2_backward_cuda(THCudaTensor *v1, THCudaTensor *v2, THCudaTensor *gout, THCudaTensor *gin1, THCudaTensor *gin2);


int pairwisediffl2_forward_cudadouble(THCudaDoubleTensor *v1, THCudaDoubleTensor *v2, THCudaDoubleTensor *out);
int pairwisediffl2_backward_cudadouble(THCudaDoubleTensor *v1, THCudaDoubleTensor *v2, THCudaDoubleTensor *gout, THCudaDoubleTensor *gin1, THCudaDoubleTensor *gin2);


int dlse_forward_cuda(THCudaTensor *out, THCudaTensor *si_j_, THCudaTensor *di_, THCudaTensor *djj_, float sigma2);
int dlse_backward_cuda(THCudaTensor *gin, THCudaTensor *gout, THCudaTensor *out, THCudaTensor *si_j_, THCudaTensor *di_, THCudaTensor *djj_, float sigma2);


int dlse_forward_cudadouble(THCudaDoubleTensor *out, THCudaDoubleTensor *si_j_, THCudaDoubleTensor *di_, THCudaDoubleTensor *djj_, float sigma2);
int dlse_backward_cudadouble(THCudaDoubleTensor *gin, THCudaDoubleTensor *gout, THCudaDoubleTensor *out, THCudaDoubleTensor *si_j_, THCudaDoubleTensor *di_, THCudaDoubleTensor *djj_, float sigma2);
