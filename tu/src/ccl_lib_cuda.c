#include <THC/THC.h>
// Marathon Match - CCL - Label Equivalence
#include "cuda/ccl_cuda_kernel.h"
#include <stdio.h>

// this symbol will be resolved automatically from PyTorch libs
extern THCState *state;

int ccl_lib_add_forward_cuda(THCudaTensor *input1, THCudaTensor *input2,
           THCudaTensor *output)
{
  if (!THCudaTensor_isSameSizeAs(state, input1, input2))
    return 0;
  THCudaTensor_resizeAs(state, output, input1);
  THCudaTensor_cadd(state, output, input1, 1.0, input2);
  return 1;
}

int ccl_lib_add_backward_cuda(THCudaTensor *grad_output, THCudaTensor *grad_input)
{
  THCudaTensor_resizeAs(state, grad_input, grad_output);
  THCudaTensor_fill(state, grad_input, 1);
  return 1;
}


int ccl_lib_ccl_le_forward_cuda(THCudaTensor *probs, THCudaLongTensor *labs, THCudaLongTensor *refs, float th) {
  unsigned int B = THCudaTensor_size(state, probs, 0);
  unsigned int H = THCudaTensor_size(state, probs, 1);
  unsigned int W = THCudaTensor_size(state, probs, 2);


  float *p = THCudaTensor_data(state, probs);
  long *l = THCudaLongTensor_data(state, labs);
  long *r = THCudaLongTensor_data(state, refs);

  run_ccl_cuda(p, l, r, B, H, W, 4, th);
  return 1;
}

int ccl_lib_init_forward_cuda(THCudaTensor *probs, THCudaLongTensor *labs, THCudaLongTensor *refs) {
  unsigned int B = THCudaTensor_size(state, probs, 0);
  unsigned int H = THCudaTensor_size(state, probs, 1);
  unsigned int W = THCudaTensor_size(state, probs, 2);

  float *p = THCudaTensor_data(state, probs);
  long *l = THCudaLongTensor_data(state, labs);
  long *r = THCudaLongTensor_data(state, refs);
  run_init_cuda(p, l, r, B, H, W);
  return 1;
}



int ccl_upsample_forward_cuda(THCudaTensor *src, THCudaLongTensor *bb, THCudaTensor *tgt) {
  unsigned int B = THCudaTensor_size(state, src, 0);
  unsigned int C = THCudaTensor_size(state, src, 1);
  unsigned int H1 = THCudaTensor_size(state, src, 2);
  unsigned int W1 = THCudaTensor_size(state, src, 3);
  unsigned int H2 = THCudaTensor_size(state, tgt, 2);
  unsigned int W2 = THCudaTensor_size(state, tgt, 3);


  float *s = THCudaTensor_data(state, src);
  long *b = THCudaLongTensor_data(state, bb);
  float *t = THCudaTensor_data(state, tgt);

  run_upsample_forward_cuda(B, C, H1, W1,
                            H2, W2,
                            s, b,
                            t);
  return 1;
}


int ccl_upsample_backward_cuda(THCudaTensor *gout, THCudaLongTensor *bb, THCudaTensor *ginp) {
  unsigned int B = THCudaTensor_size(state, ginp, 0);
  unsigned int C = THCudaTensor_size(state, ginp, 1);
  unsigned int H1 = THCudaTensor_size(state, ginp, 2);
  unsigned int W1 = THCudaTensor_size(state, ginp, 3);
  unsigned int H2 = THCudaTensor_size(state, gout, 2);
  unsigned int W2 = THCudaTensor_size(state, gout, 3);


  float *t = THCudaTensor_data(state, gout);
  long *b = THCudaLongTensor_data(state, bb);
  float *s = THCudaTensor_data(state, ginp);
  run_upsample_backward_cuda(B, C, H1, W1,
                             H2, W2,
                             t, b,
                             s);
  return 1;
}


int pairwisediffl1_forward_cuda(THCudaTensor *v1, THCudaTensor *v2, THCudaTensor *out) {
  unsigned int NB = THCudaTensor_size(state, v1, 0);
  unsigned int NV1 = THCudaTensor_size(state, v1, 1);
  unsigned int ND = THCudaTensor_size(state, v1, 2);
  unsigned int NV2 = THCudaTensor_size(state, v2, 1);

  float *v1d = THCudaTensor_data(state, v1);
  float *v2d = THCudaTensor_data(state, v2);
  float *outd = THCudaTensor_data(state, out);
  run_pairwisediffl1_forward_cuda(v1d, v2d, outd,
                                  NB, NV1, NV2, ND);
  return 1;
}

int pairwisediffl1_backward_cuda(THCudaTensor *v1, THCudaTensor *v2, THCudaTensor *gout, THCudaTensor *gin1, THCudaTensor *gin2) {
  unsigned int NB = THCudaTensor_size(state, v1, 0);
  unsigned int NV1 = THCudaTensor_size(state, v1, 1);
  unsigned int ND = THCudaTensor_size(state, v1, 2);
  unsigned int NV2 = THCudaTensor_size(state, v2, 1);

  float *v1d = THCudaTensor_data(state, v1);
  float *v2d = THCudaTensor_data(state, v2);
  float *goutd = THCudaTensor_data(state, gout);
  float *gind1 = THCudaTensor_data(state, gin1);
  float *gind2 = THCudaTensor_data(state, gin2);
  run_pairwisediffl1_backward_cuda(v1d, v2d, goutd, gind1, gind2,
                                   NB, NV1, NV2, ND);
  return 1;
}


int pairwisediffl1_forward_cudadouble(THCudaDoubleTensor *v1, THCudaDoubleTensor *v2, THCudaDoubleTensor *out) {
  unsigned int NB = THCudaDoubleTensor_size(state, v1, 0);
  unsigned int NV1 = THCudaDoubleTensor_size(state, v1, 1);
  unsigned int ND = THCudaDoubleTensor_size(state, v1, 2);
  unsigned int NV2 = THCudaDoubleTensor_size(state, v2, 1);

  double *v1d = THCudaDoubleTensor_data(state, v1);
  double *v2d = THCudaDoubleTensor_data(state, v2);
  double *outd = THCudaDoubleTensor_data(state, out);
  run_pairwisediffl1_forward_cudadouble(v1d, v2d, outd,
                                  NB, NV1, NV2, ND);
  return 1;
}

int pairwisediffl1_backward_cudadouble(THCudaDoubleTensor *v1, THCudaDoubleTensor *v2, THCudaDoubleTensor *gout, THCudaDoubleTensor *gin1, THCudaDoubleTensor *gin2) {
  unsigned int NB = THCudaDoubleTensor_size(state, v1, 0);
  unsigned int NV1 = THCudaDoubleTensor_size(state, v1, 1);
  unsigned int ND = THCudaDoubleTensor_size(state, v1, 2);
  unsigned int NV2 = THCudaDoubleTensor_size(state, v2, 1);

  double *v1d = THCudaDoubleTensor_data(state, v1);
  double *v2d = THCudaDoubleTensor_data(state, v2);
  double *goutd = THCudaDoubleTensor_data(state, gout);
  double *gind1 = THCudaDoubleTensor_data(state, gin1);
  double *gind2 = THCudaDoubleTensor_data(state, gin2);
  run_pairwisediffl1_backward_cudadouble(v1d, v2d, goutd, gind1, gind2,
                                         NB, NV1, NV2, ND);
  return 1;
}



int pairwisediffl2_forward_cuda(THCudaTensor *v1, THCudaTensor *v2, THCudaTensor *out) {
  unsigned int NB = THCudaTensor_size(state, v1, 0);
  unsigned int NV1 = THCudaTensor_size(state, v1, 1);
  unsigned int ND = THCudaTensor_size(state, v1, 2);
  unsigned int NV2 = THCudaTensor_size(state, v2, 1);

  float *v1d = THCudaTensor_data(state, v1);
  float *v2d = THCudaTensor_data(state, v2);
  float *outd = THCudaTensor_data(state, out);
  run_pairwisediffl2_forward_cuda(v1d, v2d, outd,
                                  NB, NV1, NV2, ND);
  return 1;
}

int pairwisediffl2_backward_cuda(THCudaTensor *v1, THCudaTensor *v2, THCudaTensor *gout, THCudaTensor *gin1, THCudaTensor *gin2) {
  unsigned int NB = THCudaTensor_size(state, v1, 0);
  unsigned int NV1 = THCudaTensor_size(state, v1, 1);
  unsigned int ND = THCudaTensor_size(state, v1, 2);
  unsigned int NV2 = THCudaTensor_size(state, v2, 1);

  float *v1d = THCudaTensor_data(state, v1);
  float *v2d = THCudaTensor_data(state, v2);
  float *goutd = THCudaTensor_data(state, gout);
  float *gind1 = THCudaTensor_data(state, gin1);
  float *gind2 = THCudaTensor_data(state, gin2);
  run_pairwisediffl2_backward_cuda(v1d, v2d, goutd, gind1, gind2,
                                   NB, NV1, NV2, ND);
  return 1;
}


int pairwisediffl2_forward_cudadouble(THCudaDoubleTensor *v1, THCudaDoubleTensor *v2, THCudaDoubleTensor *out) {
  unsigned int NB = THCudaDoubleTensor_size(state, v1, 0);
  unsigned int NV1 = THCudaDoubleTensor_size(state, v1, 1);
  unsigned int ND = THCudaDoubleTensor_size(state, v1, 2);
  unsigned int NV2 = THCudaDoubleTensor_size(state, v2, 1);

  double *v1d = THCudaDoubleTensor_data(state, v1);
  double *v2d = THCudaDoubleTensor_data(state, v2);
  double *outd = THCudaDoubleTensor_data(state, out);
  run_pairwisediffl2_forward_cudadouble(v1d, v2d, outd,
                                  NB, NV1, NV2, ND);
  return 1;
}

int pairwisediffl2_backward_cudadouble(THCudaDoubleTensor *v1, THCudaDoubleTensor *v2, THCudaDoubleTensor *gout, THCudaDoubleTensor *gin1, THCudaDoubleTensor *gin2) {
  unsigned int NB = THCudaDoubleTensor_size(state, v1, 0);
  unsigned int NV1 = THCudaDoubleTensor_size(state, v1, 1);
  unsigned int ND = THCudaDoubleTensor_size(state, v1, 2);
  unsigned int NV2 = THCudaDoubleTensor_size(state, v2, 1);

  double *v1d = THCudaDoubleTensor_data(state, v1);
  double *v2d = THCudaDoubleTensor_data(state, v2);
  double *goutd = THCudaDoubleTensor_data(state, gout);
  double *gind1 = THCudaDoubleTensor_data(state, gin1);
  double *gind2 = THCudaDoubleTensor_data(state, gin2);
  run_pairwisediffl2_backward_cudadouble(v1d, v2d, goutd, gind1, gind2,
                                         NB, NV1, NV2, ND);
  return 1;
}



int dlse_forward_cuda(THCudaTensor *out, THCudaTensor *si_j_, THCudaTensor *di_, THCudaTensor *djj_, float sigma2) {
  unsigned int NB = THCudaTensor_size(state, si_j_, 0);
  unsigned int NNBR = THCudaTensor_size(state, si_j_, 1);
  unsigned int NS = THCudaTensor_size(state, si_j_, 2);
  unsigned int NSUP = THCudaTensor_size(state, si_j_, 3);

  float *vout = THCudaTensor_data(state, out);
  float *vsi_j_ = THCudaTensor_data(state, si_j_);
  float *vdi_ = THCudaTensor_data(state, di_);
  float *vdjj_ = THCudaTensor_data(state, djj_);
  run_dlse_forward_cuda(vout, vsi_j_, vdi_, vdjj_, sigma2,
                        NB, NNBR, NS, NSUP);
  return 1;
}

int dlse_backward_cuda(THCudaTensor *gin, THCudaTensor *gout, THCudaTensor *out, THCudaTensor *si_j_, THCudaTensor *di_, THCudaTensor *djj_, float sigma2) {
  unsigned int NB = THCudaTensor_size(state, si_j_, 0);
  unsigned int NNBR = THCudaTensor_size(state, si_j_, 1);
  unsigned int NS = THCudaTensor_size(state, si_j_, 2);
  unsigned int NSUP = THCudaTensor_size(state, si_j_, 3);

  float *vgin = THCudaTensor_data(state, gin);
  float *vout = THCudaTensor_data(state, out);
  float *vsi_j_ = THCudaTensor_data(state, si_j_);
  float *vdi_ = THCudaTensor_data(state, di_);
  float *vdjj_ = THCudaTensor_data(state, djj_);
  float *vgout = THCudaTensor_data(state, gout);

  run_dlse_backward_cuda(vgin, vgout, vout, vsi_j_, vdi_, vdjj_, sigma2,
                         NB, NNBR, NS, NSUP);
  return 1;
}


int dlse_forward_cudadouble(THCudaDoubleTensor *out, THCudaDoubleTensor *si_j_, THCudaDoubleTensor *di_, THCudaDoubleTensor *djj_, float sigma2) {
  unsigned int NB = THCudaDoubleTensor_size(state, si_j_, 0);
  unsigned int NNBR = THCudaDoubleTensor_size(state, si_j_, 1);
  unsigned int NS = THCudaDoubleTensor_size(state, si_j_, 2);
  unsigned int NSUP = THCudaDoubleTensor_size(state, si_j_, 3);

  double *vout = THCudaDoubleTensor_data(state, out);
  double *vsi_j_ = THCudaDoubleTensor_data(state, si_j_);
  double *vdi_ = THCudaDoubleTensor_data(state, di_);
  double *vdjj_ = THCudaDoubleTensor_data(state, djj_);
  run_dlse_forward_cudadouble(vout, vsi_j_, vdi_, vdjj_, (double)sigma2,
                        NB, NNBR, NS, NSUP);
  return 1;
}

int dlse_backward_cudadouble(THCudaDoubleTensor *gin, THCudaDoubleTensor *gout, THCudaDoubleTensor *out, THCudaDoubleTensor *si_j_, THCudaDoubleTensor *di_, THCudaDoubleTensor *djj_, float sigma2) {
  unsigned int NB = THCudaDoubleTensor_size(state, si_j_, 0);
  unsigned int NNBR = THCudaDoubleTensor_size(state, si_j_, 1);
  unsigned int NS = THCudaDoubleTensor_size(state, si_j_, 2);
  unsigned int NSUP = THCudaDoubleTensor_size(state, si_j_, 3);

  double *vgin = THCudaDoubleTensor_data(state, gin);
  double *vgout = THCudaDoubleTensor_data(state, gout);
  double *vout = THCudaDoubleTensor_data(state, out);
  double *vsi_j_ = THCudaDoubleTensor_data(state, si_j_);
  double *vdi_ = THCudaDoubleTensor_data(state, di_);
  double *vdjj_ = THCudaDoubleTensor_data(state, djj_);

  run_dlse_backward_cudadouble(vgin, vgout, vout, vsi_j_, vdi_, vdjj_, (double)sigma2,
                               NB, NNBR, NS, NSUP);
  return 1;
}
