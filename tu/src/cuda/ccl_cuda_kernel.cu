#ifdef __cplusplus
extern "C" {
#endif

  // Marathon Match - CCL - Label Equivalence
  // https://github.com/foota/ccl


#include "ccl_cuda_kernel.h"
#include <stdio.h>
#include <math.h>

  // CUDA: grid stride looping
#define CUDA_KERNEL_LOOP(i, n)                                          \
  for (long i = blockIdx.x * blockDim.x + threadIdx.x; i < (n); i += blockDim.x * gridDim.x)


#define inCluster_gpu(d1, d2, th) ( (d1 >= th) && (d2 >= th) )


#define v2d(arr, nc, i, j) (arr[(i)*(nc) + (j)])
#define l2d(arr, nc, i, j) (arr + (i)*(nc) + (j))



  const long CUDA_NUM_THREADS = 1024;


  // CUDA: number of blocks for threads.
  inline long GET_BLOCKS(const long N)
  {
    return (N + CUDA_NUM_THREADS - 1) / CUDA_NUM_THREADS;
  }



  const long BLOCK = 256;

  __global__ void init_ccl_gpu(const long nthreads,
                               const long nb, const long nr, const long nc,
                               const float *probs,
                               long *labs, long *refs) {
    CUDA_KERNEL_LOOP(index, nthreads) {
      long xi = index % (nr * nc);

      labs[index] = refs[index] = xi;
    }
  }

  __global__ void scanning_gpu(const long nthreads,
                               const long nb, const long nr, const long nc,
                               const float th, const float *probs,
                               long *labs, long *refs, bool *m) {
    CUDA_KERNEL_LOOP(index, nthreads) {
      const long N = nr * nc;
      long xi = index % N;
      long bi = (index / N);

      const float *D = probs + bi*N;
      long *L = labs + bi*N;
      long *R = refs + bi*N;

      float Dxi = D[xi];
      long label = N;
      if (Dxi < th) {
        label = -1;
        if (L[xi] != label) {
          L[xi] = label;
          *m = true;
        }
      }
      else {
        if (xi - nc >= 0 && inCluster_gpu(Dxi, D[xi-nc], th)) label = min(label, L[xi-nc]); // up
        if (xi + nc < N  && inCluster_gpu(Dxi, D[xi+nc], th)) label = min(label, L[xi+nc]); // down
        long r = xi % nc;
        if (r           && inCluster_gpu(Dxi, D[xi-1], th)) label = min(label, L[xi-1]); // left
        if (r + 1 != nc  && inCluster_gpu(Dxi, D[xi+1], th)) label = min(label, L[xi+1]); // right
      }
      if (label < L[xi]) {
        //atomicMin(&R[L[xi]], label);
        R[L[xi]] = label;
        *m = true;
      }
    }
  }

  __global__ void scanning8_gpu(const long nthreads,
                                const long nb, const long nr, const long nc,
                                const float th, const float *probs,
                                long *labs, long *refs, bool *m) {
    CUDA_KERNEL_LOOP(index, nthreads) {
      const long N = nr * nc;
      long xi = index % N;
      long bi = (index / N);

      const float *D = probs + bi*N;
      long *L = labs + bi*N;
      long *R = refs + bi*N;

      float Dxi = D[xi];
      long label = N;
      if (Dxi < th) {
        label = -1;
        if (L[xi] != label) {
          L[xi] = label;
          *m = true;
        }
      }
      else{
        if (xi - nc >= 0 && inCluster_gpu(Dxi, D[xi-nc], th)) label = min(label, L[xi-nc]);
        if (xi + nc < N  && inCluster_gpu(Dxi, D[xi+nc], th)) label = min(label, L[xi+nc]);
        long r = xi % nc;
        if (r) {
          if (inCluster_gpu(Dxi, D[xi-1], th)) label = min(label, L[xi-1]);
          if (xi - nc - 1 >= 0 && inCluster_gpu(Dxi, D[xi-nc-1], th)) label = min(label, L[xi-nc-1]);
          if (xi + nc - 1 < N  && inCluster_gpu(Dxi, D[xi+nc-1], th)) label = min(label, L[xi+nc-1]);
        }
        if (r + 1 != nc) {
          if (inCluster_gpu(Dxi, D[xi+1], th)) label = min(label, L[xi+1]);
          if (xi - nc + 1 >= 0 && inCluster_gpu(Dxi, D[xi-nc+1], th)) label = min(label, L[xi-nc+1]);
          if (xi + nc + 1 < N  && inCluster_gpu(Dxi, D[xi+nc+1], th)) label = min(label, L[xi+nc+1]);
        }
      }
      if (label < L[xi]) {
        //atomicMin(&R[L[xi]], label);
        R[L[xi]] = label;
        *m = true;
      }
    }
  }

  __global__ void analysis_gpu(const long nthreads,
                               const long nb, const long nr, const long nc,
                               long *labs, long *refs) {
    CUDA_KERNEL_LOOP(index, nthreads) {
      const long N = nr * nc;
      long xi = index % N;
      long bi = (index / N);

      long *L = labs + bi*N;
      long *R = refs + bi*N;

      long label = L[xi];
      long ref;
      if (label == xi) {
        do { label = R[ref = label]; } while (ref ^ label);
        R[xi] = label;
      }
    }
  }



  __global__ void labeling_gpu(const long nthreads,
                               const long nb, const long nr, const long nc,
                               long *labs, long *refs) {
    CUDA_KERNEL_LOOP(index, nthreads) {
      const long N = nr * nc;
      long xi = index % N;
      long bi = (index / N);

      long *L = labs + bi*N;
      long *R = refs + bi*N;

      if ((L[xi] >= 0) && (R[L[xi]] >= 0))
        L[xi] = R[R[L[xi]]];
    }
  }


  void printLabel_gpu(long *res_gpu, long nb, long nr, long nc) {
    long bi, ri, ci, i;
    long *res = (long *)malloc(sizeof(long)*(nb*nr*nc));
    cudaMemcpy(res, res_gpu, sizeof(long)*(nb*nr*nc), cudaMemcpyDeviceToHost);

    i = 0;
    for (bi=0; bi<nb; bi++){
      printf("----------\nb %ld  %ld %ld %ld\n", bi, nb, nr, nc);
      for (ri=0; ri<nr; ri++) {
        for (ci=0; ci<nc; ci++){
          printf("%ld ", res[i]);
          i++;
        }
        printf("\n");
      }
    }
    printf("\n");
    free(res);
  }

  void run_ccl_cuda(float *probs, long *labs, long *refs, long nb, long nr, long nc, long degree, float th) {
    // the first element has to be in background
    float *Dd = probs;
    long *Ld = labs;
    long *Rd = refs;
    long N = nr*nc*nb;

    bool* md;
    cudaMalloc((void**)&md, sizeof(bool));

    dim3 grid(GET_BLOCKS(N));
    dim3 threads(CUDA_NUM_THREADS);

    //printf("--------------------------------------------------------------------------------\n");
    //printf("disp \n");
    //printLabel_gpu(Ld, nb, nr, nc);
    //printf("start\n");


    long step = 0;
    for (;;) {
      //printf("\nstep %d\n", step);
      bool m = false;
      cudaMemcpy(md, &m, sizeof(bool), cudaMemcpyHostToDevice);
      //printf("about to scan\n");
      if (degree == 4) scanning_gpu<<<grid, threads>>>(N, nb, nr, nc, th, Dd, Ld, Rd, md);
      else scanning8_gpu<<<grid, threads>>>(N, nb, nr, nc, th, Dd, Ld, Rd, md);
      cudaMemcpy(&m, md, sizeof(bool), cudaMemcpyDeviceToHost);
      if (!m) break;
      //printf("scanning done\n");
      analysis_gpu<<<grid, threads>>>(N, nb, nr, nc, Ld, Rd);
      //printf("analysis done\n");
      labeling_gpu<<<grid, threads>>>(N, nb, nr, nc, Ld, Rd);
      //printf("labeling done\n");
      //printLabel_gpu(Ld, nb, nr, nc);

      step++;
    }

    //printf("Result\n");
    //printLabel_gpu(Ld, nb, nr, nc);
    //printf("--------------------------------------------------------------------------------\n");
    cudaFree(md);
  }

  void run_init_cuda(float *probs, long *labs, long *refs, long nb, long nr, long nc) {
    long N = nb * nr * nc;
    dim3 grid(GET_BLOCKS(N));
    dim3 threads(CUDA_NUM_THREADS);
    init_ccl_gpu<<<grid, threads>>>(N, nb, nr, nc, probs, labs, refs);
  }

  //////////////////////////////////////////////////
  /// upsampling
  //////////////////////////////////////////////////

  __global__ void ccl_upsample_forward_gpu(const long nthreads,
                                           const long batchsize, const long channels, const long nr1, const long nc1,
                                           const long height2, const long width2,
                                           const float *inp, const long *bb,
                                           float *out) {
    CUDA_KERNEL_LOOP(index, nthreads) {
      const long w2 = index % width2; // 0:width2-1
      const long h2 = (index / width2) % height2; // 0:height2-1
      const long bi = index / height2 / width2;

      const long rmin = bb[bi*4 + 0];
      const long rmax = bb[bi*4 + 1];
      const long cmin = bb[bi*4 + 2];
      const long cmax = bb[bi*4 + 3];

      const long height1 = rmax - rmin + 1;
      const long width1 = cmax - cmin + 1;

      const float rheight =(height2 > 1) ? (float)(height1 - 1)/(height2 - 1) : 0.f;
      const float rwidth = (width2 > 1) ? (float)(width1 - 1) / (width2 - 1) : 0.f;

      // special case: just copy
      if ((height1 == height2) && (width1 == width2)) {
        const long h1 = h2;
        const long w1 = w2;
        for (long c = 0; c < channels; ++c) {
          const float *data1 = inp + bi*channels*nr1*nc1 + c*nr1*nc1;
          float *data2 = out + bi*channels*nr1*nc1 + c*nr1*nc1;
          const float val = v2d(data1, nc1, rmin+h1,  cmin+w1);
          v2d(data2, width2, h2, w2) = val;
        }
      }
      else {
        //
        const float h1r = rheight * h2;
        const long h1 = h1r;
        const long h1p = (h1 < height1 - 1) ? 1 : 0;
        const float h1lambda = h1r - h1;
        const float h0lambda = float(1) - h1lambda;
        //
        const float w1r = rwidth * w2;
        const long w1 = w1r;
        const long w1p = (w1 < width1 - 1) ? 1 : 0;
        const float w1lambda = w1r - w1;
        const float w0lambda = float(1) - w1lambda;
        //
        for (long c = 0; c < channels; ++c) {
          const float *data1 = inp + bi*channels*nr1*nc1 + c*nr1*nc1;
          float *data2 = out + bi*channels*nr1*nc1 + c*nr1*nc1;

          const float val = h0lambda * (w0lambda * v2d(data1, nc1, rmin+h1, cmin+w1)
                                        + w1lambda * v2d(data1, nc1, rmin+h1, cmin+w1+w1p))
            + h1lambda * (w0lambda * v2d(data1, nc1, rmin+h1+h1p, cmin+w1)
                          + w1lambda * v2d(data1, nc1, rmin+h1+h1p, cmin+w1+w1p));
          v2d(data2, width2, h2, w2) = val;
        }
      }
    }
  }


  // Backward (adjoint) operation 1 <- 2 (accumulates)
  __global__ void ccl_upsample_backward_gpu(const long nthreads,
                                            const long batchsize, const long channels, const long nr1, const long nc1,
                                            const long height2, const long width2,
                                            const float *gout, const long *bb,
                                            float *ginp) {
    CUDA_KERNEL_LOOP(index, nthreads) {
      const long w2 = index % width2; // 0:width2-1
      const long h2 = (index / width2) % height2; // 0:height2-1
      const long bi = index / height2 / width2;

      const long rmin = bb[bi*4 + 0];
      const long rmax = bb[bi*4 + 1];
      const long cmin = bb[bi*4 + 2];
      const long cmax = bb[bi*4 + 3];

      const long height1 = rmax - rmin + 1;
      const long width1 = cmax - cmin + 1;


      const float rheight =(height2 > 1) ? (float)(height1 - 1)/(height2 - 1) : 0.f;
      const float rwidth = (width2 > 1) ? (float)(width1 - 1) / (width2 - 1) : 0.f;

      // special case: just copy
      if ((height1 == height2) && (width1 == width2)) {
        const long h1 = h2;
        const long w1 = w2;
        for (long c = 0; c < channels; ++c) {
          float *data1 = ginp + bi*channels*nr1*nc1 + c*nr1*nc1;
          const float *data2 = gout + bi*channels*nr1*nc1 + c*nr1*nc1;

          const float val = v2d(data2, width2, h2, w2);
          v2d(data1, nc1, rmin+h1, cmin+w1) = val;
        }
      }
      else {
        //
        const float h1r = rheight * h2;
        const long h1 = h1r;
        const long h1p = (h1 < height1 - 1) ? 1 : 0;
        const float h1lambda = h1r - h1;
        const float h0lambda = float(1) - h1lambda;
        //
        const float w1r = rwidth * w2;
        const long w1 = w1r;
        const long w1p = (w1 < width1 - 1) ? 1 : 0;
        const float w1lambda = w1r - w1;
        const float w0lambda = float(1) - w1lambda;
        //
        for (long c = 0; c < channels; ++c) {
          float *data1 = ginp + bi*channels*nr1*nc1 + c*nr1*nc1;
          const float *data2 = gout + bi*channels*nr1*nc1 + c*nr1*nc1;

          const float d2val = v2d(data2, width2, h2, w2);
          atomicAdd(l2d(data1, nc1, rmin+h1, cmin+w1),
                    h0lambda * w0lambda * d2val);
          atomicAdd(l2d(data1, nc1, rmin+h1, cmin+w1+w1p),
                    h0lambda * w1lambda * d2val);
          atomicAdd(l2d(data1, nc1, rmin+h1+h1p, cmin+w1),
                    h1lambda * w0lambda * d2val);
          atomicAdd(l2d(data1, nc1, rmin+h1+h1p, cmin+w1+w1p),
                    h1lambda * w1lambda * d2val);
        }
      }
    }
  }

  void run_upsample_forward_cuda(const long batchsize, const long channels, const long nr1, const long nc1,
                                 const long height2, const long width2,
                                 const float *inp, const long *bb,
                                 float *out) {
    long N = batchsize * height2 * width2;
    dim3 grid(GET_BLOCKS(N));
    dim3 threads(CUDA_NUM_THREADS);
    ccl_upsample_forward_gpu<<<grid, threads>>>(N,
                                                batchsize, channels, nr1, nc1,
                                                height2, width2,
                                                inp, bb,
                                                out);
  }
  void run_upsample_backward_cuda(const long batchsize, const long channels, const long nr1, const long nc1,
                                  const long height2, const long width2,
                                  const float *gout, const long *bb,
                                  float *ginp) {
    long N = batchsize * height2 * width2;
    dim3 grid(GET_BLOCKS(N));
    dim3 threads(CUDA_NUM_THREADS);
    ccl_upsample_backward_gpu<<<grid, threads>>>(N,
                                                 batchsize, channels, nr1, nc1,
                                                 height2, width2,
                                                 gout, bb,
                                                 ginp);
  }



  __global__ void DiffL1ForwardFloat(const long nthreads,
                                const float* v1, const float* v2, float *out,
                                     const long nv1, const long nv2, const long ndim){
    CUDA_KERNEL_LOOP(index, nthreads) {
      long i2 = index % nv2;
      long i1 = (index / nv2) % nv1;
      long ib = (index / nv2 / nv1);

      float c1 = 0;
      float c2 = 0;
      float acc = 0;

      v1 += (ib * nv1 + i1) * ndim;
      v2 += (ib * nv2 + i2) * ndim;

      for (long i = 0; i < ndim; i++) {
        c1 = v1[i];
        c2 = v2[i];
        acc += abs(c1-c2);
      }
      out[index] = acc/ndim;
    }
  }

  __global__ void DiffL1BackwardFloat(const long nthreads,
                                 const float* v1, const float* v2, const float* g, float *gi,
                                      const long nv1, const long nv2, const long ndim, bool flip) {
    CUDA_KERNEL_LOOP(index, nthreads) {
      long id = index % ndim;
      long i1 = (index / ndim) % nv1;
      long ib = (index / ndim / nv1);

      float cg = 0;
      float c1 = 0;
      float c2 = 0;
      double acc = 0;

      g += ib * nv1 * nv2;
      v2 += ib * nv2 * ndim;
      c1 = v1[index];

      for (long i2 = 0; i2 < nv2; i2++) {
        cg = (flip) ? g[i2*nv1 + i1] : g[i1*nv2 + i2];
        c2 = v2[i2 * ndim + id];
        acc += (c1 > c2) ? cg: (-cg);
      }
      gi[index] = acc/ndim;
    }
  }

  __global__ void DiffL1ForwardDouble(const long nthreads,
                                const double* v1, const double* v2, double *out,
                                const long nv1, const long nv2, const long ndim){
    CUDA_KERNEL_LOOP(index, nthreads) {
      long i2 = index % nv2;
      long i1 = (index / nv2) % nv1;
      long ib = (index / nv2 / nv1);

      double c1 = 0;
      double c2 = 0;
      double acc = 0;

      v1 += (ib * nv1 + i1) * ndim;
      v2 += (ib * nv2 + i2) * ndim;

      for (long i = 0; i < ndim; i++) {
        c1 = v1[i];
        c2 = v2[i];
        acc += abs(c1-c2);
      }
      out[index] = acc/ndim;
    }
  }

  __global__ void DiffL1BackwardDouble(const long nthreads,
                                       const double* v1, const double* v2, const double* g, double *gi,
                                       const long nv1, const long nv2, const long ndim, bool flip) {
    CUDA_KERNEL_LOOP(index, nthreads) {
      long id = index % ndim;
      long i1 = (index / ndim) % nv1;
      long ib = (index / ndim / nv1);

      double cg = 0;
      double c1 = 0;
      double c2 = 0;
      double acc = 0;

      g += ib * nv1 * nv2;
      v2 += ib * nv2 * ndim;
      c1 = v1[index];

      for (long i2 = 0; i2 < nv2; i2++) {
        cg = (flip) ? g[i2*nv1 + i1] : g[i1*nv2 + i2];
        c2 = v2[i2 * ndim + id];
        acc += (c1 > c2) ? cg: (-cg);
      }
      gi[index] = acc/ndim;
    }
  }


  void run_pairwisediffl1_forward_cuda(float *v1, float *v2, float *diff, long nb, long nv1, long nv2, long ndim) {
    long N = nb *nv1 * nv2;
    dim3 grid(GET_BLOCKS(N));
    dim3 threads(CUDA_NUM_THREADS);
    DiffL1ForwardFloat<<<grid, threads>>>(N, v1, v2, diff, nv1, nv2, ndim);
  }

  void run_pairwisediffl1_backward_cuda(float *v1, float *v2, float *go, float *gi1, float *gi2, long nb, long nv1, long nv2, long ndim) {
    {
      long N = nb *nv1 * ndim;
      dim3 grid(GET_BLOCKS(N));
      dim3 threads(CUDA_NUM_THREADS);
      DiffL1BackwardFloat<<<grid, threads>>>(N, v1, v2, go, gi1, nv1, nv2, ndim, false);
    }
    {
      long N = nb *nv2 * ndim;
      dim3 grid(GET_BLOCKS(N));
      dim3 threads(CUDA_NUM_THREADS);
      DiffL1BackwardFloat<<<grid, threads>>>(N, v2, v1, go, gi2, nv2, nv1, ndim, true);
    }
  }


  void run_pairwisediffl1_forward_cudadouble(double *v1, double *v2, double *diff, long nb, long nv1, long nv2, long ndim) {
    long N = nb *nv1 * nv2;
    dim3 grid(GET_BLOCKS(N));
    dim3 threads(CUDA_NUM_THREADS);
    DiffL1ForwardDouble<<<grid, threads>>>(N, v1, v2, diff, nv1, nv2, ndim);
  }

  void run_pairwisediffl1_backward_cudadouble(double *v1, double *v2, double *go, double *gi1, double *gi2, long nb, long nv1, long nv2, long ndim) {
    {
      long N = nb *nv1 * ndim;
      dim3 grid(GET_BLOCKS(N));
      dim3 threads(CUDA_NUM_THREADS);
      DiffL1BackwardDouble<<<grid, threads>>>(N, v1, v2, go, gi1, nv1, nv2, ndim, false);
    }
    {
      long N = nb *nv2 * ndim;
      dim3 grid(GET_BLOCKS(N));
      dim3 threads(CUDA_NUM_THREADS);
      DiffL1BackwardDouble<<<grid, threads>>>(N, v2, v1, go, gi2, nv2, nv1, ndim, true);
    }
  }



  //////////////////////////////////////////////////
  /// L2
  //////////////////////////////////////////////////

  __global__ void DiffL2ForwardFloat(const long nthreads,
                                const float* v1, const float* v2, float *out,
                                     const long nv1, const long nv2, const long ndim){
    CUDA_KERNEL_LOOP(index, nthreads) {
      long i2 = index % nv2;
      long i1 = (index / nv2) % nv1;
      long ib = (index / nv2 / nv1);

      float c1 = 0;
      float c2 = 0;
      float acc = 0;

      v1 += (ib * nv1 + i1) * ndim;
      v2 += (ib * nv2 + i2) * ndim;

      for (long i = 0; i < ndim; i++) {
        c1 = v1[i];
        c2 = v2[i];
        acc += (c1-c2)*(c1-c2);
      }
      out[index] = acc/ndim;
    }
  }

  __global__ void DiffL2BackwardFloat(const long nthreads,
                                 const float* v1, const float* v2, const float* g, float *gi,
                                      const long nv1, const long nv2, const long ndim, bool flip) {
    CUDA_KERNEL_LOOP(index, nthreads) {
      long id = index % ndim;
      long i1 = (index / ndim) % nv1;
      long ib = (index / ndim / nv1);

      float cg = 0;
      float c1 = 0;
      float c2 = 0;
      double acc = 0;

      g += ib * nv1 * nv2;
      v2 += ib * nv2 * ndim;
      c1 = v1[index];

      for (long i2 = 0; i2 < nv2; i2++) {
        cg = (flip) ? g[i2*nv1 + i1] : g[i1*nv2 + i2];
        c2 = v2[i2 * ndim + id];
        acc += 2*cg*(c1-c2);
      }
      gi[index] = acc/ndim;
    }
  }

  __global__ void DiffL2ForwardDouble(const long nthreads,
                                const double* v1, const double* v2, double *out,
                                const long nv1, const long nv2, const long ndim){
    CUDA_KERNEL_LOOP(index, nthreads) {
      long i2 = index % nv2;
      long i1 = (index / nv2) % nv1;
      long ib = (index / nv2 / nv1);

      double c1 = 0;
      double c2 = 0;
      double acc = 0;

      v1 += (ib * nv1 + i1) * ndim;
      v2 += (ib * nv2 + i2) * ndim;

      for (long i = 0; i < ndim; i++) {
        c1 = v1[i];
        c2 = v2[i];
        acc += (c1-c2)*(c1-c2);
      }
      out[index] = acc/ndim;
    }
  }

  __global__ void DiffL2BackwardDouble(const long nthreads,
                                       const double* v1, const double* v2, const double* g, double *gi,
                                       const long nv1, const long nv2, const long ndim, bool flip) {
    CUDA_KERNEL_LOOP(index, nthreads) {
      long id = index % ndim;
      long i1 = (index / ndim) % nv1;
      long ib = (index / ndim / nv1);

      double cg = 0;
      double c1 = 0;
      double c2 = 0;
      double acc = 0;

      g += ib * nv1 * nv2;
      v2 += ib * nv2 * ndim;
      c1 = v1[index];

      for (long i2 = 0; i2 < nv2; i2++) {
        cg = (flip) ? g[i2*nv1 + i1] : g[i1*nv2 + i2];
        c2 = v2[i2 * ndim + id];
        acc += 2*cg*(c1-c2);
      }
      gi[index] = acc/ndim;
    }
  }


  void run_pairwisediffl2_forward_cuda(float *v1, float *v2, float *diff, long nb, long nv1, long nv2, long ndim) {
    long N = nb *nv1 * nv2;
    dim3 grid(GET_BLOCKS(N));
    dim3 threads(CUDA_NUM_THREADS);
    DiffL2ForwardFloat<<<grid, threads>>>(N, v1, v2, diff, nv1, nv2, ndim);
  }

  void run_pairwisediffl2_backward_cuda(float *v1, float *v2, float *go, float *gi1, float *gi2, long nb, long nv1, long nv2, long ndim) {
    {
      long N = nb *nv1 * ndim;
      dim3 grid(GET_BLOCKS(N));
      dim3 threads(CUDA_NUM_THREADS);
      DiffL2BackwardFloat<<<grid, threads>>>(N, v1, v2, go, gi1, nv1, nv2, ndim, false);
    }
    {
      long N = nb *nv2 * ndim;
      dim3 grid(GET_BLOCKS(N));
      dim3 threads(CUDA_NUM_THREADS);
      DiffL2BackwardFloat<<<grid, threads>>>(N, v2, v1, go, gi2, nv2, nv1, ndim, true);
    }
  }


  void run_pairwisediffl2_forward_cudadouble(double *v1, double *v2, double *diff, long nb, long nv1, long nv2, long ndim) {
    long N = nb *nv1 * nv2;
    dim3 grid(GET_BLOCKS(N));
    dim3 threads(CUDA_NUM_THREADS);
    DiffL2ForwardDouble<<<grid, threads>>>(N, v1, v2, diff, nv1, nv2, ndim);
  }

  void run_pairwisediffl2_backward_cudadouble(double *v1, double *v2, double *go, double *gi1, double *gi2, long nb, long nv1, long nv2, long ndim) {
    {
      long N = nb *nv1 * ndim;
      dim3 grid(GET_BLOCKS(N));
      dim3 threads(CUDA_NUM_THREADS);
      DiffL2BackwardDouble<<<grid, threads>>>(N, v1, v2, go, gi1, nv1, nv2, ndim, false);
    }
    {
      long N = nb *nv2 * ndim;
      dim3 grid(GET_BLOCKS(N));
      dim3 threads(CUDA_NUM_THREADS);
      DiffL2BackwardDouble<<<grid, threads>>>(N, v2, v1, go, gi2, nv2, nv1, ndim, true);
    }
  }


  //////////////////////////////////////////////////
  /// DLSE : Dist log sum exp
  //////////////////////////////////////////////////

  __global__ void DLSEForwardFloat(const long nthreads,
                                   float *out,
                                   const float *si_j_, const float *di_, float *djj_,
                                   const float sigma2,
                                   const long nb, const long nnbr, const long ns, const long nsup){
    CUDA_KERNEL_LOOP(index, nthreads) {
      long j = index % nsup;
      long i1 = (index / nsup) % ns;
      long i_ = (index / nsup / ns) % nnbr;
      long ib = (index / nsup / ns / nnbr);

      float tmp = 0;
      float acc = 0;


      si_j_ += ((ib * nnbr + i_) * ns + i1) * nsup;
      di_ += i_;
      djj_ += j * nsup;


      for (long j_ = 0; j_ < nsup; j_++) {
        tmp = si_j_[j_] - pow(di_[0] - djj_[j_], 2) / sigma2;
        acc += exp(tmp);
      }
      out[index] = log(acc);
    }
  }

  __global__ void DLSEBackwardFloat(const long nthreads,
                                    float *gin,
                                    const float *gj, const float *out, const float *si_j_,
                                    const float *di_, float *djj_,
                                    const float sigma2,
                                    const long nb, const long nnbr, const long ns, const long nsup) {
    CUDA_KERNEL_LOOP(index, nthreads) {
      long j_ = index % nsup;
      long i1 = (index / nsup) % ns;
      long i_ = (index / nsup / ns) % nnbr;
      long ib = (index / nsup / ns / nnbr);

      float tmp = 0;
      float acc = 0;


      gj += (ib * ns + i1) * nsup;
      di_ += i_;
      si_j_ += ((ib * nnbr + i_) * ns + i1) * nsup + j_;
      out += ((ib * nnbr + i_) * ns + i1) * nsup;

      for (long j = 0; j < nsup; j++) {
        tmp = -out[j] + si_j_[0] - pow(di_[0] - djj_[j * nsup + j_], 2) / sigma2;
        acc += gj[j] * exp(tmp);
      }
      gin[index] = acc;
    }
  }


  void run_dlse_forward_cuda(float *out, float *si_j_, float *di_, float *djj_, float sigma2, long nb, long nnbr, long ns, long nsup) {
    long N = nb * nnbr * ns * nsup;
    dim3 grid(GET_BLOCKS(N));
    dim3 threads(CUDA_NUM_THREADS);
    DLSEForwardFloat<<<grid, threads>>>(N, out, si_j_, di_, djj_, sigma2, nb, nnbr, ns, nsup);
  }

  void run_dlse_backward_cuda(float *gin, float *gout, float *out, float *si_j_, float *di_, float *djj_, float sigma2, long nb, long nnbr, long ns, long nsup) {
    long N = nb * nnbr * ns * nsup;
    dim3 grid(GET_BLOCKS(N));
    dim3 threads(CUDA_NUM_THREADS);
    DLSEBackwardFloat<<<grid, threads>>>(N, gin, gout, out, si_j_, di_, djj_, sigma2, nb, nnbr, ns, nsup);
  }




  __global__ void DLSEForwardDouble(const long nthreads,
                                   double *out,
                                   const double *si_j_, const double *di_, double *djj_,
                                   const double sigma2,
                                   const long nb, const long nnbr, const long ns, const long nsup){
    CUDA_KERNEL_LOOP(index, nthreads) {
      long j = index % nsup;
      long i1 = (index / nsup) % ns;
      long i_ = (index / nsup / ns) % nnbr;
      long ib = (index / nsup / ns / nnbr);

      double tmp = 0;
      double acc = 0;


      si_j_ += ((ib * nnbr + i_) * ns + i1) * nsup;
      di_ += i_;
      djj_ += j * nsup;


      for (long j_ = 0; j_ < nsup; j_++) {
        tmp = si_j_[j_] - pow(di_[0] - djj_[j_], 2) / sigma2;
        acc += exp(tmp);
      }
      out[index] = log(acc);
    }
  }

  __global__ void DLSEBackwardDouble(const long nthreads,
                                    double *gin,
                                    const double *gj, const double *out, const double *si_j_,
                                    const double *di_, double *djj_,
                                    const double sigma2,
                                    const long nb, const long nnbr, const long ns, const long nsup) {
    CUDA_KERNEL_LOOP(index, nthreads) {
      long j_ = index % nsup;
      long i1 = (index / nsup) % ns;
      long i_ = (index / nsup / ns) % nnbr;
      long ib = (index / nsup / ns / nnbr);

      double tmp = 0;
      double acc = 0;


      gj += (ib * ns + i1) * nsup;
      di_ += i_;
      si_j_ += ((ib * nnbr + i_) * ns + i1) * nsup + j_;
      out += ((ib * nnbr + i_) * ns + i1) * nsup;

      for (long j = 0; j < nsup; j++) {
        tmp =  si_j_[0] - pow(di_[0] - djj_[j * nsup + j_], 2) / sigma2;
        acc += gj[j] * exp(tmp - out[j]);
      }
      gin[index] = acc;
    }
  }


  void run_dlse_forward_cudadouble(double *out, double *si_j_, double *di_, double *djj_, double sigma2, long nb, long nnbr, long ns, long nsup) {
    long N = nb * nnbr * ns * nsup;
    dim3 grid(GET_BLOCKS(N));
    dim3 threads(CUDA_NUM_THREADS);
    DLSEForwardDouble<<<grid, threads>>>(N, out, si_j_, di_, djj_, sigma2, nb, nnbr, ns, nsup);
  }

  void run_dlse_backward_cudadouble(double *gin, double *gout, double *out, double *si_j_, double *di_, double *djj_, double sigma2, long nb, long nnbr, long ns, long nsup) {
    long N = nb * nnbr * ns * nsup;
    dim3 grid(GET_BLOCKS(N));
    dim3 threads(CUDA_NUM_THREADS);
    DLSEBackwardDouble<<<grid, threads>>>(N, gin, gout, out, si_j_, di_, djj_, sigma2, nb, nnbr, ns, nsup);
  }



#ifdef __cplusplus
}
#endif
