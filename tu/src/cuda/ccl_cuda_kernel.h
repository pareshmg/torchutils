#ifndef _CCL_CUDA_KERNEL
#define _CCL_CUDA_KERNEL


#ifdef __cplusplus
extern "C" {
#endif


  void run_ccl_cuda(float *probs, long *labs, long *refs, long nb, long nr, long nc, long degree, float th);
  void run_init_cuda(float *probs, long *labs, long *refs, long nb, long nr, long nc);

  void run_upsample_forward_cuda(const long batchsize, const long channels, const long nr1, const long nc1,
                                 const long height2, const long width2,
                                 const float *inp, const long *bb,
                                 float *out);
  void run_upsample_backward_cuda(const long batchsize, const long channels, const long nr1, const long nc1,
                                  const long height2, const long width2,
                                  const float *gout, const long *bb,
                                  float *ginp);
  void run_pairwisediffl1_forward_cuda(float *v1, float *v2, float *diff, long nb, long nv1, long nv2, long ndim);
  void run_pairwisediffl1_backward_cuda(float *v1, float *v2, float *go, float *gi1, float *gi2, long nb, long nv1, long nv2, long ndim);


  void run_pairwisediffl1_forward_cudadouble(double *v1, double *v2, double *diff, long nb, long nv1, long nv2, long ndim);
  void run_pairwisediffl1_backward_cudadouble(double *v1, double *v2, double *go, double *gi1, double *gi2, long nb, long nv1, long nv2, long ndim);

  void run_pairwisediffl2_forward_cuda(float *v1, float *v2, float *diff, long nb, long nv1, long nv2, long ndim);
  void run_pairwisediffl2_backward_cuda(float *v1, float *v2, float *go, float *gi1, float *gi2, long nb, long nv1, long nv2, long ndim);


  void run_pairwisediffl2_forward_cudadouble(double *v1, double *v2, double *diff, long nb, long nv1, long nv2, long ndim);
  void run_pairwisediffl2_backward_cudadouble(double *v1, double *v2, double *go, double *gi1, double *gi2, long nb, long nv1, long nv2, long ndim);





  void run_dlse_forward_cuda(float *out, float *si_j_, float *di_, float *djj_, float sigma2, long nb, long nnbr, long ns, long nsup);
  void run_dlse_backward_cuda(float *gin, float *gout, float *out, float *si_j_, float *di_, float *djj_, float sigma2, long nb, long nnbr, long ns, long nsup);

void run_dlse_forward_cudadouble(double *out, double *si_j_, double *di_, double *djj_, double sigma2, long nb, long nnbr, long ns, long nsup);
  void run_dlse_backward_cudadouble(double *gin, double *gout, double *out, double *si_j_, double *di_, double *djj_, double sigma2, long nb, long nnbr, long ns, long nsup);



#ifdef __cplusplus
}
#endif


#endif
