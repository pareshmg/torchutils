#include <TH/TH.h>

// Marathon Match - CCL - Label Equivalence
#include <stdio.h>

#define max(a,b)                                \
  ({ __typeof__ (a) _a = (a);                   \
    __typeof__ (b) _b = (b);                    \
    _a > _b ? _a : _b; })
#define min(a,b)                                \
  ({ __typeof__ (a) _a = (a);                   \
    __typeof__ (b) _b = (b);                    \
    _a < _b ? _a : _b; })

int ccl_lib_add_forward(THFloatTensor *input1, THFloatTensor *input2,
                        THFloatTensor *output)
{
  if (!THFloatTensor_isSameSizeAs(input1, input2))
    return 0;
  THFloatTensor_resizeAs(output, input1);
  THFloatTensor_cadd(output, input1, 1.0, input2);
  return 1;
}

int ccl_lib_add_backward(THFloatTensor *grad_output, THFloatTensor *grad_input)
{
  THFloatTensor_resizeAs(grad_input, grad_output);
  THFloatTensor_fill(grad_input, 1);
  return 1;
}




void init_ccl_cpu(long *L, long *R, long N)
{
  long id;
  for (id = 0; id < N; id++) L[id] = R[id] = id;
}



int ccl_lib_init_forward(THFloatTensor *probs, THLongTensor *labs, THLongTensor *refs) {
  long B = THFloatTensor_size(probs, 0);
  long H = THFloatTensor_size(probs, 1);
  long W = THFloatTensor_size(probs, 2);
  long bi;

  for (bi=0; bi<B; bi++) {
    //float *p = THFloatTensor_data(probs) + (bi * H*W);
    long *l = THLongTensor_data(labs) + (bi * H*W);
    long *r = THLongTensor_data(refs) + (bi * H*W);
    init_ccl_cpu(l, r, H*W);
  }
  return 1;
}


int inCluster_cpu(float d1, float d2, float th)
{
  // both have to be above threshold (otherwise in background)
  return (abs(d1-d2) < th);
}

int scanning_cpu(float *D, long *L, long *R, int N, int W, float th)
{
  int m = 0;
  int id = 0;
  for (id = 0; id < N; id++) {
    float Did = D[id];
    int label = N;
    if (id - W >= 0 && inCluster_cpu(Did, D[id-W], th)) label = min(label, L[id-W]);
    if (id + W < N  && inCluster_cpu(Did, D[id+W], th)) label = min(label, L[id+W]);
    int r = id % W;
    if (r           && inCluster_cpu(Did, D[id-1], th)) label = min(label, L[id-1]);
    if (r + 1 != W  && inCluster_cpu(Did, D[id+1], th)) label = min(label, L[id+1]);

    if (label < L[id]) {
      R[L[id]] = label;
      m = 1;
    }
  }

  return m;
}

int scanning8_cpu(float *D, long *L, long *R, int N, int W, float th)
{
  int m = 0;
  int id = 0;
  for (id = 0; id < N; id++) {
    float Did = D[id];
    int label = N;
    if (id - W >= 0 && inCluster_cpu(Did, D[id-W], th)) label = min(label, L[id-W]);
    if (id + W < N  && inCluster_cpu(Did, D[id+W], th)) label = min(label, L[id+W]);
    int r = id % W;
    if (r) {
      if (inCluster_cpu(Did, D[id-1], th)) label = min(label, L[id-1]);
      if (id - W - 1 >= 0 && inCluster_cpu(Did, D[id-W-1], th)) label = min(label, L[id-W-1]);
      if (id + W - 1 < N  && inCluster_cpu(Did, D[id+W-1], th)) label = min(label, L[id+W-1]);
    }
    if (r + 1 != W) {
      if (inCluster_cpu(Did, D[id+1], th)) label = min(label, L[id+1]);
      if (id - W + 1 >= 0 && inCluster_cpu(Did, D[id-W+1], th)) label = min(label, L[id-W+1]);
      if (id + W + 1 < N  && inCluster_cpu(Did, D[id+W+1], th)) label = min(label, L[id+W+1]);
    }

    if (label < L[id]) {
      R[L[id]] = label;
      m = 1;
    }
  }

  return m;
}

void analysis_cpu(float *D, long *L, long *R, int N)
{
  int id = 0;
  for (id = 0; id < N; id++) {
    int label = L[id];
    int ref;
    if (label == id) {
      do { label = R[ref = label]; } while (ref ^ label);
      R[id] = label;
    }
  }
}

void labeling_cpu(float *D, long *L, long *R, int N)
{
  int id = 0;
  for (id = 0; id < N; id++) L[id] = R[R[L[id]]];
}


void printLabel(long *res, int N, int W) {
  int i;
  for (i=0; i<N; i++) {
    if (i%W == 0) printf("\n");
    printf("%ld ", res[i]);
  }
  printf("\n");
}


void run_ccl_cpu(float *probs, long *labs, long *refs, int nr, int nc, int degree, float th) {
  int N = nr*nc;
  float *D = probs; //static_cast<float*>(&image[0]);
  long *L = labs; //static_cast<int*>(&[0]);
  long *R = refs;

  //printf("--------------------------------------------------------------------------------\n");
  //printLabel(L, N, W);

  int step = 0;
  for (;;) {
    //printf("\nstep %d\n", step);
    int scanningRes = degree == 4 ? scanning_cpu(D, L, R, N, nc, th) : scanning8_cpu(D, L, R, N, nc, th);
    //printf("scanning res %d\n", scanningRes);

    if (!scanningRes) break;
    analysis_cpu(D, L, R, N);
    labeling_cpu(D, L, R, N);
    //printLabel(L, N, W);

    step++;
  }

  //printf("Result\n");
  //printLabel(L, N, W);
  //printf("--------------------------------------------------------------------------------\n");

}



int ccl_lib_ccl_le_forward(THFloatTensor *probs, THLongTensor *labs, THLongTensor *refs, float th) {

  // 3d : batch Row x col input

  long B = THFloatTensor_size(probs, 0);
  long H = THFloatTensor_size(probs, 1);
  long W = THFloatTensor_size(probs, 2);
  long bi;

  for (bi=0; bi<B; bi++){
    float *p = THFloatTensor_data(probs) + (bi * H*W);
    long *l = THLongTensor_data(labs) + (bi * H*W);
    long *r = THLongTensor_data(refs) + (bi * H*W);

    run_ccl_cpu(p, l, r, H, W, 4, th);
  }


  return 1;
}
