# functions/ccl.py
import torch
import torch.cuda
from torch.autograd import Function
from .._ext import ccl_lib
import ipdb

class CclInitFunction(Function):
    """
    """
    def forward(self, probs):
        labs = None
        refs = None
        # run connected componentes
        if not probs.is_cuda:
            labs = torch.LongTensor(*probs.shape)
            refs = torch.LongTensor(*probs.shape)
            ccl_lib.ccl_lib_init_forward(probs, labs, refs)
        else:
            labs = torch.cuda.LongTensor(*probs.shape)
            refs = torch.cuda.LongTensor(*probs.shape)
            ccl_lib.ccl_lib_init_forward_cuda(probs, labs, refs)

        self.mark_non_differentiable(labs, refs)
        return labs, refs


class CclLabelFunction(Function):
    def __init__(self, threshold):
        super(CclLabelFunction, self).__init__()
        self.threshold = threshold

    def forward(self, probs, labs, refs):
        # run connected componentes
        if not probs.is_cuda:
            ccl_lib.ccl_lib_ccl_le_forward(probs, labs, refs, self.threshold)
        else:
            ccl_lib.ccl_lib_ccl_le_forward_cuda(probs, labs, refs, self.threshold)

        self.mark_non_differentiable(labs, refs)
        return labs, refs


class CclUpsampleFunction(Function):
    def __init__(self, osize):
        super(CclUpsampleFunction, self).__init__()
        self.osize = osize

    def forward(self, src, bb):
        # run connected componentes
        assert(src.is_cuda)
        tgt = src.new().resize_(src.size(0), src.size(1), osize[0], osize[1])

        self.save_for_backward(bb)
        ccl_lib.ccl_upsample_forward_cuda(src, bb, tgt)

        return tgt

    def backward(self, grad_output):
        bb, = self.saved_tensors
        assert(grad_output.is_cuda)
        grad_input = grad_output.new().resize_(src.size())

        ccl_lib.ccl_upsample_backward_cuda(grad_output, bb, grad_input)
        return grad_input


class PairwiseDiffL1Function(Function):
    def forward(self, v1, v2):
        """
        v1 :: nb * nv1 * ndim
        v2 :: nb * nv2 * ndim
        out :: nb * nv1 * nv2
        """
        # run connected componentes
        assert(v1.is_cuda)
        assert(v2.is_cuda)
        assert(v1.size(0) == v2.size(0))
        assert(v1.size(2) == v2.size(2))

        diff = v1.new().resize_(v1.size(0), v1.size(1), v2.size(1)).fill_(999)

        self.save_for_backward(v1, v2,diff)
        if v1.type() == 'torch.cuda.FloatTensor':
            ccl_lib.pairwisediffl1_forward_cuda(v1, v2, diff)
        else:
            ccl_lib.pairwisediffl1_forward_cudadouble(v1, v2, diff)
        return diff

    def backward(self, grad_output):
        v1, v2, diff, = self.saved_tensors
        assert(grad_output.is_cuda)
        grad_input1 = grad_output.new().resize_(v1.size()).fill_(999)
        grad_input2 = grad_output.new().resize_(v2.size()).fill_(999)

        if v1.type() == 'torch.cuda.FloatTensor':
            ccl_lib.pairwisediffl1_backward_cuda(v1, v2, grad_output,
                                                 grad_input1, grad_input2)
        else:
            ccl_lib.pairwisediffl1_backward_cudadouble(v1, v2, grad_output,
                                                       grad_input1, grad_input2)
        return grad_input1, grad_input2


class PairwiseDiffL2Function(Function):
    def forward(self, v1, v2):
        """
        v1 :: nb * nv1 * ndim
        v2 :: nb * nv2 * ndim
        out :: nb * nv1 * nv2
        """
        # run connected componentes
        assert(v1.is_cuda)
        assert(v2.is_cuda)
        assert(v1.size(0) == v2.size(0))
        assert(v1.size(2) == v2.size(2))

        diff = v1.new().resize_(v1.size(0), v1.size(1), v2.size(1)).fill_(999)

        self.save_for_backward(v1, v2,diff)
        if v1.type() == 'torch.cuda.FloatTensor':
            ccl_lib.pairwisediffl2_forward_cuda(v1, v2, diff)
        else:
            ccl_lib.pairwisediffl2_forward_cudadouble(v1, v2, diff)
        return diff

    def backward(self, grad_output):
        v1, v2, diff, = self.saved_tensors
        assert(grad_output.is_cuda)
        grad_input1 = grad_output.new().resize_(v1.size()).fill_(999)
        grad_input2 = grad_output.new().resize_(v2.size()).fill_(999)

        if v1.type() == 'torch.cuda.FloatTensor':
            ccl_lib.pairwisediffl2_backward_cuda(v1, v2, grad_output,
                                                 grad_input1, grad_input2)
        else:
            ccl_lib.pairwisediffl2_backward_cudadouble(v1, v2, grad_output,
                                                       grad_input1, grad_input2)
        return grad_input1, grad_input2




class DLSEFunction(Function):
    def __init__(self, sigma2):
        super(DLSEFunction, self).__init__()
        self.sigma2 = sigma2

    def forward(self, si_j_, di_, djj_):
        """
        si_j_ :: [nb, nnbr i_, ns, nsup j_]
        di_ :: [nnbr i_]
        djj_ :: [nsup j, nsup j_]
        """
        f = ccl_lib.dlse_forward_cudadouble
        if si_j_.type() == 'torch.cuda.FloatTensor':
            f = ccl_lib.dlse_forward_cuda


        if not si_j_.is_contiguous(): si_j_ = si_j_.contiguous()
        if not di_.is_contiguous(): di_ = di_.contiguous()
        if not djj_.is_contiguous(): djj_ = djj_.contiguous()
        out = si_j_ * 0
        f(out, si_j_, di_, djj_, self.sigma2)
        return out

    def backward(self, gout, si_j_, di_, djj_):
        f = ccl_lib.dlse_backward_cudadouble
        if si_j_.type() == 'torch.cuda.FloatTensor':
            f = ccl_lib.dlse_backward_cuda

        if not gout.is_contiguous(): gout = gout.contiguous()
        if not si_j_.is_contiguous(): si_j_ = si_j_.contiguous()
        if not di_.is_contiguous(): di_ = di_.contiguous()
        if not djj_.is_contiguous(): djj_ = djj_.contiguous()

        out = self.forward(si_j_, di_, djj_)
        gin = si_j_ * 0
        f(gin, gout, out, si_j_, di_, djj_, self.sigma2)
        return gin
