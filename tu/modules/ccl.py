import torch
from torch.nn.modules.module import Module
from torch.autograd import Variable
from torch.nn import Parameter
from torch import optim
import torch.nn as nn
import torch.nn.functional as F

from ..functions.ccl import *


import ipdb
import os


import warnings
import functools


class PairwiseDiffL1(Module):
    """perform ccl using label equivalence to find connected components. This takes as
    input an existing labeling state so that we can quickly go up the thresholds

    v1 :: nb * nv1 * ndim
    v2 :: nb * nv2 * ndim
    out :: nb * nv1 * nv2
    """
    def forward(self, v1, v2):
        if not v1.is_contiguous(): v1 = v1.contiguous()
        if not v2.is_contiguous(): v2 = v2.contiguous()
        return PairwiseDiffL1Function()(v1, v2)

class PairwiseDiffL2Kernel(Module):
    """perform ccl using label equivalence to find connected components. This takes as
    input an existing labeling state so that we can quickly go up the thresholds

    v1 :: nb * nv1 * ndim
    v2 :: nb * nv2 * ndim
    out :: nb * nv1 * nv2
    """
    def forward(self, v1, v2):
        if not v1.is_contiguous(): v1 = v1.contiguous()
        if not v2.is_contiguous(): v2 = v2.contiguous()
        return PairwiseDiffL2Function()(v1, v2)


class PairwiseDiffL2(Module):
    def forward(self, v1, v2):
        """
        v1 :: [nb, n1, ndim]
        v2 :: [nb, n2, ndim]
        res :: [nb, n1, n2]
        """
        ndim = v1.size(2)
        v1_norm = (v1 ** 2).sum(2).unsqueeze(2) # [nb, n1, 1]
        v2_norm = (v2 ** 2).sum(2).unsqueeze(1) # [nb, 1, n2]
        dist = v1_norm + v2_norm - 2.0 * torch.matmul(v1, v2.transpose(1,2))
        return dist/ndim




def deprecated(func):
    '''This is a decorator which can be used to mark functions
        as deprecated. It will result in a warning being emitted
        when the function is used.'''

    @functools.wraps(func)
    def new_func(*args, **kwargs):
        warnings.warn_explicit(
            "Call to deprecated function {}.".format(func.__name__),
            category=DeprecationWarning,
            filename=func.func_code.co_filename,
            lineno=func.func_code.co_firstlineno + 1
        )
        return func(*args, **kwargs)
    return new_func


class Memoize:
    def __init__(self, fn):
        self.fn = fn
        self.memo = {}
    def __call__(self, *args):
            if args not in self.memo:
                self.memo[args] = self.fn(*args)
            return self.memo[args]


##################################################
### Finding connected components

class CclThresholdCreateModule(Module):
    """create image with 0 centered threshold.

    Module scales the input probabilities to expand to the [0,1] region so that
    thresholds can be picked uniformly in that region.

    phi :: batch * nphi * nr * nc

    res :: batch * nphi * nth * nr * nc
    """
    def __init__(self, thresholds):
        super(CclThresholdCreateModule, self).__init__()
        self.thresholds = thresholds
        self.thresholds.sort()
        self.thresholds = Variable(self.thresholds, requires_grad=False)

    def forward(self, phi):
        assert(phi.ndimension() == 4)
        phi = phi.unsqueeze(2) # batch * nphi * nth * nr * nc
        mn = phi.min(4,keepdim=True)[0].min(3, keepdim=True)[0]
        mx = phi.max(4,keepdim=True)[0].max(3, keepdim=True)[0]
        rng = (mx-mn).clamp(min=1e-4)
        phi01 = (phi-mn)/rng  # scale range to [0,1]
        th = self.thresholds.unsqueeze(0).unsqueeze(1).unsqueeze(3).unsqueeze(4)
        #centeredPhi01 = (phi-mn)/rng - th
        #centeredPhi = phi - (rng * th + mn)
        centeredPhi = phi - th
        centeredPhi01 = centeredPhi

        assert(centeredPhi.ndimension() == 5)
        return centeredPhi01, centeredPhi

class CclInitModule(Module):
    """ Initialize the ccl with a unique label for each point (and all references point to self)
    """
    def forward(self, probs):
        return CclInitFunction()(probs)


class CclLabelModule(Module):
    """perform ccl using label equivalence to find connected components. This takes as
    input an existing labeling state so that we can quickly go up the thresholds.

    Threshold should by default be 0 now. I am scaling the probabilities so
    that they are around 0. This was a bug before

    """
    def __init__(self, threshold):
        super(CclLabelModule, self).__init__()
        self.threshold = threshold

    def forward(self, probs, labs, refs):
        return CclLabelFunction(self.threshold)(probs, labs, refs)

class CclComponentsModule(Module):
    """ Extract the components given a labeling and the unique label ids
    """
    def forward(self, labs, ulabs):
        """
        Arguments:
        - `labs`: long  batch x samples x dims
        - `ulabs`: long   batch x dims
        """
        ulabs = ulabs.unsqueeze(1).unsqueeze(2)
        ulabs = ulabs.expand(ulabs.size(0), labs.size(0), labs.size(1))
        labs = labs.unsqueeze(0).expand(ulabs.size(0), labs.size(0), labs.size(1))
        res = labs == ulabs
        return res



##################################################
### Utils
class CclUpsampleModule(Module):
    """perform ccl using label equivalence to find connected components. This takes as
    input an existing labeling state so that we can quickly go up the thresholds
    """
    def __init__(self, osize):
        super(CclUpsampleModule, self).__init__()
        self.osize = osize

    def forward(self, src, bb):
        return CclUpsampleFunction(self.osize)(src, bb)


class CclBatchLinearModule(Module):
    """Given a (weighted) mask, compute a differentiable shape vector that describes it

    """
    def forward(self, fvs, params):
        """
        Arguments:
        - `fvs`: float [0,1]  batch x samples x dims
        - `params`: float [0,1]  batch x dims
        """
        nbatch = fvs.size(0)
        nsamps = fvs.size(1)
        ndims = fvs.size(2)
        params = params.unsqueeze(1) # (nbatch, 1, ndims)

        linear = (params * fvs).sum(2) # (nbatch, nsamps)

        return linear

class ToProb(nn.Module):
    def __init__(self, nk):
        super(ToProb, self).__init__()
        self.nk = nk
        self.linear1 = nn.Linear(nk, 50)
        self.linear2 = nn.Linear(50, 1)
        self.logistic = nn.Sigmoid()

    def forward(self, x):
        """
        x : float batch x nr x nc x kern
        out : float [0,1] batch x nr x nc
        """
        nb, nr, nc, nk = x.size()
        x = x.view(nb * nr * nc, nk)
        x = self.linear2(F.relu(self.linear1(x)))

        return self.logistic(x.view(nb, nr, nc))


##################################################
### Extracting the shape vector from the components


class CclShapeModule(Module):
    """DEBUG ONLY: Run SGD to find a SVM solution to the cluster given. This is slow. Do not
    use. For debug only

    """
    def __init__(self, nr, nc, sigma, deg=4, regLambda=0.01, nsteps=100):
        super(CclShapeModule, self).__init__()
        self.nr = nr
        self.nc = nc
        self.sigma = sigma
        self.deg = deg
        self.fvs = self.rbfApproximation()
        self.fvs = self.fvs.view(1, nr*nc, self.fvs.size(2)) # (1, nr*nb, ndim)
        self.regLambda = regLambda
        self.marginLoss = nn.SoftMarginLoss(size_average=False)
        self.regLoss = nn.L1Loss(size_average=False)
        self.blm = CclBatchLinearModule()
        self.nsteps = nsteps

    def forward(self, probs, clusters):
        """
        Arguments:
        - `probs`: float [0,1]  batch x samples : weight to multiply by
        - `cluster`: float {-1,1}  batch x samples : background / foreground
        """
        if probs.is_cuda: self.fvs = self.fvs.cuda()
        nb = probs.size(0)
        nd = self.fvs.size(2)
        x = Variable(self.fvs.repeat(nb, 1, 1), requires_grad=False)
        p = None

        if probs.is_cuda:
            p = Parameter(torch.randn(nb, nd).cuda() , requires_grad=True)
        else:
            p = Parameter(torch.randn(nb, nd) , requires_grad=True)

        pt = Variable(p.data * 0, requires_grad=False) # for l1 loss
        y = clusters.view(-1) # for classification


        optimizer = optim.SGD([p], lr=0.01, momentum=0.9)

        for i in range(self.nsteps):
            # Reset gradient
            optimizer.zero_grad()

            # Forward
            fx = self.blm(x, p)
            output1 = self.marginLoss.forward(fx.view(-1), y)
            output2 = self.regLoss.forward(p, pt) * self.regLambda / nd

            # Backward
            output1.backward()
            output2.backward()

            # Update parameters
            optimizer.step()

            if (p.data != p.data).float().sum() > 0:
                ipdb.set_trace()


        # Forward
        fx = self.blm(x, p)
        output1 = self.marginLoss.forward((fx*probs).view(-1), y)
        output2 = self.regLoss.forward(p, pt) * self.regLambda / nd

        #unitP = p/torch.norm(p, p=2, dim=1).unsqueeze(1).expand_as(p)
        #print(clusters)
        #print(self.createCluster(fx > 0).view(nb, self.nr, self.nc) == clusters)

        # Backward
        return p, (output1+output2)


class CclShapeTemplateModule(Module):
    """Bank of fixed shapes and the output should the the average point
    classification error / something that indicates what the probability of
    each shape is

    """
    def __init__(self, nshapes, ndim):
        super(CclShapeTemplateModule, self).__init__()
        self.nshapes = nshapes
        self.ndim = ndim
        # ndim * nshapes
        self.sm = nn.Softmax()
        self.sigmoid = nn.Sigmoid()
        self.initializeWeights()

    def initializeWeights(self):
        """
        phis: nshapes * samples
        clusters: nshapes * samples
        fvs: nshapes * samples * ndim

        """
        self.weights = torch.randn(self.ndim, self.nshapes) * 1e-2
        self.weights.narrow(0,0,1).add_(1)
        self.weights.narrow(0,3,1).add_(-16)
        self.weights.narrow(0,5,1).add_(-16)
        self.weights.narrow(1,self.nshapes//2, self.nshapes//2).mul_(-1)
        self.weights = Parameter(self.weights * 1e-3, requires_grad=True)


    def forward(self, phi, clusters, fvs, xs, sizes):
        """
        Arguments:
        - `phi`: float [0,1]  batch * samples : how sure we are about bkgnd / frgnd
        - `cluster`: float {-1,1}  batch * samples : bkgnd / frgnd
        - `fvs`: float   batch * samples * ndim : shape feature vector associated with point
        - `xs`: float   batch * samples * ndim' : content feature vector associated with point
        - `sizes`: float   batch : number of samples that belong to the cluster

        Output:
        - `res` : float : batch * samples : average logistic loss associated with each shape

        res = \sum_i

        classification = 1/(1+exp(-y \theta^T x))
        """
        assert ((self.weights != self.weights).data.long().sum() == 0), 'found nans in weights'
        nb, ns, ndim = fvs.size()

        # \theta^T x : batch * samples * shapes
        proj = fvs.view(nb*ns, ndim).mm(self.weights).view(nb, ns, self.nshapes)

        # y \theta^T x : batch * samples * shapes
        cl = clusters.unsqueeze(2) * proj
        cl = cl.clamp(min=-10, max=-10)
        # p = 1/ (1+e^(-y \theta^T x))   : batch * samples * shapes
        p = self.sigmoid(cl)

        # batch * samples * shapes
        phi = phi.unsqueeze(2)
        # cross entropy loss : H(phi, theta_prob) = -\sum phi_i log p_i : batch * shapes
        # clamping to prevent inf errors
        llikelihood = (phi * p.log().clamp(min=-10) + (1-phi) * (1-p).log().clamp(min=-10)).mean(1)

        # now softmax across that
        sm = self.sm(llikelihood)

        assert ((llikelihood != llikelihood).data.long().sum() == 0), 'found nans in llikelihood'
        assert ((sm != sm).data.long().sum() == 0), 'found nans in sm'



        ## theta = weighted sum wrt softmax prob : nbatch * ndim * nshapes
        resfv = self.weights.unsqueeze(0).repeat(nb, 1, 1)
        # if 'cat' in self.mode:
        #     ## average content feature vector: nbatch * ndim + ndim
        #     ndimx = xs.size(2)
        #     pDecision = 1-F.relu(proj).mul_(-1).exp() # batch * samples * shapes
        #     pDecision = pDecision.unsqueeze(2) # batch * samples * ndim' * shapes
        #     xs = xs.unsqueeze(3) # batch * samples * ndim' * nshapes
        #     xsAvg = (xs * pDecision).sum(1) / sizes.unsqueeze(1).unsqueeze(1)
        #     resfv = torch.cat([resfv, xsAvg], 1)

        #resfv = resfv * sm.unsqueeze(1)
        #resfv = resfv.sum(2) # nbatch * ndim

        assert ((resfv != resfv).data.long().sum() == 0), 'found nans in resfv'
        return resfv, llikelihood




class ShapeImgModule(nn.Module):
    """ Given a cluster, it selectes a bounding box and resizes that to a W * W image
    """
    def __init__(self, nshapes, width):
        super(ShapeImgModule, self).__init__()
        self.nr = args.nr
        self.nc = args.nc
        self.width = width
        self.padWidth = self.width//2
        #self.pad = nn.ReplicationPad2d(self.padWidth)
        self.pad = nn.ZeroPad2d(self.padWidth)
        self.resizer = nn.Upsample(size=(self.width,self.width), mode='bilinear')
        self.ndim = self.width * self.width

    def forward(self, phi, clusters):
        """
        phi :: ncc * nr * nc
        clusters :: ncc * samples
        """
        nb = clusters.size(0)
        clusters = clusters.view(nb, self.nr, self.nc)
        r = self.pad(phi.view(nb, 1, self.nr, self.nc)).select(1,0)

        w = self.padWidth
        res = []
        bb = []
        bbs = clusters.data.new().long().resize_(nb, 4)
        for bi in range(nb):
            t = clusters.data.select(0,bi) > 0
            ix = torch.nonzero(t.max(0)[0]).select(1,0)
            iy = torch.nonzero(t.max(1)[0]).select(1,0)
            xmin = ix[0]
            xmax = ix[-1]
            ymin = iy[0]
            ymax = iy[-1]
            bbs.select(0,bi).narrow(0,0,1).fill_(ymin)
            bbs.select(0,bi).narrow(0,1,1).fill_(xmin)
            bbs.select(0,bi).narrow(0,2,1).fill_(ymax)
            bbs.select(0,bi).narrow(0,3,1).fill_(xmax)
            c = r.narrow(0,bi,1).narrow(1,ymin, ymax-ymin+1+2*w).narrow(2,xmin, xmax-xmin+1+2*w).unsqueeze(1)

            try:
                res.append(self.resizer(c))
            except Exception as e:
                ipdb.set_trace()

        bbs = Variable(bbs, requires_grad=False)
        res = torch.cat(res, dim=0).view(nb, self.ndim)
        res = res.clamp(min=1e-9)
        res = res.log() - (1-res).log() # go from prob to real :: logit
        return res, bbs  # batch * width^2



class ShapeImgModule2(nn.Module):
    """ Given a cluster, it selectes a bounding box and resizes that to a W * W image
    """
    def __init__(self, nshapes, width):
        super(ShapeImgModule2, self).__init__()
        self.nr = args.nr
        self.nc = args.nc
        self.width = width
        self.padWidth = self.width//2
        #self.pad = nn.ReplicationPad2d(self.padWidth)
        self.pad = nn.ZeroPad2d(self.padWidth)
        self.resizer = nn.Upsample(size=(self.width,self.width), mode='bilinear')
        self.ndim = self.width * self.width

    def forward(self, phi, clusters, fvs, xs, sizes):
        """
        phi :: ncc * nr * nc
        clusters :: ncc * samples
        """
        nb = clusters.size(0)
        clusters = clusters.view(nb, self.nr, self.nc)
        r = self.pad(phi.view(nb, 1, self.nr, self.nc)).select(1,0)

        w = self.padWidth
        res = []
        bb = []
        bbs = clusters.data.new().long().resize_(nb, 4)
        for bi in range(nb):
            t = clusters.data.select(0,bi) > 0
            ix = torch.nonzero(t.max(0)[0]).select(1,0)
            iy = torch.nonzero(t.max(1)[0]).select(1,0)
            xmin = ix[0]
            xmax = ix[-1]
            ymin = iy[0]
            ymax = iy[-1]
            bbs.select(0,bi).narrow(0,0,1).fill_(ymin)
            bbs.select(0,bi).narrow(0,1,1).fill_(xmin)
            bbs.select(0,bi).narrow(0,2,1).fill_(ymax)
            bbs.select(0,bi).narrow(0,3,1).fill_(xmax)
            c = r.narrow(0,bi,1).narrow(1,ymin, ymax-ymin+1+2*w).narrow(2,xmin, xmax-xmin+1+2*w).unsqueeze(1)

            try:
                res.append(self.resizer(c))
            except Exception as e:
                ipdb.set_trace()

        bbs = Variable(bbs, requires_grad=False)
        res = torch.cat(res, dim=0).view(nb, self.ndim)
        return res, bbs  # batch * width^2



##################################################
### RBF kernel
@Memoize
def getPolyKernNPows(n, d):
    """
    n : max power
    d : number of dims being used

    returns m * d  :: m : total number of dimensions in this
    """
    res = [torch.zeros(1,d).long()]
    sel = set([tuple(x) for x in res[0].numpy().tolist()])
    for ni in range(1,n):
        tmps = {}
        for di in range(d):
            tmp = torch.zeros(1,d).long()
            tmp.select(1,di).fill_(1)
            tmp = res[ni-1] + tmp
            tmps.update({tuple(x) : tmp.narrow(0,i,1) for i,x in enumerate(tmp.numpy().tolist())})
        tmps = torch.cat(list(tmps.values()), dim=0)
        res.append(tmps)
    return torch.cat(res, dim=0).float()


class RBFKernelFunction(object):
    """
    computes the polynomial kernel. Currently uses r,theta coordinates
    """
    def __init__(self, nr, nc, deg=4):
        self.nr = nr
        self.nc = nc
        self.deg = deg
        self.ndim = getPolyKernNPows(self.deg+1, 3).size(0)

        self.polykern = torch.cat([self.baseXY().cuda(), self.baseRJ().cuda()], dim=2) # 2nr * 2nc * ndim

    def getXY(self, t):
        return tn.narrow(3,0,5)
    def getRJ(self, t):
        return t.narrow(3,5,3)

    def baseXY(self):
        nr = self.nr
        nc = self.nc
        deg = self.deg

        x1 = torch.arange(0,2*nr).unsqueeze(1).unsqueeze(2).repeat(1,2*nc,1)/self.nr - (self.nr-1)/self.nr
        x2 = torch.arange(0,2*nc).unsqueeze(0).unsqueeze(2).repeat(2*nr,1,1)/self.nc - (self.nc-1)/self.nc
        xs = torch.cat([x1, x2, x1*x1, x1*x2, x2*x2], dim=2)
        return xs

    def baseRJ(self):
        nr = self.nr
        nc = self.nc
        deg = self.deg

        xy = torch.arange(0,2*nr).unsqueeze(1).unsqueeze(2).repeat(1,2*nc,1)/self.nr - (self.nr-1)/self.nr
        xx = torch.arange(0,2*nc).unsqueeze(0).unsqueeze(2).repeat(2*nr,1,1)/self.nc - (self.nc-1)/self.nc

        xr = (xy*xy + xx * xx).sqrt()
        xsin = xx / (xr + (xr == 0).float())
        xcos = xy / (xr + (xr == 0).float())
        xs = torch.cat([xr, xsin, xcos], dim=2)

        return xs

    @deprecated
    def createPolyKern2(self, x1, x2, appendDim=3):
        """
        x1, x2 :: batch * nr * nc * ndim
        """
        fv = [x1*0+1]
        for i in range(1,self.deg+1):
            for j in range(i+1):
                fv.append(x1.pow(j) * x2.pow(i-j))

        xs = torch.cat(fv, dim=appendDim)
        assert (xs.size(appendDim) == self.ndim),  'RBF kernel approximation size mismatch. Expected %d got %d'%(self.ndim, xs.size(2))
        xs = xs.clamp(min=-10, max=10)
        return xs

    def createPolyKernN(self, xs, appendDim=3):
        """
        xs :: batch * nr * nc * indim
        """
        xs = xs.unsqueeze(3)

        pows = getPolyKernNPows(self.deg+1, xs.size(4)) # outdim * indim
        if xs.is_cuda: pows = pows.cuda()
        pows = pows.view(1,1,1,pows.size(0), pows.size(1))

        neg =(((xs < 0).long() * ((pows % 2) == 1).long()).sum(4) % 2 == 1).float() * (-2) + 1

        res = (xs.abs().log().clamp(min=-100) * pows).sum(4).exp().clamp(max=2) * neg
        return res



    def rbfCentered(self, mean):
        """
        mean :: batch * 4  (row, col, r, j)


        if even: eg nr = 8. Mean = 3.5, using 2*nr = 16
        zero is at nr = 8
        Left is at  nr - nr//2 = 8-3 = 5


        if odd: eg nr = 7. Mean = 3. using 2*nr = 14
        center = zero is at 7
        left is at nr - nr//2
        """
        nb = mean.size(0)
        res = mean.new().resize_(nb, self.nr, self.nc, 3)

        meanx = mean.narrow(1,0,2)
        meanr = mean.narrow(1,5,1)
        meansin = mean.narrow(1,6,1)
        meancos = mean.narrow(1,7,1)

        kern = self.polykern.narrow(2,5,3)


        ## center xy
        for bi in range(nb):
            meanOffset = mean.select(0,bi).narrow(0,0,2).cpu() * torch.Tensor([self.nr, self.nc])
            centerIndex = torch.LongTensor([self.nr-1, self.nc-1]) - meanOffset.round().long()
            cornerIndex = centerIndex - torch.LongTensor([(self.nr-1) // 2, (self.nc-1) // 2])
            dr, dc = cornerIndex
            try:
                res.narrow(0,bi,1).copy_(kern.narrow(0,dr, self.nr).narrow(1, dc, self.nc))
            except Exception as e:
                ipdb.set_trace()
                print('hi')


        ## rotate
        r = res.narrow(3,0,1)
        sina = res.narrow(3,1,1)
        cosa = res.narrow(3,2,1)

        b = torch.atan2(meansin, meancos)

        sinb = torch.sin(b).unsqueeze(1).unsqueeze(2)
        cosb = torch.cos(b).unsqueeze(1).unsqueeze(2)

        newsin = sina * cosb - cosa * sinb
        newcos = cosa * cosb + sina * sinb

        res = torch.cat([r, newsin, newcos], dim=3)

        return res

    def clusterFV(self, clusters, sizes):
        """
        clusters: batch * nr * nc
        sizes: batch

        return: batch * nr * nc * ndim
        """
        nr = self.nr
        nc = self.nc
        # find the center of mass and average distance from center
        polykern = self.polykern # 2nr * 2nc * 7
        raw = polykern.narrow(0,nr//2,nr).narrow(1,nc//2,nc)  # nr * nc * 7
        raw = raw.unsqueeze(0) # batch * nr * nc * 7
        rawmask = (clusters > 0).float().unsqueeze(3)
        fvmeans = (raw * rawmask).sum(2).sum(1) / sizes.float().unsqueeze(1) # batch * 7
        mean = fvmeans # batch * 7
        dists = sum([fvmeans.select(1,2) + fvmeans.select(1,4), # x^2 + y^2
                     mean.narrow(1,0,2).pow(2).sum(1), # xm^2 + ym^2
                     (-2) * fvmeans.select(1,0) * mean.select(1,0), # -2 * x * xm
                     (-2) * fvmeans.select(1,1) * mean.select(1,1), # -2 * y * ym
        ]).sqrt() # sqrt( (x-xm)^2 + (y-ym)^2)

        ## extract the feature vectors corresponding to that center of mass
        fvs = self.rbfCentered(mean) # batch * nr * nc * ndim
        fvs.narrow(3,0,1).div_((dists*4).unsqueeze(1).unsqueeze(2).unsqueeze(3))

        fvs = self.createPolyKernN(fvs)
        ## scale so that the averag distance from the center is 1
        return fvs



##################################################
### modules that combine all the above steps together


class ShapeAverageModule(Module):
    """average fv of shapes
    """
    def __init__(self, nshapes, ndim):
        super(ShapeAverageModule, self).__init__()
        self.nshapes = nshapes
        self.ndim = ndim
        # ndim * nshapes
        self.sm = nn.Softmax()
        self.sigmoid = nn.Sigmoid()



    def forward(self, phi, clusters, fvs, xs, sizes):
        """
        Arguments:
        - `phi`: float [0,1]  ncc * (nr * nc) : how sure we are about bkgnd / frgnd
        - `cluster`: float {-1,1}  ncc * samples : bkgnd / frgnd
        - `fvs`: float   batch * samples * ndim : shape feature vector associated with point
        - `xs`: float   batch * samples * ndim' : content feature vector associated with point
        - `sizes`: float   batch : number of samples that belong to the cluster

        Output:
        - `resfv` : float : batch * dim : average logistic loss associated with each shape

        res = \sum_i

        classification = 1/(1+exp(-y \theta^T x))
        """
        phi = phi.unsqueeze(2)
        clusters = (clusters > 0).float().unsqueeze(2)
        shape = (phi * fvs * clusters).mean(1)
        #content = (phi * xs * clusters).mean(1)
        resfv = shape

        assert ((resfv != resfv).data.long().sum() == 0), 'found nans in resfv'
        return resfv



class ShapeNet(nn.Module):
    """Takes in the part classification probability and feature vector and computes
    connected components for different thresholds and returns the shapes

    """
    def __init__(self, nr, nc, nshapes, minSize=5*5, deg=4):
        super(ShapeNet, self).__init__()
        self.nr = nr
        self.nc = nc
        self.minSize = minSize

        self.cclThresh = CclThresholdCreateModule(torch.arange(0,1,0.1).cuda())
        self.cclLabel = CclLabelModule(0)
        self.cclInit = CclInitModule()
        self.cclComponents = CclComponentsModule()
        self.deg = deg
        self.nshapes = nshapes
        self.rbf = RBFKernelFunction(self.nr, self.nc, self.deg)
        self.ndim = self.rbf.ndim

    def createCluster(self, x, t, sizes=None):
        if sizes is None:
            return (x >= t).float() - (x < t).float()
        else:
            wt = (sizes / float(self.nr * self.nc)).unsqueeze(1).unsqueeze(2)
            return (x >= t).float() - (x < t).float() * wt


    def getUniqueLabels(self, l0, l, allowFull=False):
        """
        batch * nr * nc
        """

        msk = l0 == l
        idxs = torch.nonzero(msk)
        li = l0.masked_select(msk)
        if (idxs.ndimension() == 0): return None, None, None, None
        bi = idxs.select(1,0)  # batch index
        pi = idxs.select(1,1) * self.nc + idxs.select(1,2) # phi index

        clusters = (pi.view(-1, 1, 1) == l.index_select(0, bi)).long()
        szs = clusters.sum(2).sum(1)

        sel = None
        # if allowFull:
        #     sel = torch.nonzero((szs >= self.minSize) & (szs <= (sen)))
        # else:

        sel = torch.nonzero((szs >= self.minSize) & (szs <= (self.nr * self.nc - self.minSize)) |
                            ((li == 0) & (szs > 0) ))

        if sel.ndimension() == 0: return None, None, None, None

        assert(sel.size(1) == 1)

        sel = sel.select(1,0)
        if sel.sum() == 0: return None, None, None, None

        bi = bi.index_select(0, sel)
        pi = pi.index_select(0, sel)
        szs = szs.index_select(0, sel)
        clusters = clusters.index_select(0, sel)

        return clusters, bi, pi, szs

    def forward(self, phi_, xs):
        """
        Arguments:
        - `phi`: part prob: float [0,1]  batch * nphi * nr * nc
        - `xs`: content fv: float [0,1]  batch * nr * nc * ndim
        """
        assert ((phi_ != phi_).data.long().sum() == 0), 'found nans in phi in ShapeNet'

        nb = phi_.size(0)
        np = phi_.size(1)
        nr = phi_.size(2)
        nc = phi_.size(3)
        nt = self.cclThresh.thresholds.size(0)

        phic01, phic = self.cclThresh(phi_)
        phic01 = phic01.view(nb*np*nt, nr, nc) # (nb * np * nt) * nr * nc
        phic = phic.view(nb*np*nt, nr, nc) # (nb * np * nt) * nr * nc
        phi = phi_.view(nb*np, nr, nc) # (nb * np * nt) * nr * nc

        l0, r0 = self.cclInit(phic)

        li = Variable(l0.data + 0)
        ri = Variable(r0.data + 0)
        li, ri = self.cclLabel(phic, li, ri)
        clusters, batchIndexs, uli, sizes = self.getUniqueLabels(l0.data, li.data, True)
        # batchIndexs = torch.cat([batchIndexs / (np * nt),  # batch
        #                          (batchIndexs / nt)  % np, # phi
        #                          batchIndexs % nt,# threshold
        #                          batchIndexs], # actual selector
        #                         dim=1)
        assert(clusters is not None)
        clusters = clusters.float()
        batchIndexs = Variable(batchIndexs, requires_grad=False)
        clusters = Variable(clusters, requires_grad=False)
        sizes = Variable(sizes.float(), requires_grad=False)

        #probs = phi_.view(nb*np, nr, nc).index_select(0, batchIndexs / nt)
        probs = phi.index_select(0, batchIndexs/nt)
        #probs = phic.index_select(0, batchIndexs)

        # clusters = self.createCluster(clusters, 0.5, sizes)
        clusters = self.createCluster(clusters, 0.5, None)

        probs = probs.view(probs.size(0), nr*nc)
        clusters = clusters.view(probs.size(0), nr*nc)
        xs = xs.view(nb, nr*nc, xs.size(3)).index_select(0, batchIndexs / (np*nt))

        ## random sample the feature vector
        # TODO

        #theta, llikelihood =  self.cclShape(probss, clusterss, fvs, xs, sizess)
        return {'batchIndex': batchIndexs / (np * nt),
                'clusterIndex': batchIndexs,
                'probs': probs,
                'clusters': clusters,
                'contentfvs': xs,
                'np': np,
                # 'theta': theta,
                # 'shapellikelihood': llikelihood,
                'sizes': sizes}


class KernFVNet(nn.Module):
    def __init__(self, nr, nc, deg=4):
        super(KernFVNet, self).__init__()
        self.nr = nr
        self.nc = nc

        self.deg = deg
        self.rbf = RBFKernelFunction(self.nr, self.nc, self.deg)
        self.ndim = self.rbf.ndim

    def forward(self,clusters, sizes):
        """
        clusters :: nb  * nr * nc
        sizes:: nb
        """
        fvs = Variable(self.rbf.clusterFV(clusters.data, sizes.data), requires_grad=False)
        fvs = fvs.view(fvs.size(0), self.nr*self.nc, fvs.size(3)) # batch * nsamps * dim
        return fvs

class ShapeNetNoCC(nn.Module):
    """Takes in the part classification probability and feature vector and computes
    connected components for different thresholds and returns the shapes

    """
    def __init__(self, nr, nc, nshapes, minSize=5*5, deg=4):
        super(ShapeNetNoCC, self).__init__()
        self.nr = nr
        self.nc = nc
        self.minSize = minSize

        self.deg = deg
        self.nshapes = nshapes
        self.rbf = RBFKernelFunction(self.nr, self.nc, self.deg)
        self.ndim = self.rbf.ndim

    def createCluster(self, x, t, sizes=None):
        if sizes is None:
            return (x >= t).float() - (x < t).float()
        else:
            wt = (sizes / float(self.nr * self.nc)).unsqueeze(1).unsqueeze(2)
            return (x >= t).float() - (x < t).float() * wt


    def getUniqueLabels(self, phi):
        """
        nb * np * nr * nc
        """
        nb, np, nr, nc = phi.size()
        phi = phi.data.unsqueeze(2)

        ## normalize to 1 as debug
        mx = phi.view(nb, np, nr*nc).max(2)[0].view(nb, np, 1, 1, 1)
        phi = phi / mx



        cp = (phi >= 0.5).long()
        clusters = cp.view(nb*np,nr,nc)
        szs = clusters.sum(2).sum(1)

        bi = torch.arange(0,nb).long().view(nb,1,1).repeat(1,np,1).view(nb*np)
        pi = torch.arange(0,np).long().view(1,np,1).repeat(nb,1,1).view(nb*np)

        if clusters.is_cuda:
            bi = bi.cuda()
            pi = pi.cuda()

        return clusters, bi, pi, szs

    def forward(self, phi_, xs):
        """
        Arguments:
        - `phi`: part prob: float [0,1]  batch * nphi * nr * nc
        - `xs`: content fv: float [0,1]  batch * nr * nc * ndim
        """
        assert ((phi_ != phi_).data.long().sum() == 0), 'found nans in phi in ShapeNet'

        nb, np, nr, nc = phi_.size()
        nt = 1  # 0.5
        phi = phi_

        clusters, batchIndexs, uli, sizes = self.getUniqueLabels(phi)
        # batchIndexs = torch.cat([batchIndexs / (np * nt),  # batch
        #                          (batchIndexs / nt)  % np, # phi
        #                          batchIndexs % nt,# threshold
        #                          batchIndexs], # actual selector
        #                         dim=1)
        clusters = clusters.float()
        fvs = Variable(self.rbf.clusterFV(clusters, sizes), requires_grad=False)
        batchIndexs = Variable(batchIndexs, requires_grad=False)
        phiIndex = Variable(uli, requires_grad=False)
        clusters = Variable(clusters, requires_grad=False)
        sizes = Variable(sizes.float(), requires_grad=False)

        #probs = phi_.view(nb*np, nr, nc).index_select(0, batchIndexs / nt)
        probs = phi.view(nb*np, nr, nc).index_select(0, batchIndexs/nt)
        #probs = phic.index_select(0, batchIndexs)

        # clusters = self.createCluster(clusters, 0.5, sizes)
        clusters = self.createCluster(clusters, 0.5, None)

        fvs = fvs.view(fvs.size(0), nr*nc, fvs.size(3)) # batch * nsamps * dim
        probs = probs.view(probs.size(0), nr*nc)
        clusters = clusters.view(probs.size(0), nr*nc)
        xs = xs.view(nb, nr* nc, xs.size(3)).index_select(0, batchIndexs / (np*nt))

        ## random sample the feature vector
        # TODO

        #theta, llikelihood =  self.cclShape(probss, clusterss, fvs, xs, sizess)
        return {'batchIndex': batchIndexs,
                'phiIndex': phiIndex,
                'clusterIndex': batchIndexs,
                'probs': probs,
                'clusters': clusters,
                'shapefvs': fvs,
                'contentfvs': xs,
                'np': np,
                # 'theta': theta,
                # 'shapellikelihood': llikelihood,
                'sizes': sizes}
