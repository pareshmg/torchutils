from torch.nn.modules.module import Module
from ..functions.add import MyAddFunction

class MyAddModule(Module):
    def forward(self, input1, input2):
        return MyAddFunction()(input1, input2)

class LogSum(Module):
    def __init__(self, dim):
        super(LogSum, self).__init__()
        self.dim = dim

    def forward(self, t):
        """
        t : t_i = log x_i
        return : log (\sum_i x_i)
        """
        mx = t.max(self.dim)[0]
        return (t - mx.unsqueeze(self.dim)).exp().sum(self.dim).log() + mx
