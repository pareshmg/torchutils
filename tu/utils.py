import torch.nn as nn
import torch.nn.functional as F

class Identity(nn.Module):
    def forward(self, x):
        """
        x = float any
        """
        return x

class SimpleNN(nn.Module):
    """  applies linear layers and applies ReLU between each layer. Last layer doesn't have a relu
    """
    def __init__(self, nkerns, batchNormalize=False):
        super(SimpleNN, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(nkerns[i-1], nkerns[i]) for i in range(1,len(nkerns))])
        self.batchNormalize = batchNormalize
        self.bn = nn.BatchNorm1d()

    def forward(self, x):
        for i,l in enumerate(self.layers):
            x = l(x)
            if self.batchNormalize: x = self.bn(x)
            if i < len(self.layers)-1: x = F.relu(x)
        return x

class SimpleCNN(nn.Module):
    """  applies linear layers and applies ReLU between each layer. Last layer doesn't have a relu
    """
    def __init__(self, nkerns):
        super(SimpleCNN, self).__init__()
        self.layers = nn.ModuleList([nn.Conv2d(nkerns[i-1], nkerns[i], kernel_size=5,padding=2) for i in range(1,len(nkerns))])

    def forward(self, x):
        for i,l in enumerate(self.layers):
            x = l(x)
            if i < len(self.layers)-1: x = F.relu(x)
        return x

    def visualize(self, out):
        """  out :: nb * nk * nr * nc
        """
        out = F.sigmoid(out)
        img = hstackImgs([vstackImgs([out.select(0,bi).select(0,ki) for ki in range(out.size(1))]) for bi in range(out.size(0)) ])
        return img


class SimpleCNNPooling(nn.Module):
    """  applies linear layers and applies ReLU between each layer. Last layer doesn't have a relu
    """
    def __init__(self, nkerns):
        super(SimpleCNNPooling, self).__init__()
        self.layers = nn.ModuleList([nn.Conv2d(nkerns[i-1], nkerns[i], kernel_size=5,padding=2) for i in range(1,len(nkerns))])
        self.mp = nn.MaxPool2d(2)

    def forward(self, x):
        for i,l in enumerate(self.layers):
            x = l(x)
            if i < len(self.layers)-1:
                x = self.mp(x)
                x = F.relu(x)
        return x

    def visualize(self, out):
        """  out :: nb * nk * nr * nc
        """
        out = F.sigmoid(out)
        img = hstackImgs([vstackImgs([out.select(0,bi).select(0,ki) for ki in range(out.size(1))]) for bi in range(out.size(0)) ])
        return img



class LossM(object):
    # override this
    def getLoss(self, x, y, res):
        pass

class VisualizeM(object):
    # override this
    def visualize(self, res, fname):
        pass
