CUDA_PATH=/usr/local/cuda/
PATH=$PATH:/usr/local/cuda/bin

python setup.py clean
rm -rf build
rm -rf dist
rm -rf __pycache__
rm -rf tu.egg-info
rm -rf /afs/csail.mit.edu/u/p/pareshmg/.pyenv/versions/3.6.0/lib/python3.6/site-packages/tu-*

cd tu/src/cuda
echo "Compiling cuda kernels with nvcc..."
rm *.o
make

cd ../../../
python setup.py build
python setup.py install
